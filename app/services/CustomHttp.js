"use strict";
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var CustomHttp = (function (_super) {
    __extends(CustomHttp, _super);
    function CustomHttp(backend, defaultOptions) {
        return _super.call(this, backend, defaultOptions) || this;
    }
    CustomHttp.prototype.request = function (url, options) {
        if (url instanceof http_1.Request) {
            url.headers.append("app-version", "2.0.1");
            url.headers.append("platform", "ios");
        }
        console.log("##############");
        console.dump(url);
        // console.dump(options);
        return _super.prototype.request.call(this, url, options);
    };
    return CustomHttp;
}(http_1.Http));
CustomHttp = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.ConnectionBackend, http_1.RequestOptions])
], CustomHttp);
exports.CustomHttp = CustomHttp;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ3VzdG9tSHR0cC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkN1c3RvbUh0dHAudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHNDQUEyQztBQUUzQyxzQ0FBK0c7QUFFL0csSUFBYSxVQUFVO0lBQVMsOEJBQUk7SUFDbEMsb0JBQVksT0FBMEIsRUFBRSxjQUE4QjtlQUNwRSxrQkFBTSxPQUFPLEVBQUUsY0FBYyxDQUFDO0lBQ2hDLENBQUM7SUFDRCw0QkFBTyxHQUFQLFVBQVEsR0FBcUIsRUFBRSxPQUE0QjtRQUN6RCxFQUFFLENBQUMsQ0FBQyxHQUFHLFlBQVksY0FBTyxDQUFDLENBQUMsQ0FBQztZQUMzQixHQUFHLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDM0MsR0FBRyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ3hDLENBQUM7UUFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDOUIsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUVsQix5QkFBeUI7UUFDekIsTUFBTSxDQUFDLGlCQUFNLE9BQU8sWUFBQyxHQUFHLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDckMsQ0FBQztJQUVILGlCQUFDO0FBQUQsQ0FBQyxBQWhCRCxDQUFnQyxXQUFJLEdBZ0JuQztBQWhCWSxVQUFVO0lBRHRCLGlCQUFVLEVBQUU7cUNBRVUsd0JBQWlCLEVBQWtCLHFCQUFjO0dBRDNELFVBQVUsQ0FnQnRCO0FBaEJZLGdDQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcy9PYnNlcnZhYmxlJztcbmltcG9ydCB7IEh0dHAsIFJlcXVlc3QsIFJlc3BvbnNlLCBSZXF1ZXN0T3B0aW9uc0FyZ3MsIENvbm5lY3Rpb25CYWNrZW5kLCBSZXF1ZXN0T3B0aW9ucyB9IGZyb20gXCJAYW5ndWxhci9odHRwXCI7XG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgQ3VzdG9tSHR0cCBleHRlbmRzIEh0dHAge1xuICBjb25zdHJ1Y3RvcihiYWNrZW5kOiBDb25uZWN0aW9uQmFja2VuZCwgZGVmYXVsdE9wdGlvbnM6IFJlcXVlc3RPcHRpb25zKSB7XG4gICAgc3VwZXIoYmFja2VuZCwgZGVmYXVsdE9wdGlvbnMpO1xuICB9XG4gIHJlcXVlc3QodXJsOiBzdHJpbmcgfCBSZXF1ZXN0LCBvcHRpb25zPzogUmVxdWVzdE9wdGlvbnNBcmdzKTogT2JzZXJ2YWJsZTxSZXNwb25zZT4ge1xuICAgIGlmICh1cmwgaW5zdGFuY2VvZiBSZXF1ZXN0KSB7XG4gICAgICB1cmwuaGVhZGVycy5hcHBlbmQoXCJhcHAtdmVyc2lvblwiLCBcIjIuMC4xXCIpO1xuICAgICAgdXJsLmhlYWRlcnMuYXBwZW5kKFwicGxhdGZvcm1cIiwgXCJpb3NcIik7XG4gICAgfVxuICAgIGNvbnNvbGUubG9nKFwiIyMjIyMjIyMjIyMjIyNcIik7XG4gICAgY29uc29sZS5kdW1wKHVybCk7XG5cbiAgICAvLyBjb25zb2xlLmR1bXAob3B0aW9ucyk7XG4gICAgcmV0dXJuIHN1cGVyLnJlcXVlc3QodXJsLCBvcHRpb25zKTsgICAgICAgIFxuICB9XG5cbn0iXX0=