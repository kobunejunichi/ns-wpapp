import { Component } from "@angular/core";
import { LoopBackConfig } from './shared/sdk/index';

@Component({
  selector: "my-app",
  templateUrl: "./app.component.html"
})
export class AppComponent {
  user: any
  constructor() {
    this.user = {
      name: "Junichi Kobune"
    };
    LoopBackConfig.setBaseURL('http://52.37.120.228');
    LoopBackConfig.setApiVersion('v1');
    LoopBackConfig.setAuthPrefix("authorization");
  }
  goTodo() {
    console.log("go!");
  }
}
