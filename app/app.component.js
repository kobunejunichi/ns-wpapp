"use strict";
var core_1 = require("@angular/core");
var index_1 = require("./shared/sdk/index");
var AppComponent = (function () {
    function AppComponent() {
        this.user = {
            name: "Junichi Kobune"
        };
        index_1.LoopBackConfig.setBaseURL('http://52.37.120.228');
        index_1.LoopBackConfig.setApiVersion('v1');
        index_1.LoopBackConfig.setAuthPrefix("authorization");
    }
    AppComponent.prototype.goTodo = function () {
        console.log("go!");
    };
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: "my-app",
        templateUrl: "./app.component.html"
    }),
    __metadata("design:paramtypes", [])
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHNDQUEwQztBQUMxQyw0Q0FBb0Q7QUFNcEQsSUFBYSxZQUFZO0lBRXZCO1FBQ0UsSUFBSSxDQUFDLElBQUksR0FBRztZQUNWLElBQUksRUFBRSxnQkFBZ0I7U0FDdkIsQ0FBQztRQUNGLHNCQUFjLENBQUMsVUFBVSxDQUFDLHNCQUFzQixDQUFDLENBQUM7UUFDbEQsc0JBQWMsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbkMsc0JBQWMsQ0FBQyxhQUFhLENBQUMsZUFBZSxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUNELDZCQUFNLEdBQU47UUFDRSxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3JCLENBQUM7SUFDSCxtQkFBQztBQUFELENBQUMsQUFiRCxJQWFDO0FBYlksWUFBWTtJQUp4QixnQkFBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLFFBQVE7UUFDbEIsV0FBVyxFQUFFLHNCQUFzQjtLQUNwQyxDQUFDOztHQUNXLFlBQVksQ0FheEI7QUFiWSxvQ0FBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBMb29wQmFja0NvbmZpZyB9IGZyb20gJy4vc2hhcmVkL3Nkay9pbmRleCc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJteS1hcHBcIixcbiAgdGVtcGxhdGVVcmw6IFwiLi9hcHAuY29tcG9uZW50Lmh0bWxcIlxufSlcbmV4cG9ydCBjbGFzcyBBcHBDb21wb25lbnQge1xuICB1c2VyOiBhbnlcbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy51c2VyID0ge1xuICAgICAgbmFtZTogXCJKdW5pY2hpIEtvYnVuZVwiXG4gICAgfTtcbiAgICBMb29wQmFja0NvbmZpZy5zZXRCYXNlVVJMKCdodHRwOi8vNTIuMzcuMTIwLjIyOCcpO1xuICAgIExvb3BCYWNrQ29uZmlnLnNldEFwaVZlcnNpb24oJ3YxJyk7XG4gICAgTG9vcEJhY2tDb25maWcuc2V0QXV0aFByZWZpeChcImF1dGhvcml6YXRpb25cIik7XG4gIH1cbiAgZ29Ub2RvKCkge1xuICAgIGNvbnNvbGUubG9nKFwiZ28hXCIpO1xuICB9XG59XG4iXX0=