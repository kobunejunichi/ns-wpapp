"use strict";
/**
 * @module Storage
 * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
 * @license MIT
 * @description
 * The InternalStorage class is used for dependency injection swapping.
 * It will be provided using factory method from different sources.
 **/
var Storage = (function () {
    function Storage() {
    }
    /**
     * @method get
     * @param {string} key Storage key name
     * @return {any}
     * @description
     * The getter will return any type of data persisted in storage.
     **/
    Storage.prototype.get = function (key) { };
    /**
     * @method set
     * @param {string} key Storage key name
     * @param {any} value Any value
     * @return {void}
     * @description
     * The setter will return any type of data persisted in localStorage.
     **/
    Storage.prototype.set = function (key, value) { };
    /**
     * @method remove
     * @param {string} key Storage key name
     * @return {void}
     * @description
     * This method will remove a localStorage item from the client.
     **/
    Storage.prototype.remove = function (key) { };
    return Storage;
}());
exports.Storage = Storage;
/**
 * @module InternalStorage
 * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
 * @license MIT
 * @description
 * The InternalStorage class is used for dependency injection swapping.
 * It will be provided using factory method from different sources.
 * This is mainly required because Angular Universal integration.
 * It does inject a CookieStorage instead of LocalStorage.
 **/
var InternalStorage = (function (_super) {
    __extends(InternalStorage, _super);
    function InternalStorage() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return InternalStorage;
}(Storage));
exports.InternalStorage = InternalStorage;
/**
 * @module SDKStorage
 * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
 * @license MIT
 * @description
 * The SDKStorage class is used for dependency injection swapping.
 * It will be provided using factory method according the right environment.
 * This is created for public usage, to allow persisting custom data
 * Into the local storage API.
 **/
var SDKStorage = (function (_super) {
    __extends(SDKStorage, _super);
    function SDKStorage() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return SDKStorage;
}(Storage));
exports.SDKStorage = SDKStorage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmFnZS5zd2Fwcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInN0b3JhZ2Uuc3dhcHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBOzs7Ozs7O0lBT0k7QUFDSjtJQUFBO0lBMEJBLENBQUM7SUF6QkM7Ozs7OztRQU1JO0lBQ0oscUJBQUcsR0FBSCxVQUFJLEdBQVcsSUFBUSxDQUFDO0lBQ3hCOzs7Ozs7O1FBT0k7SUFDSixxQkFBRyxHQUFILFVBQUksR0FBVyxFQUFFLEtBQVUsSUFBUyxDQUFDO0lBQ3JDOzs7Ozs7UUFNSTtJQUNKLHdCQUFNLEdBQU4sVUFBTyxHQUFXLElBQVMsQ0FBQztJQUM5QixjQUFDO0FBQUQsQ0FBQyxBQTFCRCxJQTBCQztBQTFCWSwwQkFBTztBQTJCcEI7Ozs7Ozs7OztJQVNJO0FBQ0o7SUFBcUMsbUNBQU87SUFBNUM7O0lBQThDLENBQUM7SUFBRCxzQkFBQztBQUFELENBQUMsQUFBL0MsQ0FBcUMsT0FBTyxHQUFHO0FBQWxDLDBDQUFlO0FBQzVCOzs7Ozs7Ozs7SUFTSTtBQUNKO0lBQWdDLDhCQUFPO0lBQXZDOztJQUF5QyxDQUFDO0lBQUQsaUJBQUM7QUFBRCxDQUFDLEFBQTFDLENBQWdDLE9BQU8sR0FBRztBQUE3QixnQ0FBVSIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICrCoEBtb2R1bGUgU3RvcmFnZVxuICogQGF1dGhvciBKb25hdGhhbiBDYXNhcnJ1YmlhcyA8dDogam9obmNhc2FycnViaWFzLCBnaDogbWVhbi1leHBlcnQtb2ZmaWNpYWw+XG4gKiBAbGljZW5zZSBNSVRcbiAqIEBkZXNjcmlwdGlvblxuICogVGhlIEludGVybmFsU3RvcmFnZSBjbGFzcyBpcyB1c2VkIGZvciBkZXBlbmRlbmN5IGluamVjdGlvbiBzd2FwcGluZy5cbiAqIEl0IHdpbGwgYmUgcHJvdmlkZWQgdXNpbmcgZmFjdG9yeSBtZXRob2QgZnJvbSBkaWZmZXJlbnQgc291cmNlcy5cbiAqKi9cbmV4cG9ydCBjbGFzcyBTdG9yYWdlIHtcbiAgLyoqXG4gICAqIEBtZXRob2QgZ2V0XG4gICAqIEBwYXJhbSB7c3RyaW5nfSBrZXkgU3RvcmFnZSBrZXkgbmFtZVxuICAgKiBAcmV0dXJuIHthbnl9XG4gICAqIEBkZXNjcmlwdGlvblxuICAgKiBUaGUgZ2V0dGVyIHdpbGwgcmV0dXJuIGFueSB0eXBlIG9mIGRhdGEgcGVyc2lzdGVkIGluIHN0b3JhZ2UuXG4gICAqKi9cbiAgZ2V0KGtleTogc3RyaW5nKTogYW55IHt9XG4gIC8qKlxuICAgKiBAbWV0aG9kIHNldFxuICAgKiBAcGFyYW0ge3N0cmluZ30ga2V5IFN0b3JhZ2Uga2V5IG5hbWVcbiAgICogQHBhcmFtIHthbnl9IHZhbHVlIEFueSB2YWx1ZVxuICAgKiBAcmV0dXJuIHt2b2lkfVxuICAgKiBAZGVzY3JpcHRpb25cbiAgICogVGhlIHNldHRlciB3aWxsIHJldHVybiBhbnkgdHlwZSBvZiBkYXRhIHBlcnNpc3RlZCBpbiBsb2NhbFN0b3JhZ2UuXG4gICAqKi9cbiAgc2V0KGtleTogc3RyaW5nLCB2YWx1ZTogYW55KTogdm9pZCB7fVxuICAvKipcbiAgICogQG1ldGhvZCByZW1vdmVcbiAgICogQHBhcmFtIHtzdHJpbmd9IGtleSBTdG9yYWdlIGtleSBuYW1lXG4gICAqIEByZXR1cm4ge3ZvaWR9XG4gICAqIEBkZXNjcmlwdGlvblxuICAgKiBUaGlzIG1ldGhvZCB3aWxsIHJlbW92ZSBhIGxvY2FsU3RvcmFnZSBpdGVtIGZyb20gdGhlIGNsaWVudC5cbiAgICoqL1xuICByZW1vdmUoa2V5OiBzdHJpbmcpOiB2b2lkIHt9XG59XG4vKipcbiAqwqBAbW9kdWxlIEludGVybmFsU3RvcmFnZVxuICogQGF1dGhvciBKb25hdGhhbiBDYXNhcnJ1YmlhcyA8dDogam9obmNhc2FycnViaWFzLCBnaDogbWVhbi1leHBlcnQtb2ZmaWNpYWw+XG4gKiBAbGljZW5zZSBNSVRcbiAqIEBkZXNjcmlwdGlvblxuICogVGhlIEludGVybmFsU3RvcmFnZSBjbGFzcyBpcyB1c2VkIGZvciBkZXBlbmRlbmN5IGluamVjdGlvbiBzd2FwcGluZy5cbiAqIEl0IHdpbGwgYmUgcHJvdmlkZWQgdXNpbmcgZmFjdG9yeSBtZXRob2QgZnJvbSBkaWZmZXJlbnQgc291cmNlcy5cbiAqIFRoaXMgaXMgbWFpbmx5IHJlcXVpcmVkIGJlY2F1c2UgQW5ndWxhciBVbml2ZXJzYWwgaW50ZWdyYXRpb24uXG4gKiBJdCBkb2VzIGluamVjdCBhIENvb2tpZVN0b3JhZ2UgaW5zdGVhZCBvZiBMb2NhbFN0b3JhZ2UuXG4gKiovXG5leHBvcnQgY2xhc3MgSW50ZXJuYWxTdG9yYWdlIGV4dGVuZHMgU3RvcmFnZSB7fVxuLyoqXG4gKsKgQG1vZHVsZSBTREtTdG9yYWdlXG4gKiBAYXV0aG9yIEpvbmF0aGFuIENhc2FycnViaWFzIDx0OiBqb2huY2FzYXJydWJpYXMsIGdoOiBtZWFuLWV4cGVydC1vZmZpY2lhbD5cbiAqIEBsaWNlbnNlIE1JVFxuICogQGRlc2NyaXB0aW9uXG4gKiBUaGUgU0RLU3RvcmFnZSBjbGFzcyBpcyB1c2VkIGZvciBkZXBlbmRlbmN5IGluamVjdGlvbiBzd2FwcGluZy5cbiAqIEl0IHdpbGwgYmUgcHJvdmlkZWQgdXNpbmcgZmFjdG9yeSBtZXRob2QgYWNjb3JkaW5nIHRoZSByaWdodCBlbnZpcm9ubWVudC5cbiAqIFRoaXMgaXMgY3JlYXRlZCBmb3IgcHVibGljIHVzYWdlLCB0byBhbGxvdyBwZXJzaXN0aW5nIGN1c3RvbSBkYXRhXG4gKiBJbnRvIHRoZSBsb2NhbCBzdG9yYWdlIEFQSS5cbiAqKi9cbmV4cG9ydCBjbGFzcyBTREtTdG9yYWdlIGV4dGVuZHMgU3RvcmFnZSB7fVxuIl19