"use strict";
/* tslint:disable */
var AppSettings = require("application-settings");
var core_1 = require("@angular/core");
/**
* @author Jonathan Casarrubias <twitter:@johncasarrubias> <github:@mean-expert-official>
* @module StorageNative
* @license MIT
* @description
* This module handle localStorage, it will be provided using DI Swapping according the
* SDK Socket Driver Available currently supporting Angular 2 for web and NativeScript 2.
**/
var StorageNative = (function () {
    function StorageNative() {
    }
    /**
     * @method get
     * @param {string} key Storage key name
     * @return {any}
     * @description
     * The getter will return any type of data persisted in localStorage.
     **/
    StorageNative.prototype.get = function (key) {
        var data = AppSettings.getString(key);
        return this.parse(data);
    };
    /**
     * @method set
     * @param {string} key Storage key name
     * @param {any} value Any value
     * @return {void}
     * @description
     * The setter will return any type of data persisted in localStorage.
     **/
    StorageNative.prototype.set = function (key, value) {
        AppSettings.setString(key, String(typeof value === 'object' ? JSON.stringify(value) : value));
    };
    /**
     * @method remove
     * @param {string} key Storage key name
     * @return {void}
     * @description
     * This method will remove a localStorage item from the client.
     **/
    StorageNative.prototype.remove = function (key) {
        if (AppSettings.hasKey(key)) {
            AppSettings.remove(key);
        }
        else {
            console.log('Trying to remove unexisting key: ', key);
        }
    };
    /**
     * @method parse
     * @param {any} value Input data expected to be JSON
     * @return {void}
     * @description
     * This method will parse the string as JSON if possible, otherwise will
     * return the value itself.
     **/
    StorageNative.prototype.parse = function (value) {
        try {
            return JSON.parse(value);
        }
        catch (e) {
            return value;
        }
    };
    return StorageNative;
}());
StorageNative = __decorate([
    core_1.Injectable()
], StorageNative);
exports.StorageNative = StorageNative;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmFnZS5uYXRpdmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdG9yYWdlLm5hdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsb0JBQW9CO0FBQ3BCLGtEQUFvRDtBQUNwRCxzQ0FBMkM7QUFDM0M7Ozs7Ozs7R0FPRztBQUVILElBQWEsYUFBYTtJQUExQjtJQXVEQSxDQUFDO0lBdERDOzs7Ozs7UUFNSTtJQUNKLDJCQUFHLEdBQUgsVUFBSSxHQUFXO1FBQ2IsSUFBSSxJQUFJLEdBQVcsV0FBVyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUM5QyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMxQixDQUFDO0lBQ0Q7Ozs7Ozs7UUFPSTtJQUNKLDJCQUFHLEdBQUgsVUFBSSxHQUFXLEVBQUUsS0FBVTtRQUN6QixXQUFXLENBQUMsU0FBUyxDQUNuQixHQUFHLEVBQ0gsTUFBTSxDQUFDLE9BQU8sS0FBSyxLQUFLLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxHQUFHLEtBQUssQ0FBQyxDQUNsRSxDQUFDO0lBQ0osQ0FBQztJQUNEOzs7Ozs7UUFNSTtJQUNKLDhCQUFNLEdBQU4sVUFBTyxHQUFXO1FBQ2hCLEVBQUUsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzVCLFdBQVcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDMUIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQ0FBbUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUN4RCxDQUFDO0lBQ0gsQ0FBQztJQUNEOzs7Ozs7O1FBT0k7SUFDSSw2QkFBSyxHQUFiLFVBQWMsS0FBVTtRQUN0QixJQUFJLENBQUM7WUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM3QixDQUFDO1FBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNULE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDakIsQ0FBQztJQUNILENBQUM7SUFDSCxvQkFBQztBQUFELENBQUMsQUF2REQsSUF1REM7QUF2RFksYUFBYTtJQUR6QixpQkFBVSxFQUFFO0dBQ0EsYUFBYSxDQXVEekI7QUF2RFksc0NBQWEiLCJzb3VyY2VzQ29udGVudCI6WyIvKiB0c2xpbnQ6ZGlzYWJsZSAqL1xuaW1wb3J0ICogYXMgQXBwU2V0dGluZ3MgZnJvbSAnYXBwbGljYXRpb24tc2V0dGluZ3MnO1xuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuLyoqXG4qIEBhdXRob3IgSm9uYXRoYW4gQ2FzYXJydWJpYXMgPHR3aXR0ZXI6QGpvaG5jYXNhcnJ1Ymlhcz4gPGdpdGh1YjpAbWVhbi1leHBlcnQtb2ZmaWNpYWw+XG4qIEBtb2R1bGUgU3RvcmFnZU5hdGl2ZVxuKiBAbGljZW5zZSBNSVRcbiogQGRlc2NyaXB0aW9uXG4qIFRoaXMgbW9kdWxlIGhhbmRsZSBsb2NhbFN0b3JhZ2UsIGl0IHdpbGwgYmUgcHJvdmlkZWQgdXNpbmcgREkgU3dhcHBpbmcgYWNjb3JkaW5nIHRoZVxuKiBTREsgU29ja2V0IERyaXZlciBBdmFpbGFibGUgY3VycmVudGx5IHN1cHBvcnRpbmcgQW5ndWxhciAyIGZvciB3ZWIgYW5kIE5hdGl2ZVNjcmlwdCAyLlxuKiovXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgU3RvcmFnZU5hdGl2ZSB7XG4gIC8qKlxuICAgKiBAbWV0aG9kIGdldFxuICAgKiBAcGFyYW0ge3N0cmluZ30ga2V5IFN0b3JhZ2Uga2V5IG5hbWVcbiAgICogQHJldHVybiB7YW55fVxuICAgKiBAZGVzY3JpcHRpb25cbiAgICogVGhlIGdldHRlciB3aWxsIHJldHVybiBhbnkgdHlwZSBvZiBkYXRhIHBlcnNpc3RlZCBpbiBsb2NhbFN0b3JhZ2UuXG4gICAqKi9cbiAgZ2V0KGtleTogc3RyaW5nKTogYW55IHtcbiAgICBsZXQgZGF0YTogc3RyaW5nID0gQXBwU2V0dGluZ3MuZ2V0U3RyaW5nKGtleSk7XG4gICAgcmV0dXJuIHRoaXMucGFyc2UoZGF0YSk7XG4gIH1cbiAgLyoqXG4gICAqIEBtZXRob2Qgc2V0XG4gICAqIEBwYXJhbSB7c3RyaW5nfSBrZXkgU3RvcmFnZSBrZXkgbmFtZVxuICAgKiBAcGFyYW0ge2FueX0gdmFsdWUgQW55IHZhbHVlXG4gICAqIEByZXR1cm4ge3ZvaWR9XG4gICAqIEBkZXNjcmlwdGlvblxuICAgKiBUaGUgc2V0dGVyIHdpbGwgcmV0dXJuIGFueSB0eXBlIG9mIGRhdGEgcGVyc2lzdGVkIGluIGxvY2FsU3RvcmFnZS5cbiAgICoqL1xuICBzZXQoa2V5OiBzdHJpbmcsIHZhbHVlOiBhbnkpOiB2b2lkIHtcbiAgICBBcHBTZXR0aW5ncy5zZXRTdHJpbmcoXG4gICAgICBrZXksXG4gICAgICBTdHJpbmcodHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyA/IEpTT04uc3RyaW5naWZ5KHZhbHVlKSA6IHZhbHVlKVxuICAgICk7XG4gIH1cbiAgLyoqXG4gICAqIEBtZXRob2QgcmVtb3ZlXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBrZXkgU3RvcmFnZSBrZXkgbmFtZVxuICAgKiBAcmV0dXJuIHt2b2lkfVxuICAgKiBAZGVzY3JpcHRpb25cbiAgICogVGhpcyBtZXRob2Qgd2lsbCByZW1vdmUgYSBsb2NhbFN0b3JhZ2UgaXRlbSBmcm9tIHRoZSBjbGllbnQuXG4gICAqKi9cbiAgcmVtb3ZlKGtleTogc3RyaW5nKTogYW55IHtcbiAgICBpZiAoQXBwU2V0dGluZ3MuaGFzS2V5KGtleSkpIHtcbiAgICAgIEFwcFNldHRpbmdzLnJlbW92ZShrZXkpO1xuICAgIH0gZWxzZSB7XG4gICAgICBjb25zb2xlLmxvZygnVHJ5aW5nIHRvIHJlbW92ZSB1bmV4aXN0aW5nIGtleTogJywga2V5KTtcbiAgICB9XG4gIH1cbiAgLyoqXG4gICAqIEBtZXRob2QgcGFyc2VcbiAgICogQHBhcmFtIHthbnl9IHZhbHVlIElucHV0IGRhdGEgZXhwZWN0ZWQgdG8gYmUgSlNPTlxuICAgKiBAcmV0dXJuIHt2b2lkfVxuICAgKiBAZGVzY3JpcHRpb25cbiAgICogVGhpcyBtZXRob2Qgd2lsbCBwYXJzZSB0aGUgc3RyaW5nIGFzIEpTT04gaWYgcG9zc2libGUsIG90aGVyd2lzZSB3aWxsXG4gICAqIHJldHVybiB0aGUgdmFsdWUgaXRzZWxmLlxuICAgKiovXG4gIHByaXZhdGUgcGFyc2UodmFsdWU6IGFueSkge1xuICAgIHRyeSB7XG4gICAgICAgIHJldHVybiBKU09OLnBhcnNlKHZhbHVlKTtcbiAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgIHJldHVybiB2YWx1ZTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==