"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
/**
* @module SDKModule
* @author Jonathan Casarrubias <t:@johncasarrubias> <gh:jonathan-casarrubias>
* @license MIT 2016 Jonathan Casarrubias
* @version 2.1.0
* @description
* The SDKModule is a generated Software Development Kit automatically built by
* the LoopBack SDK Builder open source module.
*
* The SDKModule provides Angular 2 >= RC.5 support, which means that NgModules
* can import this Software Development Kit as follows:
*
*
* APP Route Module Context
* ============================================================================
* import { NgModule }       from '@angular/core';
* import { BrowserModule }  from '@angular/platform-browser';
* // App Root
* import { AppComponent }   from './app.component';
* // Feature Modules
* import { SDK[Browser|Node|Native]Module } from './shared/sdk/sdk.module';
* // Import Routing
* import { routing }        from './app.routing';
* @NgModule({
*  imports: [
*    BrowserModule,
*    routing,
*    SDK[Browser|Node|Native]Module.forRoot()
*  ],
*  declarations: [ AppComponent ],
*  bootstrap:    [ AppComponent ]
* })
* export class AppModule { }
*
**/
var search_params_1 = require("./services/core/search.params");
var error_service_1 = require("./services/core/error.service");
var auth_service_1 = require("./services/core/auth.service");
var logger_service_1 = require("./services/custom/logger.service");
var SDKModels_1 = require("./services/custom/SDKModels");
var storage_swaps_1 = require("./storage/storage.swaps");
var http_1 = require("@angular/http");
var common_1 = require("@angular/common");
var core_1 = require("@angular/core");
var storage_native_1 = require("./storage/storage.native");
var AppUser_1 = require("./services/custom/AppUser");
var Group_1 = require("./services/custom/Group");
var Article_1 = require("./services/custom/Article");
var Staff_1 = require("./services/custom/Staff");
var Invitation_1 = require("./services/custom/Invitation");
var Information_1 = require("./services/custom/Information");
var Design_1 = require("./services/custom/Design");
var Activity_1 = require("./services/custom/Activity");
var Util_1 = require("./services/custom/Util");
var AppVersion_1 = require("./services/custom/AppVersion");
/**
* @module SDKNativeModule
* @description
* This module should be imported when building a NativeScript Applications.
**/
var SDKNativeModule = SDKNativeModule_1 = (function () {
    function SDKNativeModule() {
    }
    SDKNativeModule.forRoot = function () {
        return {
            ngModule: SDKNativeModule_1,
            providers: [
                auth_service_1.LoopBackAuth,
                logger_service_1.LoggerService,
                search_params_1.JSONSearchParams,
                SDKModels_1.SDKModels,
                AppUser_1.AppUserApi,
                Group_1.GroupApi,
                Article_1.ArticleApi,
                Staff_1.StaffApi,
                Invitation_1.InvitationApi,
                Information_1.InformationApi,
                Design_1.DesignApi,
                Activity_1.ActivityApi,
                Util_1.UtilApi,
                AppVersion_1.AppVersionApi,
                { provide: storage_swaps_1.InternalStorage, useClass: storage_native_1.StorageNative },
                { provide: storage_swaps_1.SDKStorage, useClass: storage_native_1.StorageNative }
            ]
        };
    };
    return SDKNativeModule;
}());
SDKNativeModule = SDKNativeModule_1 = __decorate([
    core_1.NgModule({
        imports: [common_1.CommonModule, http_1.HttpModule],
        declarations: [],
        exports: [],
        providers: [
            error_service_1.ErrorHandler
        ]
    })
], SDKNativeModule);
exports.SDKNativeModule = SDKNativeModule;
/**
* Have Fun!!!
* - Jon
**/
__export(require("./models/index"));
__export(require("./services/index"));
__export(require("./lb.config"));
var SDKNativeModule_1;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0FrQ0c7QUFDSCwrREFBaUU7QUFDakUsK0RBQTZEO0FBQzdELDZEQUE0RDtBQUM1RCxtRUFBaUU7QUFDakUseURBQXdEO0FBQ3hELHlEQUFzRTtBQUN0RSxzQ0FBMkM7QUFDM0MsMENBQStDO0FBQy9DLHNDQUE4RDtBQUM5RCwyREFBeUQ7QUFDekQscURBQXVEO0FBQ3ZELGlEQUFtRDtBQUNuRCxxREFBdUQ7QUFDdkQsaURBQW1EO0FBQ25ELDJEQUE2RDtBQUM3RCw2REFBK0Q7QUFDL0QsbURBQXFEO0FBQ3JELHVEQUF5RDtBQUN6RCwrQ0FBaUQ7QUFDakQsMkRBQTZEO0FBQzdEOzs7O0dBSUc7QUFTSCxJQUFhLGVBQWU7SUFBNUI7SUF3QkEsQ0FBQztJQXZCUSx1QkFBTyxHQUFkO1FBQ0UsTUFBTSxDQUFDO1lBQ0wsUUFBUSxFQUFJLGlCQUFlO1lBQzNCLFNBQVMsRUFBRztnQkFDViwyQkFBWTtnQkFDWiw4QkFBYTtnQkFDYixnQ0FBZ0I7Z0JBQ2hCLHFCQUFTO2dCQUNULG9CQUFVO2dCQUNWLGdCQUFRO2dCQUNSLG9CQUFVO2dCQUNWLGdCQUFRO2dCQUNSLDBCQUFhO2dCQUNiLDRCQUFjO2dCQUNkLGtCQUFTO2dCQUNULHNCQUFXO2dCQUNYLGNBQU87Z0JBQ1AsMEJBQWE7Z0JBQ2IsRUFBRSxPQUFPLEVBQUUsK0JBQWUsRUFBRSxRQUFRLEVBQUUsOEJBQWEsRUFBRTtnQkFDckQsRUFBRSxPQUFPLEVBQUUsMEJBQVUsRUFBRSxRQUFRLEVBQUUsOEJBQWEsRUFBRTthQUNqRDtTQUNGLENBQUM7SUFDSixDQUFDO0lBQ0gsc0JBQUM7QUFBRCxDQUFDLEFBeEJELElBd0JDO0FBeEJZLGVBQWU7SUFSM0IsZUFBUSxDQUFDO1FBQ1IsT0FBTyxFQUFPLENBQUUscUJBQVksRUFBRSxpQkFBVSxDQUFFO1FBQzFDLFlBQVksRUFBRSxFQUFHO1FBQ2pCLE9BQU8sRUFBTyxFQUFHO1FBQ2pCLFNBQVMsRUFBSztZQUNaLDRCQUFZO1NBQ2I7S0FDRixDQUFDO0dBQ1csZUFBZSxDQXdCM0I7QUF4QlksMENBQWU7QUF5QjVCOzs7R0FHRztBQUNILG9DQUErQjtBQUMvQixzQ0FBaUM7QUFDakMsaUNBQTRCIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4qIEBtb2R1bGUgU0RLTW9kdWxlXG4qIEBhdXRob3IgSm9uYXRoYW4gQ2FzYXJydWJpYXMgPHQ6QGpvaG5jYXNhcnJ1Ymlhcz4gPGdoOmpvbmF0aGFuLWNhc2FycnViaWFzPlxuKiBAbGljZW5zZSBNSVQgMjAxNiBKb25hdGhhbiBDYXNhcnJ1Ymlhc1xuKiBAdmVyc2lvbiAyLjEuMFxuKiBAZGVzY3JpcHRpb25cbiogVGhlIFNES01vZHVsZSBpcyBhIGdlbmVyYXRlZCBTb2Z0d2FyZSBEZXZlbG9wbWVudCBLaXQgYXV0b21hdGljYWxseSBidWlsdCBieVxuKiB0aGUgTG9vcEJhY2sgU0RLIEJ1aWxkZXIgb3BlbiBzb3VyY2UgbW9kdWxlLlxuKlxuKiBUaGUgU0RLTW9kdWxlIHByb3ZpZGVzIEFuZ3VsYXIgMiA+PSBSQy41IHN1cHBvcnQsIHdoaWNoIG1lYW5zIHRoYXQgTmdNb2R1bGVzXG4qIGNhbiBpbXBvcnQgdGhpcyBTb2Z0d2FyZSBEZXZlbG9wbWVudCBLaXQgYXMgZm9sbG93czpcbipcbipcbiogQVBQIFJvdXRlIE1vZHVsZSBDb250ZXh0XG4qID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiogaW1wb3J0IHsgTmdNb2R1bGUgfSAgICAgICBmcm9tICdAYW5ndWxhci9jb3JlJztcbiogaW1wb3J0IHsgQnJvd3Nlck1vZHVsZSB9ICBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyJztcbiogLy8gQXBwIFJvb3QgXG4qIGltcG9ydCB7IEFwcENvbXBvbmVudCB9ICAgZnJvbSAnLi9hcHAuY29tcG9uZW50JztcbiogLy8gRmVhdHVyZSBNb2R1bGVzXG4qIGltcG9ydCB7IFNES1tCcm93c2VyfE5vZGV8TmF0aXZlXU1vZHVsZSB9IGZyb20gJy4vc2hhcmVkL3Nkay9zZGsubW9kdWxlJztcbiogLy8gSW1wb3J0IFJvdXRpbmdcbiogaW1wb3J0IHsgcm91dGluZyB9ICAgICAgICBmcm9tICcuL2FwcC5yb3V0aW5nJztcbiogQE5nTW9kdWxlKHtcbiogIGltcG9ydHM6IFtcbiogICAgQnJvd3Nlck1vZHVsZSxcbiogICAgcm91dGluZyxcbiogICAgU0RLW0Jyb3dzZXJ8Tm9kZXxOYXRpdmVdTW9kdWxlLmZvclJvb3QoKVxuKiAgXSxcbiogIGRlY2xhcmF0aW9uczogWyBBcHBDb21wb25lbnQgXSxcbiogIGJvb3RzdHJhcDogICAgWyBBcHBDb21wb25lbnQgXVxuKiB9KVxuKiBleHBvcnQgY2xhc3MgQXBwTW9kdWxlIHsgfVxuKlxuKiovXG5pbXBvcnQgeyBKU09OU2VhcmNoUGFyYW1zIH0gZnJvbSAnLi9zZXJ2aWNlcy9jb3JlL3NlYXJjaC5wYXJhbXMnO1xuaW1wb3J0IHsgRXJyb3JIYW5kbGVyIH0gZnJvbSAnLi9zZXJ2aWNlcy9jb3JlL2Vycm9yLnNlcnZpY2UnO1xuaW1wb3J0IHsgTG9vcEJhY2tBdXRoIH0gZnJvbSAnLi9zZXJ2aWNlcy9jb3JlL2F1dGguc2VydmljZSc7XG5pbXBvcnQgeyBMb2dnZXJTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9jdXN0b20vbG9nZ2VyLnNlcnZpY2UnO1xuaW1wb3J0IHsgU0RLTW9kZWxzIH0gZnJvbSAnLi9zZXJ2aWNlcy9jdXN0b20vU0RLTW9kZWxzJztcbmltcG9ydCB7IEludGVybmFsU3RvcmFnZSwgU0RLU3RvcmFnZSB9IGZyb20gJy4vc3RvcmFnZS9zdG9yYWdlLnN3YXBzJztcbmltcG9ydCB7IEh0dHBNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9odHRwJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBOZ01vZHVsZSwgTW9kdWxlV2l0aFByb3ZpZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgU3RvcmFnZU5hdGl2ZSB9IGZyb20gJy4vc3RvcmFnZS9zdG9yYWdlLm5hdGl2ZSc7XG5pbXBvcnQgeyBBcHBVc2VyQXBpIH0gZnJvbSAnLi9zZXJ2aWNlcy9jdXN0b20vQXBwVXNlcic7XG5pbXBvcnQgeyBHcm91cEFwaSB9IGZyb20gJy4vc2VydmljZXMvY3VzdG9tL0dyb3VwJztcbmltcG9ydCB7IEFydGljbGVBcGkgfSBmcm9tICcuL3NlcnZpY2VzL2N1c3RvbS9BcnRpY2xlJztcbmltcG9ydCB7IFN0YWZmQXBpIH0gZnJvbSAnLi9zZXJ2aWNlcy9jdXN0b20vU3RhZmYnO1xuaW1wb3J0IHsgSW52aXRhdGlvbkFwaSB9IGZyb20gJy4vc2VydmljZXMvY3VzdG9tL0ludml0YXRpb24nO1xuaW1wb3J0IHsgSW5mb3JtYXRpb25BcGkgfSBmcm9tICcuL3NlcnZpY2VzL2N1c3RvbS9JbmZvcm1hdGlvbic7XG5pbXBvcnQgeyBEZXNpZ25BcGkgfSBmcm9tICcuL3NlcnZpY2VzL2N1c3RvbS9EZXNpZ24nO1xuaW1wb3J0IHsgQWN0aXZpdHlBcGkgfSBmcm9tICcuL3NlcnZpY2VzL2N1c3RvbS9BY3Rpdml0eSc7XG5pbXBvcnQgeyBVdGlsQXBpIH0gZnJvbSAnLi9zZXJ2aWNlcy9jdXN0b20vVXRpbCc7XG5pbXBvcnQgeyBBcHBWZXJzaW9uQXBpIH0gZnJvbSAnLi9zZXJ2aWNlcy9jdXN0b20vQXBwVmVyc2lvbic7XG4vKipcbiogQG1vZHVsZSBTREtOYXRpdmVNb2R1bGVcbiogQGRlc2NyaXB0aW9uXG4qIFRoaXMgbW9kdWxlIHNob3VsZCBiZSBpbXBvcnRlZCB3aGVuIGJ1aWxkaW5nIGEgTmF0aXZlU2NyaXB0IEFwcGxpY2F0aW9ucy5cbioqL1xuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogICAgICBbIENvbW1vbk1vZHVsZSwgSHR0cE1vZHVsZSBdLFxuICBkZWNsYXJhdGlvbnM6IFsgXSxcbiAgZXhwb3J0czogICAgICBbIF0sXG4gIHByb3ZpZGVyczogICAgW1xuICAgIEVycm9ySGFuZGxlclxuICBdXG59KVxuZXhwb3J0IGNsYXNzIFNES05hdGl2ZU1vZHVsZSB7XG4gIHN0YXRpYyBmb3JSb290KCk6IE1vZHVsZVdpdGhQcm92aWRlcnMge1xuICAgIHJldHVybiB7XG4gICAgICBuZ01vZHVsZSAgOiBTREtOYXRpdmVNb2R1bGUsXG4gICAgICBwcm92aWRlcnMgOiBbXG4gICAgICAgIExvb3BCYWNrQXV0aCxcbiAgICAgICAgTG9nZ2VyU2VydmljZSxcbiAgICAgICAgSlNPTlNlYXJjaFBhcmFtcyxcbiAgICAgICAgU0RLTW9kZWxzLFxuICAgICAgICBBcHBVc2VyQXBpLFxuICAgICAgICBHcm91cEFwaSxcbiAgICAgICAgQXJ0aWNsZUFwaSxcbiAgICAgICAgU3RhZmZBcGksXG4gICAgICAgIEludml0YXRpb25BcGksXG4gICAgICAgIEluZm9ybWF0aW9uQXBpLFxuICAgICAgICBEZXNpZ25BcGksXG4gICAgICAgIEFjdGl2aXR5QXBpLFxuICAgICAgICBVdGlsQXBpLFxuICAgICAgICBBcHBWZXJzaW9uQXBpLFxuICAgICAgICB7IHByb3ZpZGU6IEludGVybmFsU3RvcmFnZSwgdXNlQ2xhc3M6IFN0b3JhZ2VOYXRpdmUgfSxcbiAgICAgICAgeyBwcm92aWRlOiBTREtTdG9yYWdlLCB1c2VDbGFzczogU3RvcmFnZU5hdGl2ZSB9XG4gICAgICBdXG4gICAgfTtcbiAgfVxufVxuLyoqXG4qIEhhdmUgRnVuISEhXG4qIC0gSm9uXG4qKi9cbmV4cG9ydCAqIGZyb20gJy4vbW9kZWxzL2luZGV4JztcbmV4cG9ydCAqIGZyb20gJy4vc2VydmljZXMvaW5kZXgnO1xuZXhwb3J0ICogZnJvbSAnLi9sYi5jb25maWcnO1xuIl19