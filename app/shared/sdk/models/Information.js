/* tslint:disable */
"use strict";
var Information = (function () {
    function Information(data) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Information`.
     */
    Information.getModelName = function () {
        return "Information";
    };
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Information for dynamic purposes.
    **/
    Information.factory = function (data) {
        return new Information(data);
    };
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    Information.getModelDefinition = function () {
        return {
            name: 'Information',
            plural: 'Information',
            properties: {
                name: {
                    name: 'name',
                    type: 'any'
                },
                message: {
                    name: 'message',
                    type: 'any'
                },
                informationDate: {
                    name: 'informationDate',
                    type: 'Date'
                },
                statusValid: {
                    name: 'statusValid',
                    type: 'string'
                },
                language: {
                    name: 'language',
                    type: 'string'
                },
                id: {
                    name: 'id',
                    type: 'any'
                },
                createdAt: {
                    name: 'createdAt',
                    type: 'Date',
                    default: new Date(0)
                },
                createdBy: {
                    name: 'createdBy',
                    type: 'string'
                },
                modifiedAt: {
                    name: 'modifiedAt',
                    type: 'Date',
                    default: new Date(0)
                },
                modifiedBy: {
                    name: 'modifiedBy',
                    type: 'string'
                },
            },
            relations: {}
        };
    };
    return Information;
}());
exports.Information = Information;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSW5mb3JtYXRpb24uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJJbmZvcm1hdGlvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxvQkFBb0I7O0FBZ0JwQjtJQVdFLHFCQUFZLElBQTJCO1FBQ3JDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzVCLENBQUM7SUFDRDs7O09BR0c7SUFDVyx3QkFBWSxHQUExQjtRQUNFLE1BQU0sQ0FBQyxhQUFhLENBQUM7SUFDdkIsQ0FBQztJQUNEOzs7OztPQUtHO0lBQ1csbUJBQU8sR0FBckIsVUFBc0IsSUFBMEI7UUFDOUMsTUFBTSxDQUFDLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFDRDs7Ozs7O09BTUc7SUFDVyw4QkFBa0IsR0FBaEM7UUFDRSxNQUFNLENBQUM7WUFDTCxJQUFJLEVBQUUsYUFBYTtZQUNuQixNQUFNLEVBQUUsYUFBYTtZQUNyQixVQUFVLEVBQUU7Z0JBQ1YsSUFBSSxFQUFFO29CQUNKLElBQUksRUFBRSxNQUFNO29CQUNaLElBQUksRUFBRSxLQUFLO2lCQUNaO2dCQUNELE9BQU8sRUFBRTtvQkFDUCxJQUFJLEVBQUUsU0FBUztvQkFDZixJQUFJLEVBQUUsS0FBSztpQkFDWjtnQkFDRCxlQUFlLEVBQUU7b0JBQ2YsSUFBSSxFQUFFLGlCQUFpQjtvQkFDdkIsSUFBSSxFQUFFLE1BQU07aUJBQ2I7Z0JBQ0QsV0FBVyxFQUFFO29CQUNYLElBQUksRUFBRSxhQUFhO29CQUNuQixJQUFJLEVBQUUsUUFBUTtpQkFDZjtnQkFDRCxRQUFRLEVBQUU7b0JBQ1IsSUFBSSxFQUFFLFVBQVU7b0JBQ2hCLElBQUksRUFBRSxRQUFRO2lCQUNmO2dCQUNELEVBQUUsRUFBRTtvQkFDRixJQUFJLEVBQUUsSUFBSTtvQkFDVixJQUFJLEVBQUUsS0FBSztpQkFDWjtnQkFDRCxTQUFTLEVBQUU7b0JBQ1QsSUFBSSxFQUFFLFdBQVc7b0JBQ2pCLElBQUksRUFBRSxNQUFNO29CQUNaLE9BQU8sRUFBRSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUM7aUJBQ3JCO2dCQUNELFNBQVMsRUFBRTtvQkFDVCxJQUFJLEVBQUUsV0FBVztvQkFDakIsSUFBSSxFQUFFLFFBQVE7aUJBQ2Y7Z0JBQ0QsVUFBVSxFQUFFO29CQUNWLElBQUksRUFBRSxZQUFZO29CQUNsQixJQUFJLEVBQUUsTUFBTTtvQkFDWixPQUFPLEVBQUUsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO2lCQUNyQjtnQkFDRCxVQUFVLEVBQUU7b0JBQ1YsSUFBSSxFQUFFLFlBQVk7b0JBQ2xCLElBQUksRUFBRSxRQUFRO2lCQUNmO2FBQ0Y7WUFDRCxTQUFTLEVBQUUsRUFDVjtTQUNGLENBQUE7SUFDSCxDQUFDO0lBQ0gsa0JBQUM7QUFBRCxDQUFDLEFBekZELElBeUZDO0FBekZZLGtDQUFXIiwic291cmNlc0NvbnRlbnQiOlsiLyogdHNsaW50OmRpc2FibGUgKi9cblxuZGVjbGFyZSB2YXIgT2JqZWN0OiBhbnk7XG5leHBvcnQgaW50ZXJmYWNlIEluZm9ybWF0aW9uSW50ZXJmYWNlIHtcbiAgbmFtZT86IGFueTtcbiAgbWVzc2FnZT86IGFueTtcbiAgaW5mb3JtYXRpb25EYXRlPzogRGF0ZTtcbiAgc3RhdHVzVmFsaWQ/OiBzdHJpbmc7XG4gIGxhbmd1YWdlPzogc3RyaW5nO1xuICBpZD86IGFueTtcbiAgY3JlYXRlZEF0PzogRGF0ZTtcbiAgY3JlYXRlZEJ5Pzogc3RyaW5nO1xuICBtb2RpZmllZEF0PzogRGF0ZTtcbiAgbW9kaWZpZWRCeT86IHN0cmluZztcbn1cblxuZXhwb3J0IGNsYXNzIEluZm9ybWF0aW9uIGltcGxlbWVudHMgSW5mb3JtYXRpb25JbnRlcmZhY2Uge1xuICBuYW1lOiBhbnk7XG4gIG1lc3NhZ2U6IGFueTtcbiAgaW5mb3JtYXRpb25EYXRlOiBEYXRlO1xuICBzdGF0dXNWYWxpZDogc3RyaW5nO1xuICBsYW5ndWFnZTogc3RyaW5nO1xuICBpZDogYW55O1xuICBjcmVhdGVkQXQ6IERhdGU7XG4gIGNyZWF0ZWRCeTogc3RyaW5nO1xuICBtb2RpZmllZEF0OiBEYXRlO1xuICBtb2RpZmllZEJ5OiBzdHJpbmc7XG4gIGNvbnN0cnVjdG9yKGRhdGE/OiBJbmZvcm1hdGlvbkludGVyZmFjZSkge1xuICAgIE9iamVjdC5hc3NpZ24odGhpcywgZGF0YSk7XG4gIH1cbiAgLyoqXG4gICAqIFRoZSBuYW1lIG9mIHRoZSBtb2RlbCByZXByZXNlbnRlZCBieSB0aGlzICRyZXNvdXJjZSxcbiAgICogaS5lLiBgSW5mb3JtYXRpb25gLlxuICAgKi9cbiAgcHVibGljIHN0YXRpYyBnZXRNb2RlbE5hbWUoKSB7XG4gICAgcmV0dXJuIFwiSW5mb3JtYXRpb25cIjtcbiAgfVxuICAvKipcbiAgKiBAbWV0aG9kIGZhY3RvcnlcbiAgKiBAYXV0aG9yIEpvbmF0aGFuIENhc2FycnViaWFzXG4gICogQGxpY2Vuc2UgTUlUXG4gICogVGhpcyBtZXRob2QgY3JlYXRlcyBhbiBpbnN0YW5jZSBvZiBJbmZvcm1hdGlvbiBmb3IgZHluYW1pYyBwdXJwb3Nlcy5cbiAgKiovXG4gIHB1YmxpYyBzdGF0aWMgZmFjdG9yeShkYXRhOiBJbmZvcm1hdGlvbkludGVyZmFjZSk6IEluZm9ybWF0aW9ue1xuICAgIHJldHVybiBuZXcgSW5mb3JtYXRpb24oZGF0YSk7XG4gIH0gIFxuICAvKipcbiAgKiBAbWV0aG9kIGdldE1vZGVsRGVmaW5pdGlvblxuICAqIEBhdXRob3IgSnVsaWVuIExlZHVuXG4gICogQGxpY2Vuc2UgTUlUXG4gICogVGhpcyBtZXRob2QgcmV0dXJucyBhbiBvYmplY3QgdGhhdCByZXByZXNlbnRzIHNvbWUgb2YgdGhlIG1vZGVsXG4gICogZGVmaW5pdGlvbnMuXG4gICoqL1xuICBwdWJsaWMgc3RhdGljIGdldE1vZGVsRGVmaW5pdGlvbigpIHtcbiAgICByZXR1cm4ge1xuICAgICAgbmFtZTogJ0luZm9ybWF0aW9uJyxcbiAgICAgIHBsdXJhbDogJ0luZm9ybWF0aW9uJyxcbiAgICAgIHByb3BlcnRpZXM6IHtcbiAgICAgICAgbmFtZToge1xuICAgICAgICAgIG5hbWU6ICduYW1lJyxcbiAgICAgICAgICB0eXBlOiAnYW55J1xuICAgICAgICB9LFxuICAgICAgICBtZXNzYWdlOiB7XG4gICAgICAgICAgbmFtZTogJ21lc3NhZ2UnLFxuICAgICAgICAgIHR5cGU6ICdhbnknXG4gICAgICAgIH0sXG4gICAgICAgIGluZm9ybWF0aW9uRGF0ZToge1xuICAgICAgICAgIG5hbWU6ICdpbmZvcm1hdGlvbkRhdGUnLFxuICAgICAgICAgIHR5cGU6ICdEYXRlJ1xuICAgICAgICB9LFxuICAgICAgICBzdGF0dXNWYWxpZDoge1xuICAgICAgICAgIG5hbWU6ICdzdGF0dXNWYWxpZCcsXG4gICAgICAgICAgdHlwZTogJ3N0cmluZydcbiAgICAgICAgfSxcbiAgICAgICAgbGFuZ3VhZ2U6IHtcbiAgICAgICAgICBuYW1lOiAnbGFuZ3VhZ2UnLFxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnXG4gICAgICAgIH0sXG4gICAgICAgIGlkOiB7XG4gICAgICAgICAgbmFtZTogJ2lkJyxcbiAgICAgICAgICB0eXBlOiAnYW55J1xuICAgICAgICB9LFxuICAgICAgICBjcmVhdGVkQXQ6IHtcbiAgICAgICAgICBuYW1lOiAnY3JlYXRlZEF0JyxcbiAgICAgICAgICB0eXBlOiAnRGF0ZScsXG4gICAgICAgICAgZGVmYXVsdDogbmV3IERhdGUoMClcbiAgICAgICAgfSxcbiAgICAgICAgY3JlYXRlZEJ5OiB7XG4gICAgICAgICAgbmFtZTogJ2NyZWF0ZWRCeScsXG4gICAgICAgICAgdHlwZTogJ3N0cmluZydcbiAgICAgICAgfSxcbiAgICAgICAgbW9kaWZpZWRBdDoge1xuICAgICAgICAgIG5hbWU6ICdtb2RpZmllZEF0JyxcbiAgICAgICAgICB0eXBlOiAnRGF0ZScsXG4gICAgICAgICAgZGVmYXVsdDogbmV3IERhdGUoMClcbiAgICAgICAgfSxcbiAgICAgICAgbW9kaWZpZWRCeToge1xuICAgICAgICAgIG5hbWU6ICdtb2RpZmllZEJ5JyxcbiAgICAgICAgICB0eXBlOiAnc3RyaW5nJ1xuICAgICAgICB9LFxuICAgICAgfSxcbiAgICAgIHJlbGF0aW9uczoge1xuICAgICAgfVxuICAgIH1cbiAgfVxufVxuIl19