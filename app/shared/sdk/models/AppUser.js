/* tslint:disable */
"use strict";
var AppUser = (function () {
    function AppUser(data) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `AppUser`.
     */
    AppUser.getModelName = function () {
        return "AppUser";
    };
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of AppUser for dynamic purposes.
    **/
    AppUser.factory = function (data) {
        return new AppUser(data);
    };
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    AppUser.getModelDefinition = function () {
        return {
            name: 'AppUser',
            plural: 'AppUsers',
            properties: {
                name: {
                    name: 'name',
                    type: 'string'
                },
                carrier: {
                    name: 'carrier',
                    type: 'string'
                },
                devices: {
                    name: 'devices',
                    type: 'any',
                    default: null
                },
                memberCode: {
                    name: 'memberCode',
                    type: 'string'
                },
                imageLargeUrl: {
                    name: 'imageLargeUrl',
                    type: 'string'
                },
                imageUrl: {
                    name: 'imageUrl',
                    type: 'string'
                },
                email: {
                    name: 'email',
                    type: 'string'
                },
                emailChanged: {
                    name: 'emailChanged',
                    type: 'string'
                },
                emailVerificationToken: {
                    name: 'emailVerificationToken',
                    type: 'string'
                },
                emailVerificationExpireDate: {
                    name: 'emailVerificationExpireDate',
                    type: 'Date'
                },
                emailSendRevokeCount: {
                    name: 'emailSendRevokeCount',
                    type: 'number',
                    default: 0
                },
                emailVerified: {
                    name: 'emailVerified',
                    type: 'string',
                    default: '1'
                },
                password: {
                    name: 'password',
                    type: 'string'
                },
                tmpPasswordExpireDate: {
                    name: 'tmpPasswordExpireDate',
                    type: 'Date'
                },
                notifications: {
                    name: 'notifications',
                    type: 'any'
                },
                infoNotifications: {
                    name: 'infoNotifications',
                    type: 'any'
                },
                groups: {
                    name: 'groups',
                    type: 'any',
                    default: null
                },
                via: {
                    name: 'via',
                    type: 'string'
                },
                joinDate: {
                    name: 'joinDate',
                    type: 'Date'
                },
                resignDate: {
                    name: 'resignDate',
                    type: 'Date'
                },
                resignReason: {
                    name: 'resignReason',
                    type: 'any'
                },
                dateLastLogined: {
                    name: 'dateLastLogined',
                    type: 'Date'
                },
                revokeCount: {
                    name: 'revokeCount',
                    type: 'number',
                    default: 0
                },
                statusValid: {
                    name: 'statusValid',
                    type: 'string',
                    default: '1'
                },
                statusDeleted: {
                    name: 'statusDeleted',
                    type: 'string',
                    default: '0'
                },
                loginCount: {
                    name: 'loginCount',
                    type: 'number',
                    default: 0
                },
                announceConfirmDate: {
                    name: 'announceConfirmDate',
                    type: 'any'
                },
                announceBadgeConfirmDate: {
                    name: 'announceBadgeConfirmDate',
                    type: 'any'
                },
                realm: {
                    name: 'realm',
                    type: 'string'
                },
                username: {
                    name: 'username',
                    type: 'string'
                },
                credentials: {
                    name: 'credentials',
                    type: 'any'
                },
                challenges: {
                    name: 'challenges',
                    type: 'any'
                },
                verificationToken: {
                    name: 'verificationToken',
                    type: 'string'
                },
                status: {
                    name: 'status',
                    type: 'string'
                },
                created: {
                    name: 'created',
                    type: 'Date'
                },
                lastUpdated: {
                    name: 'lastUpdated',
                    type: 'Date'
                },
                id: {
                    name: 'id',
                    type: 'any'
                },
                createdAt: {
                    name: 'createdAt',
                    type: 'Date',
                    default: new Date(0)
                },
                createdBy: {
                    name: 'createdBy',
                    type: 'string'
                },
                modifiedAt: {
                    name: 'modifiedAt',
                    type: 'Date',
                    default: new Date(0)
                },
                modifiedBy: {
                    name: 'modifiedBy',
                    type: 'string'
                },
            },
            relations: {
                accessTokens: {
                    name: 'accessTokens',
                    type: 'any[]',
                    model: ''
                },
            }
        };
    };
    return AppUser;
}());
exports.AppUser = AppUser;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQXBwVXNlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkFwcFVzZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsb0JBQW9COztBQStDcEI7SUEwQ0UsaUJBQVksSUFBdUI7UUFDakMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDNUIsQ0FBQztJQUNEOzs7T0FHRztJQUNXLG9CQUFZLEdBQTFCO1FBQ0UsTUFBTSxDQUFDLFNBQVMsQ0FBQztJQUNuQixDQUFDO0lBQ0Q7Ozs7O09BS0c7SUFDVyxlQUFPLEdBQXJCLFVBQXNCLElBQXNCO1FBQzFDLE1BQU0sQ0FBQyxJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMzQixDQUFDO0lBQ0Q7Ozs7OztPQU1HO0lBQ1csMEJBQWtCLEdBQWhDO1FBQ0UsTUFBTSxDQUFDO1lBQ0wsSUFBSSxFQUFFLFNBQVM7WUFDZixNQUFNLEVBQUUsVUFBVTtZQUNsQixVQUFVLEVBQUU7Z0JBQ1YsSUFBSSxFQUFFO29CQUNKLElBQUksRUFBRSxNQUFNO29CQUNaLElBQUksRUFBRSxRQUFRO2lCQUNmO2dCQUNELE9BQU8sRUFBRTtvQkFDUCxJQUFJLEVBQUUsU0FBUztvQkFDZixJQUFJLEVBQUUsUUFBUTtpQkFDZjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsSUFBSSxFQUFFLFNBQVM7b0JBQ2YsSUFBSSxFQUFFLEtBQUs7b0JBQ1gsT0FBTyxFQUFPLElBQUk7aUJBQ25CO2dCQUNELFVBQVUsRUFBRTtvQkFDVixJQUFJLEVBQUUsWUFBWTtvQkFDbEIsSUFBSSxFQUFFLFFBQVE7aUJBQ2Y7Z0JBQ0QsYUFBYSxFQUFFO29CQUNiLElBQUksRUFBRSxlQUFlO29CQUNyQixJQUFJLEVBQUUsUUFBUTtpQkFDZjtnQkFDRCxRQUFRLEVBQUU7b0JBQ1IsSUFBSSxFQUFFLFVBQVU7b0JBQ2hCLElBQUksRUFBRSxRQUFRO2lCQUNmO2dCQUNELEtBQUssRUFBRTtvQkFDTCxJQUFJLEVBQUUsT0FBTztvQkFDYixJQUFJLEVBQUUsUUFBUTtpQkFDZjtnQkFDRCxZQUFZLEVBQUU7b0JBQ1osSUFBSSxFQUFFLGNBQWM7b0JBQ3BCLElBQUksRUFBRSxRQUFRO2lCQUNmO2dCQUNELHNCQUFzQixFQUFFO29CQUN0QixJQUFJLEVBQUUsd0JBQXdCO29CQUM5QixJQUFJLEVBQUUsUUFBUTtpQkFDZjtnQkFDRCwyQkFBMkIsRUFBRTtvQkFDM0IsSUFBSSxFQUFFLDZCQUE2QjtvQkFDbkMsSUFBSSxFQUFFLE1BQU07aUJBQ2I7Z0JBQ0Qsb0JBQW9CLEVBQUU7b0JBQ3BCLElBQUksRUFBRSxzQkFBc0I7b0JBQzVCLElBQUksRUFBRSxRQUFRO29CQUNkLE9BQU8sRUFBRSxDQUFDO2lCQUNYO2dCQUNELGFBQWEsRUFBRTtvQkFDYixJQUFJLEVBQUUsZUFBZTtvQkFDckIsSUFBSSxFQUFFLFFBQVE7b0JBQ2QsT0FBTyxFQUFFLEdBQUc7aUJBQ2I7Z0JBQ0QsUUFBUSxFQUFFO29CQUNSLElBQUksRUFBRSxVQUFVO29CQUNoQixJQUFJLEVBQUUsUUFBUTtpQkFDZjtnQkFDRCxxQkFBcUIsRUFBRTtvQkFDckIsSUFBSSxFQUFFLHVCQUF1QjtvQkFDN0IsSUFBSSxFQUFFLE1BQU07aUJBQ2I7Z0JBQ0QsYUFBYSxFQUFFO29CQUNiLElBQUksRUFBRSxlQUFlO29CQUNyQixJQUFJLEVBQUUsS0FBSztpQkFDWjtnQkFDRCxpQkFBaUIsRUFBRTtvQkFDakIsSUFBSSxFQUFFLG1CQUFtQjtvQkFDekIsSUFBSSxFQUFFLEtBQUs7aUJBQ1o7Z0JBQ0QsTUFBTSxFQUFFO29CQUNOLElBQUksRUFBRSxRQUFRO29CQUNkLElBQUksRUFBRSxLQUFLO29CQUNYLE9BQU8sRUFBTyxJQUFJO2lCQUNuQjtnQkFDRCxHQUFHLEVBQUU7b0JBQ0gsSUFBSSxFQUFFLEtBQUs7b0JBQ1gsSUFBSSxFQUFFLFFBQVE7aUJBQ2Y7Z0JBQ0QsUUFBUSxFQUFFO29CQUNSLElBQUksRUFBRSxVQUFVO29CQUNoQixJQUFJLEVBQUUsTUFBTTtpQkFDYjtnQkFDRCxVQUFVLEVBQUU7b0JBQ1YsSUFBSSxFQUFFLFlBQVk7b0JBQ2xCLElBQUksRUFBRSxNQUFNO2lCQUNiO2dCQUNELFlBQVksRUFBRTtvQkFDWixJQUFJLEVBQUUsY0FBYztvQkFDcEIsSUFBSSxFQUFFLEtBQUs7aUJBQ1o7Z0JBQ0QsZUFBZSxFQUFFO29CQUNmLElBQUksRUFBRSxpQkFBaUI7b0JBQ3ZCLElBQUksRUFBRSxNQUFNO2lCQUNiO2dCQUNELFdBQVcsRUFBRTtvQkFDWCxJQUFJLEVBQUUsYUFBYTtvQkFDbkIsSUFBSSxFQUFFLFFBQVE7b0JBQ2QsT0FBTyxFQUFFLENBQUM7aUJBQ1g7Z0JBQ0QsV0FBVyxFQUFFO29CQUNYLElBQUksRUFBRSxhQUFhO29CQUNuQixJQUFJLEVBQUUsUUFBUTtvQkFDZCxPQUFPLEVBQUUsR0FBRztpQkFDYjtnQkFDRCxhQUFhLEVBQUU7b0JBQ2IsSUFBSSxFQUFFLGVBQWU7b0JBQ3JCLElBQUksRUFBRSxRQUFRO29CQUNkLE9BQU8sRUFBRSxHQUFHO2lCQUNiO2dCQUNELFVBQVUsRUFBRTtvQkFDVixJQUFJLEVBQUUsWUFBWTtvQkFDbEIsSUFBSSxFQUFFLFFBQVE7b0JBQ2QsT0FBTyxFQUFFLENBQUM7aUJBQ1g7Z0JBQ0QsbUJBQW1CLEVBQUU7b0JBQ25CLElBQUksRUFBRSxxQkFBcUI7b0JBQzNCLElBQUksRUFBRSxLQUFLO2lCQUNaO2dCQUNELHdCQUF3QixFQUFFO29CQUN4QixJQUFJLEVBQUUsMEJBQTBCO29CQUNoQyxJQUFJLEVBQUUsS0FBSztpQkFDWjtnQkFDRCxLQUFLLEVBQUU7b0JBQ0wsSUFBSSxFQUFFLE9BQU87b0JBQ2IsSUFBSSxFQUFFLFFBQVE7aUJBQ2Y7Z0JBQ0QsUUFBUSxFQUFFO29CQUNSLElBQUksRUFBRSxVQUFVO29CQUNoQixJQUFJLEVBQUUsUUFBUTtpQkFDZjtnQkFDRCxXQUFXLEVBQUU7b0JBQ1gsSUFBSSxFQUFFLGFBQWE7b0JBQ25CLElBQUksRUFBRSxLQUFLO2lCQUNaO2dCQUNELFVBQVUsRUFBRTtvQkFDVixJQUFJLEVBQUUsWUFBWTtvQkFDbEIsSUFBSSxFQUFFLEtBQUs7aUJBQ1o7Z0JBQ0QsaUJBQWlCLEVBQUU7b0JBQ2pCLElBQUksRUFBRSxtQkFBbUI7b0JBQ3pCLElBQUksRUFBRSxRQUFRO2lCQUNmO2dCQUNELE1BQU0sRUFBRTtvQkFDTixJQUFJLEVBQUUsUUFBUTtvQkFDZCxJQUFJLEVBQUUsUUFBUTtpQkFDZjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsSUFBSSxFQUFFLFNBQVM7b0JBQ2YsSUFBSSxFQUFFLE1BQU07aUJBQ2I7Z0JBQ0QsV0FBVyxFQUFFO29CQUNYLElBQUksRUFBRSxhQUFhO29CQUNuQixJQUFJLEVBQUUsTUFBTTtpQkFDYjtnQkFDRCxFQUFFLEVBQUU7b0JBQ0YsSUFBSSxFQUFFLElBQUk7b0JBQ1YsSUFBSSxFQUFFLEtBQUs7aUJBQ1o7Z0JBQ0QsU0FBUyxFQUFFO29CQUNULElBQUksRUFBRSxXQUFXO29CQUNqQixJQUFJLEVBQUUsTUFBTTtvQkFDWixPQUFPLEVBQUUsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO2lCQUNyQjtnQkFDRCxTQUFTLEVBQUU7b0JBQ1QsSUFBSSxFQUFFLFdBQVc7b0JBQ2pCLElBQUksRUFBRSxRQUFRO2lCQUNmO2dCQUNELFVBQVUsRUFBRTtvQkFDVixJQUFJLEVBQUUsWUFBWTtvQkFDbEIsSUFBSSxFQUFFLE1BQU07b0JBQ1osT0FBTyxFQUFFLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQztpQkFDckI7Z0JBQ0QsVUFBVSxFQUFFO29CQUNWLElBQUksRUFBRSxZQUFZO29CQUNsQixJQUFJLEVBQUUsUUFBUTtpQkFDZjthQUNGO1lBQ0QsU0FBUyxFQUFFO2dCQUNULFlBQVksRUFBRTtvQkFDWixJQUFJLEVBQUUsY0FBYztvQkFDcEIsSUFBSSxFQUFFLE9BQU87b0JBQ2IsS0FBSyxFQUFFLEVBQUU7aUJBQ1Y7YUFDRjtTQUNGLENBQUE7SUFDSCxDQUFDO0lBQ0gsY0FBQztBQUFELENBQUMsQUFqUUQsSUFpUUM7QUFqUVksMEJBQU8iLCJzb3VyY2VzQ29udGVudCI6WyIvKiB0c2xpbnQ6ZGlzYWJsZSAqL1xuXG5kZWNsYXJlIHZhciBPYmplY3Q6IGFueTtcbmV4cG9ydCBpbnRlcmZhY2UgQXBwVXNlckludGVyZmFjZSB7XG4gIG5hbWU/OiBzdHJpbmc7XG4gIGNhcnJpZXI/OiBzdHJpbmc7XG4gIGRldmljZXM/OiBhbnk7XG4gIG1lbWJlckNvZGU/OiBzdHJpbmc7XG4gIGltYWdlTGFyZ2VVcmw/OiBzdHJpbmc7XG4gIGltYWdlVXJsPzogc3RyaW5nO1xuICBlbWFpbD86IHN0cmluZztcbiAgZW1haWxDaGFuZ2VkPzogc3RyaW5nO1xuICBlbWFpbFZlcmlmaWNhdGlvblRva2VuPzogc3RyaW5nO1xuICBlbWFpbFZlcmlmaWNhdGlvbkV4cGlyZURhdGU/OiBEYXRlO1xuICBlbWFpbFNlbmRSZXZva2VDb3VudD86IG51bWJlcjtcbiAgZW1haWxWZXJpZmllZD86IHN0cmluZztcbiAgcGFzc3dvcmQ/OiBzdHJpbmc7XG4gIHRtcFBhc3N3b3JkRXhwaXJlRGF0ZT86IERhdGU7XG4gIG5vdGlmaWNhdGlvbnM/OiBhbnk7XG4gIGluZm9Ob3RpZmljYXRpb25zPzogYW55O1xuICBncm91cHM/OiBhbnk7XG4gIHZpYT86IHN0cmluZztcbiAgam9pbkRhdGU/OiBEYXRlO1xuICByZXNpZ25EYXRlPzogRGF0ZTtcbiAgcmVzaWduUmVhc29uPzogYW55O1xuICBkYXRlTGFzdExvZ2luZWQ/OiBEYXRlO1xuICByZXZva2VDb3VudD86IG51bWJlcjtcbiAgc3RhdHVzVmFsaWQ/OiBzdHJpbmc7XG4gIHN0YXR1c0RlbGV0ZWQ/OiBzdHJpbmc7XG4gIGxvZ2luQ291bnQ/OiBudW1iZXI7XG4gIGFubm91bmNlQ29uZmlybURhdGU/OiBhbnk7XG4gIGFubm91bmNlQmFkZ2VDb25maXJtRGF0ZT86IGFueTtcbiAgcmVhbG0/OiBzdHJpbmc7XG4gIHVzZXJuYW1lPzogc3RyaW5nO1xuICBjaGFsbGVuZ2VzPzogYW55O1xuICB2ZXJpZmljYXRpb25Ub2tlbj86IHN0cmluZztcbiAgc3RhdHVzPzogc3RyaW5nO1xuICBjcmVhdGVkPzogRGF0ZTtcbiAgbGFzdFVwZGF0ZWQ/OiBEYXRlO1xuICBpZD86IGFueTtcbiAgY3JlYXRlZEF0PzogRGF0ZTtcbiAgY3JlYXRlZEJ5Pzogc3RyaW5nO1xuICBtb2RpZmllZEF0PzogRGF0ZTtcbiAgbW9kaWZpZWRCeT86IHN0cmluZztcbiAgYWNjZXNzVG9rZW5zPzogYW55W107XG59XG5cbmV4cG9ydCBjbGFzcyBBcHBVc2VyIGltcGxlbWVudHMgQXBwVXNlckludGVyZmFjZSB7XG4gIG5hbWU6IHN0cmluZztcbiAgY2Fycmllcjogc3RyaW5nO1xuICBkZXZpY2VzOiBhbnk7XG4gIG1lbWJlckNvZGU6IHN0cmluZztcbiAgaW1hZ2VMYXJnZVVybDogc3RyaW5nO1xuICBpbWFnZVVybDogc3RyaW5nO1xuICBlbWFpbDogc3RyaW5nO1xuICBlbWFpbENoYW5nZWQ6IHN0cmluZztcbiAgZW1haWxWZXJpZmljYXRpb25Ub2tlbjogc3RyaW5nO1xuICBlbWFpbFZlcmlmaWNhdGlvbkV4cGlyZURhdGU6IERhdGU7XG4gIGVtYWlsU2VuZFJldm9rZUNvdW50OiBudW1iZXI7XG4gIGVtYWlsVmVyaWZpZWQ6IHN0cmluZztcbiAgcGFzc3dvcmQ6IHN0cmluZztcbiAgdG1wUGFzc3dvcmRFeHBpcmVEYXRlOiBEYXRlO1xuICBub3RpZmljYXRpb25zOiBhbnk7XG4gIGluZm9Ob3RpZmljYXRpb25zOiBhbnk7XG4gIGdyb3VwczogYW55O1xuICB2aWE6IHN0cmluZztcbiAgam9pbkRhdGU6IERhdGU7XG4gIHJlc2lnbkRhdGU6IERhdGU7XG4gIHJlc2lnblJlYXNvbjogYW55O1xuICBkYXRlTGFzdExvZ2luZWQ6IERhdGU7XG4gIHJldm9rZUNvdW50OiBudW1iZXI7XG4gIHN0YXR1c1ZhbGlkOiBzdHJpbmc7XG4gIHN0YXR1c0RlbGV0ZWQ6IHN0cmluZztcbiAgbG9naW5Db3VudDogbnVtYmVyO1xuICBhbm5vdW5jZUNvbmZpcm1EYXRlOiBhbnk7XG4gIGFubm91bmNlQmFkZ2VDb25maXJtRGF0ZTogYW55O1xuICByZWFsbTogc3RyaW5nO1xuICB1c2VybmFtZTogc3RyaW5nO1xuICBjaGFsbGVuZ2VzOiBhbnk7XG4gIHZlcmlmaWNhdGlvblRva2VuOiBzdHJpbmc7XG4gIHN0YXR1czogc3RyaW5nO1xuICBjcmVhdGVkOiBEYXRlO1xuICBsYXN0VXBkYXRlZDogRGF0ZTtcbiAgaWQ6IGFueTtcbiAgY3JlYXRlZEF0OiBEYXRlO1xuICBjcmVhdGVkQnk6IHN0cmluZztcbiAgbW9kaWZpZWRBdDogRGF0ZTtcbiAgbW9kaWZpZWRCeTogc3RyaW5nO1xuICBhY2Nlc3NUb2tlbnM6IGFueVtdO1xuICBjb25zdHJ1Y3RvcihkYXRhPzogQXBwVXNlckludGVyZmFjZSkge1xuICAgIE9iamVjdC5hc3NpZ24odGhpcywgZGF0YSk7XG4gIH1cbiAgLyoqXG4gICAqIFRoZSBuYW1lIG9mIHRoZSBtb2RlbCByZXByZXNlbnRlZCBieSB0aGlzICRyZXNvdXJjZSxcbiAgICogaS5lLiBgQXBwVXNlcmAuXG4gICAqL1xuICBwdWJsaWMgc3RhdGljIGdldE1vZGVsTmFtZSgpIHtcbiAgICByZXR1cm4gXCJBcHBVc2VyXCI7XG4gIH1cbiAgLyoqXG4gICogQG1ldGhvZCBmYWN0b3J5XG4gICogQGF1dGhvciBKb25hdGhhbiBDYXNhcnJ1Ymlhc1xuICAqIEBsaWNlbnNlIE1JVFxuICAqIFRoaXMgbWV0aG9kIGNyZWF0ZXMgYW4gaW5zdGFuY2Ugb2YgQXBwVXNlciBmb3IgZHluYW1pYyBwdXJwb3Nlcy5cbiAgKiovXG4gIHB1YmxpYyBzdGF0aWMgZmFjdG9yeShkYXRhOiBBcHBVc2VySW50ZXJmYWNlKTogQXBwVXNlcntcbiAgICByZXR1cm4gbmV3IEFwcFVzZXIoZGF0YSk7XG4gIH0gIFxuICAvKipcbiAgKiBAbWV0aG9kIGdldE1vZGVsRGVmaW5pdGlvblxuICAqIEBhdXRob3IgSnVsaWVuIExlZHVuXG4gICogQGxpY2Vuc2UgTUlUXG4gICogVGhpcyBtZXRob2QgcmV0dXJucyBhbiBvYmplY3QgdGhhdCByZXByZXNlbnRzIHNvbWUgb2YgdGhlIG1vZGVsXG4gICogZGVmaW5pdGlvbnMuXG4gICoqL1xuICBwdWJsaWMgc3RhdGljIGdldE1vZGVsRGVmaW5pdGlvbigpIHtcbiAgICByZXR1cm4ge1xuICAgICAgbmFtZTogJ0FwcFVzZXInLFxuICAgICAgcGx1cmFsOiAnQXBwVXNlcnMnLFxuICAgICAgcHJvcGVydGllczoge1xuICAgICAgICBuYW1lOiB7XG4gICAgICAgICAgbmFtZTogJ25hbWUnLFxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnXG4gICAgICAgIH0sXG4gICAgICAgIGNhcnJpZXI6IHtcbiAgICAgICAgICBuYW1lOiAnY2FycmllcicsXG4gICAgICAgICAgdHlwZTogJ3N0cmluZydcbiAgICAgICAgfSxcbiAgICAgICAgZGV2aWNlczoge1xuICAgICAgICAgIG5hbWU6ICdkZXZpY2VzJyxcbiAgICAgICAgICB0eXBlOiAnYW55JyxcbiAgICAgICAgICBkZWZhdWx0OiA8YW55Pm51bGxcbiAgICAgICAgfSxcbiAgICAgICAgbWVtYmVyQ29kZToge1xuICAgICAgICAgIG5hbWU6ICdtZW1iZXJDb2RlJyxcbiAgICAgICAgICB0eXBlOiAnc3RyaW5nJ1xuICAgICAgICB9LFxuICAgICAgICBpbWFnZUxhcmdlVXJsOiB7XG4gICAgICAgICAgbmFtZTogJ2ltYWdlTGFyZ2VVcmwnLFxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnXG4gICAgICAgIH0sXG4gICAgICAgIGltYWdlVXJsOiB7XG4gICAgICAgICAgbmFtZTogJ2ltYWdlVXJsJyxcbiAgICAgICAgICB0eXBlOiAnc3RyaW5nJ1xuICAgICAgICB9LFxuICAgICAgICBlbWFpbDoge1xuICAgICAgICAgIG5hbWU6ICdlbWFpbCcsXG4gICAgICAgICAgdHlwZTogJ3N0cmluZydcbiAgICAgICAgfSxcbiAgICAgICAgZW1haWxDaGFuZ2VkOiB7XG4gICAgICAgICAgbmFtZTogJ2VtYWlsQ2hhbmdlZCcsXG4gICAgICAgICAgdHlwZTogJ3N0cmluZydcbiAgICAgICAgfSxcbiAgICAgICAgZW1haWxWZXJpZmljYXRpb25Ub2tlbjoge1xuICAgICAgICAgIG5hbWU6ICdlbWFpbFZlcmlmaWNhdGlvblRva2VuJyxcbiAgICAgICAgICB0eXBlOiAnc3RyaW5nJ1xuICAgICAgICB9LFxuICAgICAgICBlbWFpbFZlcmlmaWNhdGlvbkV4cGlyZURhdGU6IHtcbiAgICAgICAgICBuYW1lOiAnZW1haWxWZXJpZmljYXRpb25FeHBpcmVEYXRlJyxcbiAgICAgICAgICB0eXBlOiAnRGF0ZSdcbiAgICAgICAgfSxcbiAgICAgICAgZW1haWxTZW5kUmV2b2tlQ291bnQ6IHtcbiAgICAgICAgICBuYW1lOiAnZW1haWxTZW5kUmV2b2tlQ291bnQnLFxuICAgICAgICAgIHR5cGU6ICdudW1iZXInLFxuICAgICAgICAgIGRlZmF1bHQ6IDBcbiAgICAgICAgfSxcbiAgICAgICAgZW1haWxWZXJpZmllZDoge1xuICAgICAgICAgIG5hbWU6ICdlbWFpbFZlcmlmaWVkJyxcbiAgICAgICAgICB0eXBlOiAnc3RyaW5nJyxcbiAgICAgICAgICBkZWZhdWx0OiAnMSdcbiAgICAgICAgfSxcbiAgICAgICAgcGFzc3dvcmQ6IHtcbiAgICAgICAgICBuYW1lOiAncGFzc3dvcmQnLFxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnXG4gICAgICAgIH0sXG4gICAgICAgIHRtcFBhc3N3b3JkRXhwaXJlRGF0ZToge1xuICAgICAgICAgIG5hbWU6ICd0bXBQYXNzd29yZEV4cGlyZURhdGUnLFxuICAgICAgICAgIHR5cGU6ICdEYXRlJ1xuICAgICAgICB9LFxuICAgICAgICBub3RpZmljYXRpb25zOiB7XG4gICAgICAgICAgbmFtZTogJ25vdGlmaWNhdGlvbnMnLFxuICAgICAgICAgIHR5cGU6ICdhbnknXG4gICAgICAgIH0sXG4gICAgICAgIGluZm9Ob3RpZmljYXRpb25zOiB7XG4gICAgICAgICAgbmFtZTogJ2luZm9Ob3RpZmljYXRpb25zJyxcbiAgICAgICAgICB0eXBlOiAnYW55J1xuICAgICAgICB9LFxuICAgICAgICBncm91cHM6IHtcbiAgICAgICAgICBuYW1lOiAnZ3JvdXBzJyxcbiAgICAgICAgICB0eXBlOiAnYW55JyxcbiAgICAgICAgICBkZWZhdWx0OiA8YW55Pm51bGxcbiAgICAgICAgfSxcbiAgICAgICAgdmlhOiB7XG4gICAgICAgICAgbmFtZTogJ3ZpYScsXG4gICAgICAgICAgdHlwZTogJ3N0cmluZydcbiAgICAgICAgfSxcbiAgICAgICAgam9pbkRhdGU6IHtcbiAgICAgICAgICBuYW1lOiAnam9pbkRhdGUnLFxuICAgICAgICAgIHR5cGU6ICdEYXRlJ1xuICAgICAgICB9LFxuICAgICAgICByZXNpZ25EYXRlOiB7XG4gICAgICAgICAgbmFtZTogJ3Jlc2lnbkRhdGUnLFxuICAgICAgICAgIHR5cGU6ICdEYXRlJ1xuICAgICAgICB9LFxuICAgICAgICByZXNpZ25SZWFzb246IHtcbiAgICAgICAgICBuYW1lOiAncmVzaWduUmVhc29uJyxcbiAgICAgICAgICB0eXBlOiAnYW55J1xuICAgICAgICB9LFxuICAgICAgICBkYXRlTGFzdExvZ2luZWQ6IHtcbiAgICAgICAgICBuYW1lOiAnZGF0ZUxhc3RMb2dpbmVkJyxcbiAgICAgICAgICB0eXBlOiAnRGF0ZSdcbiAgICAgICAgfSxcbiAgICAgICAgcmV2b2tlQ291bnQ6IHtcbiAgICAgICAgICBuYW1lOiAncmV2b2tlQ291bnQnLFxuICAgICAgICAgIHR5cGU6ICdudW1iZXInLFxuICAgICAgICAgIGRlZmF1bHQ6IDBcbiAgICAgICAgfSxcbiAgICAgICAgc3RhdHVzVmFsaWQ6IHtcbiAgICAgICAgICBuYW1lOiAnc3RhdHVzVmFsaWQnLFxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnLFxuICAgICAgICAgIGRlZmF1bHQ6ICcxJ1xuICAgICAgICB9LFxuICAgICAgICBzdGF0dXNEZWxldGVkOiB7XG4gICAgICAgICAgbmFtZTogJ3N0YXR1c0RlbGV0ZWQnLFxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnLFxuICAgICAgICAgIGRlZmF1bHQ6ICcwJ1xuICAgICAgICB9LFxuICAgICAgICBsb2dpbkNvdW50OiB7XG4gICAgICAgICAgbmFtZTogJ2xvZ2luQ291bnQnLFxuICAgICAgICAgIHR5cGU6ICdudW1iZXInLFxuICAgICAgICAgIGRlZmF1bHQ6IDBcbiAgICAgICAgfSxcbiAgICAgICAgYW5ub3VuY2VDb25maXJtRGF0ZToge1xuICAgICAgICAgIG5hbWU6ICdhbm5vdW5jZUNvbmZpcm1EYXRlJyxcbiAgICAgICAgICB0eXBlOiAnYW55J1xuICAgICAgICB9LFxuICAgICAgICBhbm5vdW5jZUJhZGdlQ29uZmlybURhdGU6IHtcbiAgICAgICAgICBuYW1lOiAnYW5ub3VuY2VCYWRnZUNvbmZpcm1EYXRlJyxcbiAgICAgICAgICB0eXBlOiAnYW55J1xuICAgICAgICB9LFxuICAgICAgICByZWFsbToge1xuICAgICAgICAgIG5hbWU6ICdyZWFsbScsXG4gICAgICAgICAgdHlwZTogJ3N0cmluZydcbiAgICAgICAgfSxcbiAgICAgICAgdXNlcm5hbWU6IHtcbiAgICAgICAgICBuYW1lOiAndXNlcm5hbWUnLFxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnXG4gICAgICAgIH0sXG4gICAgICAgIGNyZWRlbnRpYWxzOiB7XG4gICAgICAgICAgbmFtZTogJ2NyZWRlbnRpYWxzJyxcbiAgICAgICAgICB0eXBlOiAnYW55J1xuICAgICAgICB9LFxuICAgICAgICBjaGFsbGVuZ2VzOiB7XG4gICAgICAgICAgbmFtZTogJ2NoYWxsZW5nZXMnLFxuICAgICAgICAgIHR5cGU6ICdhbnknXG4gICAgICAgIH0sXG4gICAgICAgIHZlcmlmaWNhdGlvblRva2VuOiB7XG4gICAgICAgICAgbmFtZTogJ3ZlcmlmaWNhdGlvblRva2VuJyxcbiAgICAgICAgICB0eXBlOiAnc3RyaW5nJ1xuICAgICAgICB9LFxuICAgICAgICBzdGF0dXM6IHtcbiAgICAgICAgICBuYW1lOiAnc3RhdHVzJyxcbiAgICAgICAgICB0eXBlOiAnc3RyaW5nJ1xuICAgICAgICB9LFxuICAgICAgICBjcmVhdGVkOiB7XG4gICAgICAgICAgbmFtZTogJ2NyZWF0ZWQnLFxuICAgICAgICAgIHR5cGU6ICdEYXRlJ1xuICAgICAgICB9LFxuICAgICAgICBsYXN0VXBkYXRlZDoge1xuICAgICAgICAgIG5hbWU6ICdsYXN0VXBkYXRlZCcsXG4gICAgICAgICAgdHlwZTogJ0RhdGUnXG4gICAgICAgIH0sXG4gICAgICAgIGlkOiB7XG4gICAgICAgICAgbmFtZTogJ2lkJyxcbiAgICAgICAgICB0eXBlOiAnYW55J1xuICAgICAgICB9LFxuICAgICAgICBjcmVhdGVkQXQ6IHtcbiAgICAgICAgICBuYW1lOiAnY3JlYXRlZEF0JyxcbiAgICAgICAgICB0eXBlOiAnRGF0ZScsXG4gICAgICAgICAgZGVmYXVsdDogbmV3IERhdGUoMClcbiAgICAgICAgfSxcbiAgICAgICAgY3JlYXRlZEJ5OiB7XG4gICAgICAgICAgbmFtZTogJ2NyZWF0ZWRCeScsXG4gICAgICAgICAgdHlwZTogJ3N0cmluZydcbiAgICAgICAgfSxcbiAgICAgICAgbW9kaWZpZWRBdDoge1xuICAgICAgICAgIG5hbWU6ICdtb2RpZmllZEF0JyxcbiAgICAgICAgICB0eXBlOiAnRGF0ZScsXG4gICAgICAgICAgZGVmYXVsdDogbmV3IERhdGUoMClcbiAgICAgICAgfSxcbiAgICAgICAgbW9kaWZpZWRCeToge1xuICAgICAgICAgIG5hbWU6ICdtb2RpZmllZEJ5JyxcbiAgICAgICAgICB0eXBlOiAnc3RyaW5nJ1xuICAgICAgICB9LFxuICAgICAgfSxcbiAgICAgIHJlbGF0aW9uczoge1xuICAgICAgICBhY2Nlc3NUb2tlbnM6IHtcbiAgICAgICAgICBuYW1lOiAnYWNjZXNzVG9rZW5zJyxcbiAgICAgICAgICB0eXBlOiAnYW55W10nLFxuICAgICAgICAgIG1vZGVsOiAnJ1xuICAgICAgICB9LFxuICAgICAgfVxuICAgIH1cbiAgfVxufVxuIl19