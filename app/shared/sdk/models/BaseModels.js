/* tslint:disable */
"use strict";
var AccessToken = (function () {
    function AccessToken(data) {
        this.id = '';
        this.ttl = 1209600;
        this.created = new Date(0);
        this.userId = 0;
        this.user = null;
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `AccessToken`.
     */
    AccessToken.getModelName = function () {
        return "AccessToken";
    };
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of AccessToken for dynamic purposes.
    **/
    AccessToken.factory = function (data) {
        return new AccessToken(data);
    };
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    AccessToken.getModelDefinition = function () {
        return {
            name: 'AccessToken',
            plural: 'AccessTokens',
            properties: {
                id: {
                    name: 'id',
                    type: 'string'
                },
                ttl: {
                    name: 'ttl',
                    type: 'number',
                    default: 1209600
                },
                created: {
                    name: 'created',
                    type: 'Date',
                    default: new Date(0)
                },
                userId: {
                    name: 'userId',
                    type: 'number'
                },
            },
            relations: {
                user: {
                    name: 'user',
                    type: 'User',
                    model: 'User'
                },
            }
        };
    };
    return AccessToken;
}());
exports.AccessToken = AccessToken;
var SDKToken = (function () {
    function SDKToken(data) {
        this.id = null;
        this.ttl = null;
        this.issuedAt = null;
        this.created = null;
        this.userId = null;
        this.user = null;
        this.rememberMe = null;
        Object.assign(this, data);
    }
    return SDKToken;
}());
exports.SDKToken = SDKToken;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQmFzZU1vZGVscy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkJhc2VNb2RlbHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsb0JBQW9COztBQXNCcEI7SUFNRSxxQkFBWSxJQUEyQjtRQUx2QyxPQUFFLEdBQVcsRUFBRSxDQUFDO1FBQ2hCLFFBQUcsR0FBVyxPQUFPLENBQUM7UUFDdEIsWUFBTyxHQUFTLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzVCLFdBQU0sR0FBVyxDQUFDLENBQUM7UUFDbkIsU0FBSSxHQUFRLElBQUksQ0FBQztRQUVmLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzVCLENBQUM7SUFDRDs7O09BR0c7SUFDVyx3QkFBWSxHQUExQjtRQUNFLE1BQU0sQ0FBQyxhQUFhLENBQUM7SUFDdkIsQ0FBQztJQUNEOzs7OztPQUtHO0lBQ1csbUJBQU8sR0FBckIsVUFBc0IsSUFBMEI7UUFDOUMsTUFBTSxDQUFDLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFDRDs7Ozs7O09BTUc7SUFDVyw4QkFBa0IsR0FBaEM7UUFDRSxNQUFNLENBQUM7WUFDTCxJQUFJLEVBQUUsYUFBYTtZQUNuQixNQUFNLEVBQUUsY0FBYztZQUN0QixVQUFVLEVBQUU7Z0JBQ1YsRUFBRSxFQUFFO29CQUNGLElBQUksRUFBRSxJQUFJO29CQUNWLElBQUksRUFBRSxRQUFRO2lCQUNmO2dCQUNELEdBQUcsRUFBRTtvQkFDSCxJQUFJLEVBQUUsS0FBSztvQkFDWCxJQUFJLEVBQUUsUUFBUTtvQkFDZCxPQUFPLEVBQUUsT0FBTztpQkFDakI7Z0JBQ0QsT0FBTyxFQUFFO29CQUNQLElBQUksRUFBRSxTQUFTO29CQUNmLElBQUksRUFBRSxNQUFNO29CQUNaLE9BQU8sRUFBRSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUM7aUJBQ3JCO2dCQUNELE1BQU0sRUFBRTtvQkFDTixJQUFJLEVBQUUsUUFBUTtvQkFDZCxJQUFJLEVBQUUsUUFBUTtpQkFDZjthQUNGO1lBQ0QsU0FBUyxFQUFFO2dCQUNULElBQUksRUFBRTtvQkFDSixJQUFJLEVBQUUsTUFBTTtvQkFDWixJQUFJLEVBQUUsTUFBTTtvQkFDWixLQUFLLEVBQUUsTUFBTTtpQkFDZDthQUNGO1NBQ0YsQ0FBQTtJQUNILENBQUM7SUFDSCxrQkFBQztBQUFELENBQUMsQUFqRUQsSUFpRUM7QUFqRVksa0NBQVc7QUFtRXhCO0lBUUksa0JBQVksSUFBMkI7UUFQdkMsT0FBRSxHQUFRLElBQUksQ0FBQztRQUNmLFFBQUcsR0FBVyxJQUFJLENBQUM7UUFDbkIsYUFBUSxHQUFRLElBQUksQ0FBQztRQUNyQixZQUFPLEdBQVEsSUFBSSxDQUFDO1FBQ3BCLFdBQU0sR0FBUSxJQUFJLENBQUM7UUFDbkIsU0FBSSxHQUFRLElBQUksQ0FBQztRQUNqQixlQUFVLEdBQVksSUFBSSxDQUFDO1FBRXZCLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzlCLENBQUM7SUFDTCxlQUFDO0FBQUQsQ0FBQyxBQVhELElBV0M7QUFYWSw0QkFBUSIsInNvdXJjZXNDb250ZW50IjpbIi8qIHRzbGludDpkaXNhYmxlICovXG5cbmRlY2xhcmUgdmFyIE9iamVjdDogYW55O1xuZXhwb3J0IGludGVyZmFjZSBMb29wQmFja0ZpbHRlciB7XG4gIGZpZWxkcz86IGFueTtcbiAgaW5jbHVkZT86IGFueTtcbiAgbGltaXQ/OiBhbnk7XG4gIG9yZGVyPzogYW55O1xuICBza2lwPzogYW55O1xuICBvZmZzZXQ/OiBhbnk7XG4gIHdoZXJlPzogYW55O1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIEFjY2Vzc1Rva2VuSW50ZXJmYWNlIHtcbiAgICBpZD86IHN0cmluZztcbiAgICB0dGw/OiBudW1iZXI7XG4gICAgaXNzdWVkQXQ/OiBhbnk7XG4gICAgY3JlYXRlZDogYW55O1xuICAgIHVzZXJJZD86IG51bWJlcjtcbiAgICByZW1lbWJlck1lPzogYm9vbGVhbjtcbn1cblxuZXhwb3J0IGNsYXNzIEFjY2Vzc1Rva2VuIGltcGxlbWVudHMgQWNjZXNzVG9rZW5JbnRlcmZhY2Uge1xuICBpZDogc3RyaW5nID0gJyc7XG4gIHR0bDogbnVtYmVyID0gMTIwOTYwMDtcbiAgY3JlYXRlZDogRGF0ZSA9IG5ldyBEYXRlKDApO1xuICB1c2VySWQ6IG51bWJlciA9IDA7XG4gIHVzZXI6IGFueSA9IG51bGw7XG4gIGNvbnN0cnVjdG9yKGRhdGE/OiBBY2Nlc3NUb2tlbkludGVyZmFjZSkge1xuICAgIE9iamVjdC5hc3NpZ24odGhpcywgZGF0YSk7XG4gIH1cbiAgLyoqXG4gICAqIFRoZSBuYW1lIG9mIHRoZSBtb2RlbCByZXByZXNlbnRlZCBieSB0aGlzICRyZXNvdXJjZSxcbiAgICogaS5lLiBgQWNjZXNzVG9rZW5gLlxuICAgKi9cbiAgcHVibGljIHN0YXRpYyBnZXRNb2RlbE5hbWUoKSB7XG4gICAgcmV0dXJuIFwiQWNjZXNzVG9rZW5cIjtcbiAgfVxuICAvKipcbiAgKiBAbWV0aG9kIGZhY3RvcnlcbiAgKiBAYXV0aG9yIEpvbmF0aGFuIENhc2FycnViaWFzXG4gICogQGxpY2Vuc2UgTUlUXG4gICogVGhpcyBtZXRob2QgY3JlYXRlcyBhbiBpbnN0YW5jZSBvZiBBY2Nlc3NUb2tlbiBmb3IgZHluYW1pYyBwdXJwb3Nlcy5cbiAgKiovXG4gIHB1YmxpYyBzdGF0aWMgZmFjdG9yeShkYXRhOiBBY2Nlc3NUb2tlbkludGVyZmFjZSk6IEFjY2Vzc1Rva2Vue1xuICAgIHJldHVybiBuZXcgQWNjZXNzVG9rZW4oZGF0YSk7XG4gIH0gIFxuICAvKipcbiAgKiBAbWV0aG9kIGdldE1vZGVsRGVmaW5pdGlvblxuICAqIEBhdXRob3IgSnVsaWVuIExlZHVuXG4gICogQGxpY2Vuc2UgTUlUXG4gICogVGhpcyBtZXRob2QgcmV0dXJucyBhbiBvYmplY3QgdGhhdCByZXByZXNlbnRzIHNvbWUgb2YgdGhlIG1vZGVsXG4gICogZGVmaW5pdGlvbnMuXG4gICoqL1xuICBwdWJsaWMgc3RhdGljIGdldE1vZGVsRGVmaW5pdGlvbigpIHtcbiAgICByZXR1cm4ge1xuICAgICAgbmFtZTogJ0FjY2Vzc1Rva2VuJyxcbiAgICAgIHBsdXJhbDogJ0FjY2Vzc1Rva2VucycsXG4gICAgICBwcm9wZXJ0aWVzOiB7XG4gICAgICAgIGlkOiB7XG4gICAgICAgICAgbmFtZTogJ2lkJyxcbiAgICAgICAgICB0eXBlOiAnc3RyaW5nJ1xuICAgICAgICB9LFxuICAgICAgICB0dGw6IHtcbiAgICAgICAgICBuYW1lOiAndHRsJyxcbiAgICAgICAgICB0eXBlOiAnbnVtYmVyJyxcbiAgICAgICAgICBkZWZhdWx0OiAxMjA5NjAwXG4gICAgICAgIH0sXG4gICAgICAgIGNyZWF0ZWQ6IHtcbiAgICAgICAgICBuYW1lOiAnY3JlYXRlZCcsXG4gICAgICAgICAgdHlwZTogJ0RhdGUnLFxuICAgICAgICAgIGRlZmF1bHQ6IG5ldyBEYXRlKDApXG4gICAgICAgIH0sXG4gICAgICAgIHVzZXJJZDoge1xuICAgICAgICAgIG5hbWU6ICd1c2VySWQnLFxuICAgICAgICAgIHR5cGU6ICdudW1iZXInXG4gICAgICAgIH0sXG4gICAgICB9LFxuICAgICAgcmVsYXRpb25zOiB7XG4gICAgICAgIHVzZXI6IHtcbiAgICAgICAgICBuYW1lOiAndXNlcicsXG4gICAgICAgICAgdHlwZTogJ1VzZXInLFxuICAgICAgICAgIG1vZGVsOiAnVXNlcidcbiAgICAgICAgfSxcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxuZXhwb3J0IGNsYXNzIFNES1Rva2VuIGltcGxlbWVudHMgQWNjZXNzVG9rZW5JbnRlcmZhY2Uge1xuICAgIGlkOiBhbnkgPSBudWxsO1xuICAgIHR0bDogbnVtYmVyID0gbnVsbDtcbiAgICBpc3N1ZWRBdDogYW55ID0gbnVsbDtcbiAgICBjcmVhdGVkOiBhbnkgPSBudWxsO1xuICAgIHVzZXJJZDogYW55ID0gbnVsbDtcbiAgICB1c2VyOiBhbnkgPSBudWxsO1xuICAgIHJlbWVtYmVyTWU6IGJvb2xlYW4gPSBudWxsO1xuICAgIGNvbnN0cnVjdG9yKGRhdGE/OiBBY2Nlc3NUb2tlbkludGVyZmFjZSkge1xuICAgICAgICBPYmplY3QuYXNzaWduKHRoaXMsIGRhdGEpO1xuICAgIH1cbn1cblxuZXhwb3J0IGludGVyZmFjZSBHZW9Qb2ludCAge1xuICAgIGxhdDogbnVtYmVyO1xuICAgIGxuZzogbnVtYmVyO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIFN0YXRGaWx0ZXIge1xuICAgIHJhbmdlOiBzdHJpbmcsXG4gICAgY3VzdG9tPzoge1xuICAgICAgc3RhcnQ6IHN0cmluZyxcbiAgICAgIGVuZDogc3RyaW5nXG4gICAgfSxcbiAgICB3aGVyZT86IHt9LFxuICAgIGdyb3VwQnk/OiBzdHJpbmdcbn1cbiJdfQ==