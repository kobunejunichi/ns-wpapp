/* tslint:disable */
"use strict";
var Group = (function () {
    function Group(data) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Group`.
     */
    Group.getModelName = function () {
        return "Group";
    };
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Group for dynamic purposes.
    **/
    Group.factory = function (data) {
        return new Group(data);
    };
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    Group.getModelDefinition = function () {
        return {
            name: 'Group',
            plural: 'Groups',
            properties: {
                name: {
                    name: 'name',
                    type: 'string'
                },
                designId: {
                    name: 'designId',
                    type: 'string',
                    default: 'temp0001-01'
                },
                pages: {
                    name: 'pages',
                    type: 'Array&lt;any&gt;',
                    default: []
                },
                members: {
                    name: 'members',
                    type: 'Array&lt;any&gt;',
                    default: []
                },
                url: {
                    name: 'url',
                    type: 'string'
                },
                password: {
                    name: 'password',
                    type: 'string'
                },
                statusDeleted: {
                    name: 'statusDeleted',
                    type: 'string',
                    default: '0'
                },
                id: {
                    name: 'id',
                    type: 'any'
                },
                createdAt: {
                    name: 'createdAt',
                    type: 'Date',
                    default: new Date(0)
                },
                createdBy: {
                    name: 'createdBy',
                    type: 'string'
                },
                modifiedAt: {
                    name: 'modifiedAt',
                    type: 'Date',
                    default: new Date(0)
                },
                modifiedBy: {
                    name: 'modifiedBy',
                    type: 'string'
                },
            },
            relations: {
                pageList: {
                    name: 'pageList',
                    type: 'any[]',
                    model: ''
                },
                memberList: {
                    name: 'memberList',
                    type: 'any[]',
                    model: ''
                },
            }
        };
    };
    return Group;
}());
exports.Group = Group;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiR3JvdXAuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJHcm91cC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxvQkFBb0I7O0FBb0JwQjtJQWVFLGVBQVksSUFBcUI7UUFDL0IsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDNUIsQ0FBQztJQUNEOzs7T0FHRztJQUNXLGtCQUFZLEdBQTFCO1FBQ0UsTUFBTSxDQUFDLE9BQU8sQ0FBQztJQUNqQixDQUFDO0lBQ0Q7Ozs7O09BS0c7SUFDVyxhQUFPLEdBQXJCLFVBQXNCLElBQW9CO1FBQ3hDLE1BQU0sQ0FBQyxJQUFJLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN6QixDQUFDO0lBQ0Q7Ozs7OztPQU1HO0lBQ1csd0JBQWtCLEdBQWhDO1FBQ0UsTUFBTSxDQUFDO1lBQ0wsSUFBSSxFQUFFLE9BQU87WUFDYixNQUFNLEVBQUUsUUFBUTtZQUNoQixVQUFVLEVBQUU7Z0JBQ1YsSUFBSSxFQUFFO29CQUNKLElBQUksRUFBRSxNQUFNO29CQUNaLElBQUksRUFBRSxRQUFRO2lCQUNmO2dCQUNELFFBQVEsRUFBRTtvQkFDUixJQUFJLEVBQUUsVUFBVTtvQkFDaEIsSUFBSSxFQUFFLFFBQVE7b0JBQ2QsT0FBTyxFQUFFLGFBQWE7aUJBQ3ZCO2dCQUNELEtBQUssRUFBRTtvQkFDTCxJQUFJLEVBQUUsT0FBTztvQkFDYixJQUFJLEVBQUUsa0JBQWtCO29CQUN4QixPQUFPLEVBQU8sRUFBRTtpQkFDakI7Z0JBQ0QsT0FBTyxFQUFFO29CQUNQLElBQUksRUFBRSxTQUFTO29CQUNmLElBQUksRUFBRSxrQkFBa0I7b0JBQ3hCLE9BQU8sRUFBTyxFQUFFO2lCQUNqQjtnQkFDRCxHQUFHLEVBQUU7b0JBQ0gsSUFBSSxFQUFFLEtBQUs7b0JBQ1gsSUFBSSxFQUFFLFFBQVE7aUJBQ2Y7Z0JBQ0QsUUFBUSxFQUFFO29CQUNSLElBQUksRUFBRSxVQUFVO29CQUNoQixJQUFJLEVBQUUsUUFBUTtpQkFDZjtnQkFDRCxhQUFhLEVBQUU7b0JBQ2IsSUFBSSxFQUFFLGVBQWU7b0JBQ3JCLElBQUksRUFBRSxRQUFRO29CQUNkLE9BQU8sRUFBRSxHQUFHO2lCQUNiO2dCQUNELEVBQUUsRUFBRTtvQkFDRixJQUFJLEVBQUUsSUFBSTtvQkFDVixJQUFJLEVBQUUsS0FBSztpQkFDWjtnQkFDRCxTQUFTLEVBQUU7b0JBQ1QsSUFBSSxFQUFFLFdBQVc7b0JBQ2pCLElBQUksRUFBRSxNQUFNO29CQUNaLE9BQU8sRUFBRSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUM7aUJBQ3JCO2dCQUNELFNBQVMsRUFBRTtvQkFDVCxJQUFJLEVBQUUsV0FBVztvQkFDakIsSUFBSSxFQUFFLFFBQVE7aUJBQ2Y7Z0JBQ0QsVUFBVSxFQUFFO29CQUNWLElBQUksRUFBRSxZQUFZO29CQUNsQixJQUFJLEVBQUUsTUFBTTtvQkFDWixPQUFPLEVBQUUsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO2lCQUNyQjtnQkFDRCxVQUFVLEVBQUU7b0JBQ1YsSUFBSSxFQUFFLFlBQVk7b0JBQ2xCLElBQUksRUFBRSxRQUFRO2lCQUNmO2FBQ0Y7WUFDRCxTQUFTLEVBQUU7Z0JBQ1QsUUFBUSxFQUFFO29CQUNSLElBQUksRUFBRSxVQUFVO29CQUNoQixJQUFJLEVBQUUsT0FBTztvQkFDYixLQUFLLEVBQUUsRUFBRTtpQkFDVjtnQkFDRCxVQUFVLEVBQUU7b0JBQ1YsSUFBSSxFQUFFLFlBQVk7b0JBQ2xCLElBQUksRUFBRSxPQUFPO29CQUNiLEtBQUssRUFBRSxFQUFFO2lCQUNWO2FBQ0Y7U0FDRixDQUFBO0lBQ0gsQ0FBQztJQUNILFlBQUM7QUFBRCxDQUFDLEFBbkhELElBbUhDO0FBbkhZLHNCQUFLIiwic291cmNlc0NvbnRlbnQiOlsiLyogdHNsaW50OmRpc2FibGUgKi9cblxuZGVjbGFyZSB2YXIgT2JqZWN0OiBhbnk7XG5leHBvcnQgaW50ZXJmYWNlIEdyb3VwSW50ZXJmYWNlIHtcbiAgbmFtZT86IHN0cmluZztcbiAgZGVzaWduSWQ/OiBzdHJpbmc7XG4gIHBhZ2VzPzogQXJyYXk8YW55PjtcbiAgbWVtYmVycz86IEFycmF5PGFueT47XG4gIHVybD86IHN0cmluZztcbiAgcGFzc3dvcmQ/OiBzdHJpbmc7XG4gIHN0YXR1c0RlbGV0ZWQ/OiBzdHJpbmc7XG4gIGlkPzogYW55O1xuICBjcmVhdGVkQXQ/OiBEYXRlO1xuICBjcmVhdGVkQnk/OiBzdHJpbmc7XG4gIG1vZGlmaWVkQXQ/OiBEYXRlO1xuICBtb2RpZmllZEJ5Pzogc3RyaW5nO1xuICBwYWdlTGlzdD86IGFueVtdO1xuICBtZW1iZXJMaXN0PzogYW55W107XG59XG5cbmV4cG9ydCBjbGFzcyBHcm91cCBpbXBsZW1lbnRzIEdyb3VwSW50ZXJmYWNlIHtcbiAgbmFtZTogc3RyaW5nO1xuICBkZXNpZ25JZDogc3RyaW5nO1xuICBwYWdlczogQXJyYXk8YW55PjtcbiAgbWVtYmVyczogQXJyYXk8YW55PjtcbiAgdXJsOiBzdHJpbmc7XG4gIHBhc3N3b3JkOiBzdHJpbmc7XG4gIHN0YXR1c0RlbGV0ZWQ6IHN0cmluZztcbiAgaWQ6IGFueTtcbiAgY3JlYXRlZEF0OiBEYXRlO1xuICBjcmVhdGVkQnk6IHN0cmluZztcbiAgbW9kaWZpZWRBdDogRGF0ZTtcbiAgbW9kaWZpZWRCeTogc3RyaW5nO1xuICBwYWdlTGlzdDogYW55W107XG4gIG1lbWJlckxpc3Q6IGFueVtdO1xuICBjb25zdHJ1Y3RvcihkYXRhPzogR3JvdXBJbnRlcmZhY2UpIHtcbiAgICBPYmplY3QuYXNzaWduKHRoaXMsIGRhdGEpO1xuICB9XG4gIC8qKlxuICAgKiBUaGUgbmFtZSBvZiB0aGUgbW9kZWwgcmVwcmVzZW50ZWQgYnkgdGhpcyAkcmVzb3VyY2UsXG4gICAqIGkuZS4gYEdyb3VwYC5cbiAgICovXG4gIHB1YmxpYyBzdGF0aWMgZ2V0TW9kZWxOYW1lKCkge1xuICAgIHJldHVybiBcIkdyb3VwXCI7XG4gIH1cbiAgLyoqXG4gICogQG1ldGhvZCBmYWN0b3J5XG4gICogQGF1dGhvciBKb25hdGhhbiBDYXNhcnJ1Ymlhc1xuICAqIEBsaWNlbnNlIE1JVFxuICAqIFRoaXMgbWV0aG9kIGNyZWF0ZXMgYW4gaW5zdGFuY2Ugb2YgR3JvdXAgZm9yIGR5bmFtaWMgcHVycG9zZXMuXG4gICoqL1xuICBwdWJsaWMgc3RhdGljIGZhY3RvcnkoZGF0YTogR3JvdXBJbnRlcmZhY2UpOiBHcm91cHtcbiAgICByZXR1cm4gbmV3IEdyb3VwKGRhdGEpO1xuICB9ICBcbiAgLyoqXG4gICogQG1ldGhvZCBnZXRNb2RlbERlZmluaXRpb25cbiAgKiBAYXV0aG9yIEp1bGllbiBMZWR1blxuICAqIEBsaWNlbnNlIE1JVFxuICAqIFRoaXMgbWV0aG9kIHJldHVybnMgYW4gb2JqZWN0IHRoYXQgcmVwcmVzZW50cyBzb21lIG9mIHRoZSBtb2RlbFxuICAqIGRlZmluaXRpb25zLlxuICAqKi9cbiAgcHVibGljIHN0YXRpYyBnZXRNb2RlbERlZmluaXRpb24oKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIG5hbWU6ICdHcm91cCcsXG4gICAgICBwbHVyYWw6ICdHcm91cHMnLFxuICAgICAgcHJvcGVydGllczoge1xuICAgICAgICBuYW1lOiB7XG4gICAgICAgICAgbmFtZTogJ25hbWUnLFxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnXG4gICAgICAgIH0sXG4gICAgICAgIGRlc2lnbklkOiB7XG4gICAgICAgICAgbmFtZTogJ2Rlc2lnbklkJyxcbiAgICAgICAgICB0eXBlOiAnc3RyaW5nJyxcbiAgICAgICAgICBkZWZhdWx0OiAndGVtcDAwMDEtMDEnXG4gICAgICAgIH0sXG4gICAgICAgIHBhZ2VzOiB7XG4gICAgICAgICAgbmFtZTogJ3BhZ2VzJyxcbiAgICAgICAgICB0eXBlOiAnQXJyYXkmbHQ7YW55Jmd0OycsXG4gICAgICAgICAgZGVmYXVsdDogPGFueT5bXVxuICAgICAgICB9LFxuICAgICAgICBtZW1iZXJzOiB7XG4gICAgICAgICAgbmFtZTogJ21lbWJlcnMnLFxuICAgICAgICAgIHR5cGU6ICdBcnJheSZsdDthbnkmZ3Q7JyxcbiAgICAgICAgICBkZWZhdWx0OiA8YW55PltdXG4gICAgICAgIH0sXG4gICAgICAgIHVybDoge1xuICAgICAgICAgIG5hbWU6ICd1cmwnLFxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnXG4gICAgICAgIH0sXG4gICAgICAgIHBhc3N3b3JkOiB7XG4gICAgICAgICAgbmFtZTogJ3Bhc3N3b3JkJyxcbiAgICAgICAgICB0eXBlOiAnc3RyaW5nJ1xuICAgICAgICB9LFxuICAgICAgICBzdGF0dXNEZWxldGVkOiB7XG4gICAgICAgICAgbmFtZTogJ3N0YXR1c0RlbGV0ZWQnLFxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnLFxuICAgICAgICAgIGRlZmF1bHQ6ICcwJ1xuICAgICAgICB9LFxuICAgICAgICBpZDoge1xuICAgICAgICAgIG5hbWU6ICdpZCcsXG4gICAgICAgICAgdHlwZTogJ2FueSdcbiAgICAgICAgfSxcbiAgICAgICAgY3JlYXRlZEF0OiB7XG4gICAgICAgICAgbmFtZTogJ2NyZWF0ZWRBdCcsXG4gICAgICAgICAgdHlwZTogJ0RhdGUnLFxuICAgICAgICAgIGRlZmF1bHQ6IG5ldyBEYXRlKDApXG4gICAgICAgIH0sXG4gICAgICAgIGNyZWF0ZWRCeToge1xuICAgICAgICAgIG5hbWU6ICdjcmVhdGVkQnknLFxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnXG4gICAgICAgIH0sXG4gICAgICAgIG1vZGlmaWVkQXQ6IHtcbiAgICAgICAgICBuYW1lOiAnbW9kaWZpZWRBdCcsXG4gICAgICAgICAgdHlwZTogJ0RhdGUnLFxuICAgICAgICAgIGRlZmF1bHQ6IG5ldyBEYXRlKDApXG4gICAgICAgIH0sXG4gICAgICAgIG1vZGlmaWVkQnk6IHtcbiAgICAgICAgICBuYW1lOiAnbW9kaWZpZWRCeScsXG4gICAgICAgICAgdHlwZTogJ3N0cmluZydcbiAgICAgICAgfSxcbiAgICAgIH0sXG4gICAgICByZWxhdGlvbnM6IHtcbiAgICAgICAgcGFnZUxpc3Q6IHtcbiAgICAgICAgICBuYW1lOiAncGFnZUxpc3QnLFxuICAgICAgICAgIHR5cGU6ICdhbnlbXScsXG4gICAgICAgICAgbW9kZWw6ICcnXG4gICAgICAgIH0sXG4gICAgICAgIG1lbWJlckxpc3Q6IHtcbiAgICAgICAgICBuYW1lOiAnbWVtYmVyTGlzdCcsXG4gICAgICAgICAgdHlwZTogJ2FueVtdJyxcbiAgICAgICAgICBtb2RlbDogJydcbiAgICAgICAgfSxcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cbiJdfQ==