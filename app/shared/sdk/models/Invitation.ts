/* tslint:disable */

declare var Object: any;
export interface InvitationInterface {
  groupId?: string;
  userId?: string;
  type?: string;
  expireDate?: Date;
  id?: any;
  createdAt?: Date;
  createdBy?: string;
  modifiedAt?: Date;
  modifiedBy?: string;
}

export class Invitation implements InvitationInterface {
  groupId: string;
  userId: string;
  type: string;
  expireDate: Date;
  id: any;
  createdAt: Date;
  createdBy: string;
  modifiedAt: Date;
  modifiedBy: string;
  constructor(data?: InvitationInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Invitation`.
   */
  public static getModelName() {
    return "Invitation";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Invitation for dynamic purposes.
  **/
  public static factory(data: InvitationInterface): Invitation{
    return new Invitation(data);
  }  
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Invitation',
      plural: 'Invitations',
      properties: {
        groupId: {
          name: 'groupId',
          type: 'string'
        },
        userId: {
          name: 'userId',
          type: 'string'
        },
        type: {
          name: 'type',
          type: 'string'
        },
        expireDate: {
          name: 'expireDate',
          type: 'Date'
        },
        id: {
          name: 'id',
          type: 'any'
        },
        createdAt: {
          name: 'createdAt',
          type: 'Date',
          default: new Date(0)
        },
        createdBy: {
          name: 'createdBy',
          type: 'string'
        },
        modifiedAt: {
          name: 'modifiedAt',
          type: 'Date',
          default: new Date(0)
        },
        modifiedBy: {
          name: 'modifiedBy',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
