/* tslint:disable */
"use strict";
var Staff = (function () {
    function Staff(data) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Staff`.
     */
    Staff.getModelName = function () {
        return "Staff";
    };
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Staff for dynamic purposes.
    **/
    Staff.factory = function (data) {
        return new Staff(data);
    };
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    Staff.getModelDefinition = function () {
        return {
            name: 'Staff',
            plural: 'Staffs',
            properties: {
                name: {
                    name: 'name',
                    type: 'string'
                },
                email: {
                    name: 'email',
                    type: 'string'
                },
                password: {
                    name: 'password',
                    type: 'string'
                },
                passwordStatus: {
                    name: 'passwordStatus',
                    type: 'string',
                    default: '0'
                },
                tmpPasswordExipireDate: {
                    name: 'tmpPasswordExipireDate',
                    type: 'Date'
                },
                auth: {
                    name: 'auth',
                    type: 'any'
                },
                revokeCount: {
                    name: 'revokeCount',
                    type: 'number',
                    default: 0
                },
                statusValid: {
                    name: 'statusValid',
                    type: 'string',
                    default: '0'
                },
                statusDeleted: {
                    name: 'statusDeleted',
                    type: 'string',
                    default: '0'
                },
                dateLastLogined: {
                    name: 'dateLastLogined',
                    type: 'string'
                },
                createdAt: {
                    name: 'createdAt',
                    type: 'Date',
                    default: new Date(0)
                },
                createdBy: {
                    name: 'createdBy',
                    type: 'string'
                },
                updatedAt: {
                    name: 'updatedAt',
                    type: 'Date'
                },
                updatedBy: {
                    name: 'updatedBy',
                    type: 'string'
                },
                realm: {
                    name: 'realm',
                    type: 'string'
                },
                username: {
                    name: 'username',
                    type: 'string'
                },
                credentials: {
                    name: 'credentials',
                    type: 'any'
                },
                challenges: {
                    name: 'challenges',
                    type: 'any'
                },
                emailVerified: {
                    name: 'emailVerified',
                    type: 'boolean'
                },
                verificationToken: {
                    name: 'verificationToken',
                    type: 'string'
                },
                status: {
                    name: 'status',
                    type: 'string'
                },
                created: {
                    name: 'created',
                    type: 'Date'
                },
                lastUpdated: {
                    name: 'lastUpdated',
                    type: 'Date'
                },
                id: {
                    name: 'id',
                    type: 'any'
                },
                modifiedAt: {
                    name: 'modifiedAt',
                    type: 'Date',
                    default: new Date(0)
                },
                modifiedBy: {
                    name: 'modifiedBy',
                    type: 'string'
                },
            },
            relations: {
                accessTokens: {
                    name: 'accessTokens',
                    type: 'any[]',
                    model: ''
                },
            }
        };
    };
    return Staff;
}());
exports.Staff = Staff;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU3RhZmYuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJTdGFmZi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxvQkFBb0I7O0FBZ0NwQjtJQTJCRSxlQUFZLElBQXFCO1FBQy9CLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzVCLENBQUM7SUFDRDs7O09BR0c7SUFDVyxrQkFBWSxHQUExQjtRQUNFLE1BQU0sQ0FBQyxPQUFPLENBQUM7SUFDakIsQ0FBQztJQUNEOzs7OztPQUtHO0lBQ1csYUFBTyxHQUFyQixVQUFzQixJQUFvQjtRQUN4QyxNQUFNLENBQUMsSUFBSSxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDekIsQ0FBQztJQUNEOzs7Ozs7T0FNRztJQUNXLHdCQUFrQixHQUFoQztRQUNFLE1BQU0sQ0FBQztZQUNMLElBQUksRUFBRSxPQUFPO1lBQ2IsTUFBTSxFQUFFLFFBQVE7WUFDaEIsVUFBVSxFQUFFO2dCQUNWLElBQUksRUFBRTtvQkFDSixJQUFJLEVBQUUsTUFBTTtvQkFDWixJQUFJLEVBQUUsUUFBUTtpQkFDZjtnQkFDRCxLQUFLLEVBQUU7b0JBQ0wsSUFBSSxFQUFFLE9BQU87b0JBQ2IsSUFBSSxFQUFFLFFBQVE7aUJBQ2Y7Z0JBQ0QsUUFBUSxFQUFFO29CQUNSLElBQUksRUFBRSxVQUFVO29CQUNoQixJQUFJLEVBQUUsUUFBUTtpQkFDZjtnQkFDRCxjQUFjLEVBQUU7b0JBQ2QsSUFBSSxFQUFFLGdCQUFnQjtvQkFDdEIsSUFBSSxFQUFFLFFBQVE7b0JBQ2QsT0FBTyxFQUFFLEdBQUc7aUJBQ2I7Z0JBQ0Qsc0JBQXNCLEVBQUU7b0JBQ3RCLElBQUksRUFBRSx3QkFBd0I7b0JBQzlCLElBQUksRUFBRSxNQUFNO2lCQUNiO2dCQUNELElBQUksRUFBRTtvQkFDSixJQUFJLEVBQUUsTUFBTTtvQkFDWixJQUFJLEVBQUUsS0FBSztpQkFDWjtnQkFDRCxXQUFXLEVBQUU7b0JBQ1gsSUFBSSxFQUFFLGFBQWE7b0JBQ25CLElBQUksRUFBRSxRQUFRO29CQUNkLE9BQU8sRUFBRSxDQUFDO2lCQUNYO2dCQUNELFdBQVcsRUFBRTtvQkFDWCxJQUFJLEVBQUUsYUFBYTtvQkFDbkIsSUFBSSxFQUFFLFFBQVE7b0JBQ2QsT0FBTyxFQUFFLEdBQUc7aUJBQ2I7Z0JBQ0QsYUFBYSxFQUFFO29CQUNiLElBQUksRUFBRSxlQUFlO29CQUNyQixJQUFJLEVBQUUsUUFBUTtvQkFDZCxPQUFPLEVBQUUsR0FBRztpQkFDYjtnQkFDRCxlQUFlLEVBQUU7b0JBQ2YsSUFBSSxFQUFFLGlCQUFpQjtvQkFDdkIsSUFBSSxFQUFFLFFBQVE7aUJBQ2Y7Z0JBQ0QsU0FBUyxFQUFFO29CQUNULElBQUksRUFBRSxXQUFXO29CQUNqQixJQUFJLEVBQUUsTUFBTTtvQkFDWixPQUFPLEVBQUUsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO2lCQUNyQjtnQkFDRCxTQUFTLEVBQUU7b0JBQ1QsSUFBSSxFQUFFLFdBQVc7b0JBQ2pCLElBQUksRUFBRSxRQUFRO2lCQUNmO2dCQUNELFNBQVMsRUFBRTtvQkFDVCxJQUFJLEVBQUUsV0FBVztvQkFDakIsSUFBSSxFQUFFLE1BQU07aUJBQ2I7Z0JBQ0QsU0FBUyxFQUFFO29CQUNULElBQUksRUFBRSxXQUFXO29CQUNqQixJQUFJLEVBQUUsUUFBUTtpQkFDZjtnQkFDRCxLQUFLLEVBQUU7b0JBQ0wsSUFBSSxFQUFFLE9BQU87b0JBQ2IsSUFBSSxFQUFFLFFBQVE7aUJBQ2Y7Z0JBQ0QsUUFBUSxFQUFFO29CQUNSLElBQUksRUFBRSxVQUFVO29CQUNoQixJQUFJLEVBQUUsUUFBUTtpQkFDZjtnQkFDRCxXQUFXLEVBQUU7b0JBQ1gsSUFBSSxFQUFFLGFBQWE7b0JBQ25CLElBQUksRUFBRSxLQUFLO2lCQUNaO2dCQUNELFVBQVUsRUFBRTtvQkFDVixJQUFJLEVBQUUsWUFBWTtvQkFDbEIsSUFBSSxFQUFFLEtBQUs7aUJBQ1o7Z0JBQ0QsYUFBYSxFQUFFO29CQUNiLElBQUksRUFBRSxlQUFlO29CQUNyQixJQUFJLEVBQUUsU0FBUztpQkFDaEI7Z0JBQ0QsaUJBQWlCLEVBQUU7b0JBQ2pCLElBQUksRUFBRSxtQkFBbUI7b0JBQ3pCLElBQUksRUFBRSxRQUFRO2lCQUNmO2dCQUNELE1BQU0sRUFBRTtvQkFDTixJQUFJLEVBQUUsUUFBUTtvQkFDZCxJQUFJLEVBQUUsUUFBUTtpQkFDZjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsSUFBSSxFQUFFLFNBQVM7b0JBQ2YsSUFBSSxFQUFFLE1BQU07aUJBQ2I7Z0JBQ0QsV0FBVyxFQUFFO29CQUNYLElBQUksRUFBRSxhQUFhO29CQUNuQixJQUFJLEVBQUUsTUFBTTtpQkFDYjtnQkFDRCxFQUFFLEVBQUU7b0JBQ0YsSUFBSSxFQUFFLElBQUk7b0JBQ1YsSUFBSSxFQUFFLEtBQUs7aUJBQ1o7Z0JBQ0QsVUFBVSxFQUFFO29CQUNWLElBQUksRUFBRSxZQUFZO29CQUNsQixJQUFJLEVBQUUsTUFBTTtvQkFDWixPQUFPLEVBQUUsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO2lCQUNyQjtnQkFDRCxVQUFVLEVBQUU7b0JBQ1YsSUFBSSxFQUFFLFlBQVk7b0JBQ2xCLElBQUksRUFBRSxRQUFRO2lCQUNmO2FBQ0Y7WUFDRCxTQUFTLEVBQUU7Z0JBQ1QsWUFBWSxFQUFFO29CQUNaLElBQUksRUFBRSxjQUFjO29CQUNwQixJQUFJLEVBQUUsT0FBTztvQkFDYixLQUFLLEVBQUUsRUFBRTtpQkFDVjthQUNGO1NBQ0YsQ0FBQTtJQUNILENBQUM7SUFDSCxZQUFDO0FBQUQsQ0FBQyxBQWxMRCxJQWtMQztBQWxMWSxzQkFBSyIsInNvdXJjZXNDb250ZW50IjpbIi8qIHRzbGludDpkaXNhYmxlICovXG5cbmRlY2xhcmUgdmFyIE9iamVjdDogYW55O1xuZXhwb3J0IGludGVyZmFjZSBTdGFmZkludGVyZmFjZSB7XG4gIG5hbWU/OiBzdHJpbmc7XG4gIGVtYWlsPzogc3RyaW5nO1xuICBwYXNzd29yZD86IHN0cmluZztcbiAgcGFzc3dvcmRTdGF0dXM/OiBzdHJpbmc7XG4gIHRtcFBhc3N3b3JkRXhpcGlyZURhdGU/OiBEYXRlO1xuICBhdXRoPzogYW55O1xuICByZXZva2VDb3VudD86IG51bWJlcjtcbiAgc3RhdHVzVmFsaWQ/OiBzdHJpbmc7XG4gIHN0YXR1c0RlbGV0ZWQ/OiBzdHJpbmc7XG4gIGRhdGVMYXN0TG9naW5lZD86IHN0cmluZztcbiAgY3JlYXRlZEF0PzogRGF0ZTtcbiAgY3JlYXRlZEJ5Pzogc3RyaW5nO1xuICB1cGRhdGVkQXQ/OiBEYXRlO1xuICB1cGRhdGVkQnk/OiBzdHJpbmc7XG4gIHJlYWxtPzogc3RyaW5nO1xuICB1c2VybmFtZT86IHN0cmluZztcbiAgY2hhbGxlbmdlcz86IGFueTtcbiAgZW1haWxWZXJpZmllZD86IGJvb2xlYW47XG4gIHZlcmlmaWNhdGlvblRva2VuPzogc3RyaW5nO1xuICBzdGF0dXM/OiBzdHJpbmc7XG4gIGNyZWF0ZWQ/OiBEYXRlO1xuICBsYXN0VXBkYXRlZD86IERhdGU7XG4gIGlkPzogYW55O1xuICBtb2RpZmllZEF0PzogRGF0ZTtcbiAgbW9kaWZpZWRCeT86IHN0cmluZztcbiAgYWNjZXNzVG9rZW5zPzogYW55W107XG59XG5cbmV4cG9ydCBjbGFzcyBTdGFmZiBpbXBsZW1lbnRzIFN0YWZmSW50ZXJmYWNlIHtcbiAgbmFtZTogc3RyaW5nO1xuICBlbWFpbDogc3RyaW5nO1xuICBwYXNzd29yZDogc3RyaW5nO1xuICBwYXNzd29yZFN0YXR1czogc3RyaW5nO1xuICB0bXBQYXNzd29yZEV4aXBpcmVEYXRlOiBEYXRlO1xuICBhdXRoOiBhbnk7XG4gIHJldm9rZUNvdW50OiBudW1iZXI7XG4gIHN0YXR1c1ZhbGlkOiBzdHJpbmc7XG4gIHN0YXR1c0RlbGV0ZWQ6IHN0cmluZztcbiAgZGF0ZUxhc3RMb2dpbmVkOiBzdHJpbmc7XG4gIGNyZWF0ZWRBdDogRGF0ZTtcbiAgY3JlYXRlZEJ5OiBzdHJpbmc7XG4gIHVwZGF0ZWRBdDogRGF0ZTtcbiAgdXBkYXRlZEJ5OiBzdHJpbmc7XG4gIHJlYWxtOiBzdHJpbmc7XG4gIHVzZXJuYW1lOiBzdHJpbmc7XG4gIGNoYWxsZW5nZXM6IGFueTtcbiAgZW1haWxWZXJpZmllZDogYm9vbGVhbjtcbiAgdmVyaWZpY2F0aW9uVG9rZW46IHN0cmluZztcbiAgc3RhdHVzOiBzdHJpbmc7XG4gIGNyZWF0ZWQ6IERhdGU7XG4gIGxhc3RVcGRhdGVkOiBEYXRlO1xuICBpZDogYW55O1xuICBtb2RpZmllZEF0OiBEYXRlO1xuICBtb2RpZmllZEJ5OiBzdHJpbmc7XG4gIGFjY2Vzc1Rva2VuczogYW55W107XG4gIGNvbnN0cnVjdG9yKGRhdGE/OiBTdGFmZkludGVyZmFjZSkge1xuICAgIE9iamVjdC5hc3NpZ24odGhpcywgZGF0YSk7XG4gIH1cbiAgLyoqXG4gICAqIFRoZSBuYW1lIG9mIHRoZSBtb2RlbCByZXByZXNlbnRlZCBieSB0aGlzICRyZXNvdXJjZSxcbiAgICogaS5lLiBgU3RhZmZgLlxuICAgKi9cbiAgcHVibGljIHN0YXRpYyBnZXRNb2RlbE5hbWUoKSB7XG4gICAgcmV0dXJuIFwiU3RhZmZcIjtcbiAgfVxuICAvKipcbiAgKiBAbWV0aG9kIGZhY3RvcnlcbiAgKiBAYXV0aG9yIEpvbmF0aGFuIENhc2FycnViaWFzXG4gICogQGxpY2Vuc2UgTUlUXG4gICogVGhpcyBtZXRob2QgY3JlYXRlcyBhbiBpbnN0YW5jZSBvZiBTdGFmZiBmb3IgZHluYW1pYyBwdXJwb3Nlcy5cbiAgKiovXG4gIHB1YmxpYyBzdGF0aWMgZmFjdG9yeShkYXRhOiBTdGFmZkludGVyZmFjZSk6IFN0YWZme1xuICAgIHJldHVybiBuZXcgU3RhZmYoZGF0YSk7XG4gIH0gIFxuICAvKipcbiAgKiBAbWV0aG9kIGdldE1vZGVsRGVmaW5pdGlvblxuICAqIEBhdXRob3IgSnVsaWVuIExlZHVuXG4gICogQGxpY2Vuc2UgTUlUXG4gICogVGhpcyBtZXRob2QgcmV0dXJucyBhbiBvYmplY3QgdGhhdCByZXByZXNlbnRzIHNvbWUgb2YgdGhlIG1vZGVsXG4gICogZGVmaW5pdGlvbnMuXG4gICoqL1xuICBwdWJsaWMgc3RhdGljIGdldE1vZGVsRGVmaW5pdGlvbigpIHtcbiAgICByZXR1cm4ge1xuICAgICAgbmFtZTogJ1N0YWZmJyxcbiAgICAgIHBsdXJhbDogJ1N0YWZmcycsXG4gICAgICBwcm9wZXJ0aWVzOiB7XG4gICAgICAgIG5hbWU6IHtcbiAgICAgICAgICBuYW1lOiAnbmFtZScsXG4gICAgICAgICAgdHlwZTogJ3N0cmluZydcbiAgICAgICAgfSxcbiAgICAgICAgZW1haWw6IHtcbiAgICAgICAgICBuYW1lOiAnZW1haWwnLFxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnXG4gICAgICAgIH0sXG4gICAgICAgIHBhc3N3b3JkOiB7XG4gICAgICAgICAgbmFtZTogJ3Bhc3N3b3JkJyxcbiAgICAgICAgICB0eXBlOiAnc3RyaW5nJ1xuICAgICAgICB9LFxuICAgICAgICBwYXNzd29yZFN0YXR1czoge1xuICAgICAgICAgIG5hbWU6ICdwYXNzd29yZFN0YXR1cycsXG4gICAgICAgICAgdHlwZTogJ3N0cmluZycsXG4gICAgICAgICAgZGVmYXVsdDogJzAnXG4gICAgICAgIH0sXG4gICAgICAgIHRtcFBhc3N3b3JkRXhpcGlyZURhdGU6IHtcbiAgICAgICAgICBuYW1lOiAndG1wUGFzc3dvcmRFeGlwaXJlRGF0ZScsXG4gICAgICAgICAgdHlwZTogJ0RhdGUnXG4gICAgICAgIH0sXG4gICAgICAgIGF1dGg6IHtcbiAgICAgICAgICBuYW1lOiAnYXV0aCcsXG4gICAgICAgICAgdHlwZTogJ2FueSdcbiAgICAgICAgfSxcbiAgICAgICAgcmV2b2tlQ291bnQ6IHtcbiAgICAgICAgICBuYW1lOiAncmV2b2tlQ291bnQnLFxuICAgICAgICAgIHR5cGU6ICdudW1iZXInLFxuICAgICAgICAgIGRlZmF1bHQ6IDBcbiAgICAgICAgfSxcbiAgICAgICAgc3RhdHVzVmFsaWQ6IHtcbiAgICAgICAgICBuYW1lOiAnc3RhdHVzVmFsaWQnLFxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnLFxuICAgICAgICAgIGRlZmF1bHQ6ICcwJ1xuICAgICAgICB9LFxuICAgICAgICBzdGF0dXNEZWxldGVkOiB7XG4gICAgICAgICAgbmFtZTogJ3N0YXR1c0RlbGV0ZWQnLFxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnLFxuICAgICAgICAgIGRlZmF1bHQ6ICcwJ1xuICAgICAgICB9LFxuICAgICAgICBkYXRlTGFzdExvZ2luZWQ6IHtcbiAgICAgICAgICBuYW1lOiAnZGF0ZUxhc3RMb2dpbmVkJyxcbiAgICAgICAgICB0eXBlOiAnc3RyaW5nJ1xuICAgICAgICB9LFxuICAgICAgICBjcmVhdGVkQXQ6IHtcbiAgICAgICAgICBuYW1lOiAnY3JlYXRlZEF0JyxcbiAgICAgICAgICB0eXBlOiAnRGF0ZScsXG4gICAgICAgICAgZGVmYXVsdDogbmV3IERhdGUoMClcbiAgICAgICAgfSxcbiAgICAgICAgY3JlYXRlZEJ5OiB7XG4gICAgICAgICAgbmFtZTogJ2NyZWF0ZWRCeScsXG4gICAgICAgICAgdHlwZTogJ3N0cmluZydcbiAgICAgICAgfSxcbiAgICAgICAgdXBkYXRlZEF0OiB7XG4gICAgICAgICAgbmFtZTogJ3VwZGF0ZWRBdCcsXG4gICAgICAgICAgdHlwZTogJ0RhdGUnXG4gICAgICAgIH0sXG4gICAgICAgIHVwZGF0ZWRCeToge1xuICAgICAgICAgIG5hbWU6ICd1cGRhdGVkQnknLFxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnXG4gICAgICAgIH0sXG4gICAgICAgIHJlYWxtOiB7XG4gICAgICAgICAgbmFtZTogJ3JlYWxtJyxcbiAgICAgICAgICB0eXBlOiAnc3RyaW5nJ1xuICAgICAgICB9LFxuICAgICAgICB1c2VybmFtZToge1xuICAgICAgICAgIG5hbWU6ICd1c2VybmFtZScsXG4gICAgICAgICAgdHlwZTogJ3N0cmluZydcbiAgICAgICAgfSxcbiAgICAgICAgY3JlZGVudGlhbHM6IHtcbiAgICAgICAgICBuYW1lOiAnY3JlZGVudGlhbHMnLFxuICAgICAgICAgIHR5cGU6ICdhbnknXG4gICAgICAgIH0sXG4gICAgICAgIGNoYWxsZW5nZXM6IHtcbiAgICAgICAgICBuYW1lOiAnY2hhbGxlbmdlcycsXG4gICAgICAgICAgdHlwZTogJ2FueSdcbiAgICAgICAgfSxcbiAgICAgICAgZW1haWxWZXJpZmllZDoge1xuICAgICAgICAgIG5hbWU6ICdlbWFpbFZlcmlmaWVkJyxcbiAgICAgICAgICB0eXBlOiAnYm9vbGVhbidcbiAgICAgICAgfSxcbiAgICAgICAgdmVyaWZpY2F0aW9uVG9rZW46IHtcbiAgICAgICAgICBuYW1lOiAndmVyaWZpY2F0aW9uVG9rZW4nLFxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnXG4gICAgICAgIH0sXG4gICAgICAgIHN0YXR1czoge1xuICAgICAgICAgIG5hbWU6ICdzdGF0dXMnLFxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnXG4gICAgICAgIH0sXG4gICAgICAgIGNyZWF0ZWQ6IHtcbiAgICAgICAgICBuYW1lOiAnY3JlYXRlZCcsXG4gICAgICAgICAgdHlwZTogJ0RhdGUnXG4gICAgICAgIH0sXG4gICAgICAgIGxhc3RVcGRhdGVkOiB7XG4gICAgICAgICAgbmFtZTogJ2xhc3RVcGRhdGVkJyxcbiAgICAgICAgICB0eXBlOiAnRGF0ZSdcbiAgICAgICAgfSxcbiAgICAgICAgaWQ6IHtcbiAgICAgICAgICBuYW1lOiAnaWQnLFxuICAgICAgICAgIHR5cGU6ICdhbnknXG4gICAgICAgIH0sXG4gICAgICAgIG1vZGlmaWVkQXQ6IHtcbiAgICAgICAgICBuYW1lOiAnbW9kaWZpZWRBdCcsXG4gICAgICAgICAgdHlwZTogJ0RhdGUnLFxuICAgICAgICAgIGRlZmF1bHQ6IG5ldyBEYXRlKDApXG4gICAgICAgIH0sXG4gICAgICAgIG1vZGlmaWVkQnk6IHtcbiAgICAgICAgICBuYW1lOiAnbW9kaWZpZWRCeScsXG4gICAgICAgICAgdHlwZTogJ3N0cmluZydcbiAgICAgICAgfSxcbiAgICAgIH0sXG4gICAgICByZWxhdGlvbnM6IHtcbiAgICAgICAgYWNjZXNzVG9rZW5zOiB7XG4gICAgICAgICAgbmFtZTogJ2FjY2Vzc1Rva2VucycsXG4gICAgICAgICAgdHlwZTogJ2FueVtdJyxcbiAgICAgICAgICBtb2RlbDogJydcbiAgICAgICAgfSxcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cbiJdfQ==