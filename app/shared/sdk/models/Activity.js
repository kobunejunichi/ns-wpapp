/* tslint:disable */
"use strict";
var Activity = (function () {
    function Activity(data) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Activity`.
     */
    Activity.getModelName = function () {
        return "Activity";
    };
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Activity for dynamic purposes.
    **/
    Activity.factory = function (data) {
        return new Activity(data);
    };
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    Activity.getModelDefinition = function () {
        return {
            name: 'Activity',
            plural: 'Activities',
            properties: {
                version: {
                    name: 'version',
                    type: 'string'
                },
                groupId: {
                    name: 'groupId',
                    type: 'string'
                },
                userId: {
                    name: 'userId',
                    type: 'string'
                },
                activityType: {
                    name: 'activityType',
                    type: 'string'
                },
                users: {
                    name: 'users',
                    type: 'any'
                },
                messageOption: {
                    name: 'messageOption',
                    type: 'any'
                },
                targetInfo: {
                    name: 'targetInfo',
                    type: 'any'
                },
                activityDate: {
                    name: 'activityDate',
                    type: 'Date'
                },
                statusDeleted: {
                    name: 'statusDeleted',
                    type: 'string',
                    default: '0'
                },
                id: {
                    name: 'id',
                    type: 'any'
                },
                createdAt: {
                    name: 'createdAt',
                    type: 'Date',
                    default: new Date(0)
                },
                createdBy: {
                    name: 'createdBy',
                    type: 'string'
                },
                modifiedAt: {
                    name: 'modifiedAt',
                    type: 'Date',
                    default: new Date(0)
                },
                modifiedBy: {
                    name: 'modifiedBy',
                    type: 'string'
                },
            },
            relations: {}
        };
    };
    return Activity;
}());
exports.Activity = Activity;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQWN0aXZpdHkuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJBY3Rpdml0eS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxvQkFBb0I7O0FBb0JwQjtJQWVFLGtCQUFZLElBQXdCO1FBQ2xDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzVCLENBQUM7SUFDRDs7O09BR0c7SUFDVyxxQkFBWSxHQUExQjtRQUNFLE1BQU0sQ0FBQyxVQUFVLENBQUM7SUFDcEIsQ0FBQztJQUNEOzs7OztPQUtHO0lBQ1csZ0JBQU8sR0FBckIsVUFBc0IsSUFBdUI7UUFDM0MsTUFBTSxDQUFDLElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzVCLENBQUM7SUFDRDs7Ozs7O09BTUc7SUFDVywyQkFBa0IsR0FBaEM7UUFDRSxNQUFNLENBQUM7WUFDTCxJQUFJLEVBQUUsVUFBVTtZQUNoQixNQUFNLEVBQUUsWUFBWTtZQUNwQixVQUFVLEVBQUU7Z0JBQ1YsT0FBTyxFQUFFO29CQUNQLElBQUksRUFBRSxTQUFTO29CQUNmLElBQUksRUFBRSxRQUFRO2lCQUNmO2dCQUNELE9BQU8sRUFBRTtvQkFDUCxJQUFJLEVBQUUsU0FBUztvQkFDZixJQUFJLEVBQUUsUUFBUTtpQkFDZjtnQkFDRCxNQUFNLEVBQUU7b0JBQ04sSUFBSSxFQUFFLFFBQVE7b0JBQ2QsSUFBSSxFQUFFLFFBQVE7aUJBQ2Y7Z0JBQ0QsWUFBWSxFQUFFO29CQUNaLElBQUksRUFBRSxjQUFjO29CQUNwQixJQUFJLEVBQUUsUUFBUTtpQkFDZjtnQkFDRCxLQUFLLEVBQUU7b0JBQ0wsSUFBSSxFQUFFLE9BQU87b0JBQ2IsSUFBSSxFQUFFLEtBQUs7aUJBQ1o7Z0JBQ0QsYUFBYSxFQUFFO29CQUNiLElBQUksRUFBRSxlQUFlO29CQUNyQixJQUFJLEVBQUUsS0FBSztpQkFDWjtnQkFDRCxVQUFVLEVBQUU7b0JBQ1YsSUFBSSxFQUFFLFlBQVk7b0JBQ2xCLElBQUksRUFBRSxLQUFLO2lCQUNaO2dCQUNELFlBQVksRUFBRTtvQkFDWixJQUFJLEVBQUUsY0FBYztvQkFDcEIsSUFBSSxFQUFFLE1BQU07aUJBQ2I7Z0JBQ0QsYUFBYSxFQUFFO29CQUNiLElBQUksRUFBRSxlQUFlO29CQUNyQixJQUFJLEVBQUUsUUFBUTtvQkFDZCxPQUFPLEVBQUUsR0FBRztpQkFDYjtnQkFDRCxFQUFFLEVBQUU7b0JBQ0YsSUFBSSxFQUFFLElBQUk7b0JBQ1YsSUFBSSxFQUFFLEtBQUs7aUJBQ1o7Z0JBQ0QsU0FBUyxFQUFFO29CQUNULElBQUksRUFBRSxXQUFXO29CQUNqQixJQUFJLEVBQUUsTUFBTTtvQkFDWixPQUFPLEVBQUUsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO2lCQUNyQjtnQkFDRCxTQUFTLEVBQUU7b0JBQ1QsSUFBSSxFQUFFLFdBQVc7b0JBQ2pCLElBQUksRUFBRSxRQUFRO2lCQUNmO2dCQUNELFVBQVUsRUFBRTtvQkFDVixJQUFJLEVBQUUsWUFBWTtvQkFDbEIsSUFBSSxFQUFFLE1BQU07b0JBQ1osT0FBTyxFQUFFLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQztpQkFDckI7Z0JBQ0QsVUFBVSxFQUFFO29CQUNWLElBQUksRUFBRSxZQUFZO29CQUNsQixJQUFJLEVBQUUsUUFBUTtpQkFDZjthQUNGO1lBQ0QsU0FBUyxFQUFFLEVBQ1Y7U0FDRixDQUFBO0lBQ0gsQ0FBQztJQUNILGVBQUM7QUFBRCxDQUFDLEFBOUdELElBOEdDO0FBOUdZLDRCQUFRIiwic291cmNlc0NvbnRlbnQiOlsiLyogdHNsaW50OmRpc2FibGUgKi9cblxuZGVjbGFyZSB2YXIgT2JqZWN0OiBhbnk7XG5leHBvcnQgaW50ZXJmYWNlIEFjdGl2aXR5SW50ZXJmYWNlIHtcbiAgdmVyc2lvbj86IHN0cmluZztcbiAgZ3JvdXBJZD86IHN0cmluZztcbiAgdXNlcklkPzogc3RyaW5nO1xuICBhY3Rpdml0eVR5cGU/OiBzdHJpbmc7XG4gIHVzZXJzPzogYW55O1xuICBtZXNzYWdlT3B0aW9uPzogYW55O1xuICB0YXJnZXRJbmZvPzogYW55O1xuICBhY3Rpdml0eURhdGU/OiBEYXRlO1xuICBzdGF0dXNEZWxldGVkPzogc3RyaW5nO1xuICBpZD86IGFueTtcbiAgY3JlYXRlZEF0PzogRGF0ZTtcbiAgY3JlYXRlZEJ5Pzogc3RyaW5nO1xuICBtb2RpZmllZEF0PzogRGF0ZTtcbiAgbW9kaWZpZWRCeT86IHN0cmluZztcbn1cblxuZXhwb3J0IGNsYXNzIEFjdGl2aXR5IGltcGxlbWVudHMgQWN0aXZpdHlJbnRlcmZhY2Uge1xuICB2ZXJzaW9uOiBzdHJpbmc7XG4gIGdyb3VwSWQ6IHN0cmluZztcbiAgdXNlcklkOiBzdHJpbmc7XG4gIGFjdGl2aXR5VHlwZTogc3RyaW5nO1xuICB1c2VyczogYW55O1xuICBtZXNzYWdlT3B0aW9uOiBhbnk7XG4gIHRhcmdldEluZm86IGFueTtcbiAgYWN0aXZpdHlEYXRlOiBEYXRlO1xuICBzdGF0dXNEZWxldGVkOiBzdHJpbmc7XG4gIGlkOiBhbnk7XG4gIGNyZWF0ZWRBdDogRGF0ZTtcbiAgY3JlYXRlZEJ5OiBzdHJpbmc7XG4gIG1vZGlmaWVkQXQ6IERhdGU7XG4gIG1vZGlmaWVkQnk6IHN0cmluZztcbiAgY29uc3RydWN0b3IoZGF0YT86IEFjdGl2aXR5SW50ZXJmYWNlKSB7XG4gICAgT2JqZWN0LmFzc2lnbih0aGlzLCBkYXRhKTtcbiAgfVxuICAvKipcbiAgICogVGhlIG5hbWUgb2YgdGhlIG1vZGVsIHJlcHJlc2VudGVkIGJ5IHRoaXMgJHJlc291cmNlLFxuICAgKiBpLmUuIGBBY3Rpdml0eWAuXG4gICAqL1xuICBwdWJsaWMgc3RhdGljIGdldE1vZGVsTmFtZSgpIHtcbiAgICByZXR1cm4gXCJBY3Rpdml0eVwiO1xuICB9XG4gIC8qKlxuICAqIEBtZXRob2QgZmFjdG9yeVxuICAqIEBhdXRob3IgSm9uYXRoYW4gQ2FzYXJydWJpYXNcbiAgKiBAbGljZW5zZSBNSVRcbiAgKiBUaGlzIG1ldGhvZCBjcmVhdGVzIGFuIGluc3RhbmNlIG9mIEFjdGl2aXR5IGZvciBkeW5hbWljIHB1cnBvc2VzLlxuICAqKi9cbiAgcHVibGljIHN0YXRpYyBmYWN0b3J5KGRhdGE6IEFjdGl2aXR5SW50ZXJmYWNlKTogQWN0aXZpdHl7XG4gICAgcmV0dXJuIG5ldyBBY3Rpdml0eShkYXRhKTtcbiAgfSAgXG4gIC8qKlxuICAqIEBtZXRob2QgZ2V0TW9kZWxEZWZpbml0aW9uXG4gICogQGF1dGhvciBKdWxpZW4gTGVkdW5cbiAgKiBAbGljZW5zZSBNSVRcbiAgKiBUaGlzIG1ldGhvZCByZXR1cm5zIGFuIG9iamVjdCB0aGF0IHJlcHJlc2VudHMgc29tZSBvZiB0aGUgbW9kZWxcbiAgKiBkZWZpbml0aW9ucy5cbiAgKiovXG4gIHB1YmxpYyBzdGF0aWMgZ2V0TW9kZWxEZWZpbml0aW9uKCkge1xuICAgIHJldHVybiB7XG4gICAgICBuYW1lOiAnQWN0aXZpdHknLFxuICAgICAgcGx1cmFsOiAnQWN0aXZpdGllcycsXG4gICAgICBwcm9wZXJ0aWVzOiB7XG4gICAgICAgIHZlcnNpb246IHtcbiAgICAgICAgICBuYW1lOiAndmVyc2lvbicsXG4gICAgICAgICAgdHlwZTogJ3N0cmluZydcbiAgICAgICAgfSxcbiAgICAgICAgZ3JvdXBJZDoge1xuICAgICAgICAgIG5hbWU6ICdncm91cElkJyxcbiAgICAgICAgICB0eXBlOiAnc3RyaW5nJ1xuICAgICAgICB9LFxuICAgICAgICB1c2VySWQ6IHtcbiAgICAgICAgICBuYW1lOiAndXNlcklkJyxcbiAgICAgICAgICB0eXBlOiAnc3RyaW5nJ1xuICAgICAgICB9LFxuICAgICAgICBhY3Rpdml0eVR5cGU6IHtcbiAgICAgICAgICBuYW1lOiAnYWN0aXZpdHlUeXBlJyxcbiAgICAgICAgICB0eXBlOiAnc3RyaW5nJ1xuICAgICAgICB9LFxuICAgICAgICB1c2Vyczoge1xuICAgICAgICAgIG5hbWU6ICd1c2VycycsXG4gICAgICAgICAgdHlwZTogJ2FueSdcbiAgICAgICAgfSxcbiAgICAgICAgbWVzc2FnZU9wdGlvbjoge1xuICAgICAgICAgIG5hbWU6ICdtZXNzYWdlT3B0aW9uJyxcbiAgICAgICAgICB0eXBlOiAnYW55J1xuICAgICAgICB9LFxuICAgICAgICB0YXJnZXRJbmZvOiB7XG4gICAgICAgICAgbmFtZTogJ3RhcmdldEluZm8nLFxuICAgICAgICAgIHR5cGU6ICdhbnknXG4gICAgICAgIH0sXG4gICAgICAgIGFjdGl2aXR5RGF0ZToge1xuICAgICAgICAgIG5hbWU6ICdhY3Rpdml0eURhdGUnLFxuICAgICAgICAgIHR5cGU6ICdEYXRlJ1xuICAgICAgICB9LFxuICAgICAgICBzdGF0dXNEZWxldGVkOiB7XG4gICAgICAgICAgbmFtZTogJ3N0YXR1c0RlbGV0ZWQnLFxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnLFxuICAgICAgICAgIGRlZmF1bHQ6ICcwJ1xuICAgICAgICB9LFxuICAgICAgICBpZDoge1xuICAgICAgICAgIG5hbWU6ICdpZCcsXG4gICAgICAgICAgdHlwZTogJ2FueSdcbiAgICAgICAgfSxcbiAgICAgICAgY3JlYXRlZEF0OiB7XG4gICAgICAgICAgbmFtZTogJ2NyZWF0ZWRBdCcsXG4gICAgICAgICAgdHlwZTogJ0RhdGUnLFxuICAgICAgICAgIGRlZmF1bHQ6IG5ldyBEYXRlKDApXG4gICAgICAgIH0sXG4gICAgICAgIGNyZWF0ZWRCeToge1xuICAgICAgICAgIG5hbWU6ICdjcmVhdGVkQnknLFxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnXG4gICAgICAgIH0sXG4gICAgICAgIG1vZGlmaWVkQXQ6IHtcbiAgICAgICAgICBuYW1lOiAnbW9kaWZpZWRBdCcsXG4gICAgICAgICAgdHlwZTogJ0RhdGUnLFxuICAgICAgICAgIGRlZmF1bHQ6IG5ldyBEYXRlKDApXG4gICAgICAgIH0sXG4gICAgICAgIG1vZGlmaWVkQnk6IHtcbiAgICAgICAgICBuYW1lOiAnbW9kaWZpZWRCeScsXG4gICAgICAgICAgdHlwZTogJ3N0cmluZydcbiAgICAgICAgfSxcbiAgICAgIH0sXG4gICAgICByZWxhdGlvbnM6IHtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cbiJdfQ==