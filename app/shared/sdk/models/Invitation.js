/* tslint:disable */
"use strict";
var Invitation = (function () {
    function Invitation(data) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Invitation`.
     */
    Invitation.getModelName = function () {
        return "Invitation";
    };
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Invitation for dynamic purposes.
    **/
    Invitation.factory = function (data) {
        return new Invitation(data);
    };
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    Invitation.getModelDefinition = function () {
        return {
            name: 'Invitation',
            plural: 'Invitations',
            properties: {
                groupId: {
                    name: 'groupId',
                    type: 'string'
                },
                userId: {
                    name: 'userId',
                    type: 'string'
                },
                type: {
                    name: 'type',
                    type: 'string'
                },
                expireDate: {
                    name: 'expireDate',
                    type: 'Date'
                },
                id: {
                    name: 'id',
                    type: 'any'
                },
                createdAt: {
                    name: 'createdAt',
                    type: 'Date',
                    default: new Date(0)
                },
                createdBy: {
                    name: 'createdBy',
                    type: 'string'
                },
                modifiedAt: {
                    name: 'modifiedAt',
                    type: 'Date',
                    default: new Date(0)
                },
                modifiedBy: {
                    name: 'modifiedBy',
                    type: 'string'
                },
            },
            relations: {}
        };
    };
    return Invitation;
}());
exports.Invitation = Invitation;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSW52aXRhdGlvbi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkludml0YXRpb24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsb0JBQW9COztBQWVwQjtJQVVFLG9CQUFZLElBQTBCO1FBQ3BDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzVCLENBQUM7SUFDRDs7O09BR0c7SUFDVyx1QkFBWSxHQUExQjtRQUNFLE1BQU0sQ0FBQyxZQUFZLENBQUM7SUFDdEIsQ0FBQztJQUNEOzs7OztPQUtHO0lBQ1csa0JBQU8sR0FBckIsVUFBc0IsSUFBeUI7UUFDN0MsTUFBTSxDQUFDLElBQUksVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzlCLENBQUM7SUFDRDs7Ozs7O09BTUc7SUFDVyw2QkFBa0IsR0FBaEM7UUFDRSxNQUFNLENBQUM7WUFDTCxJQUFJLEVBQUUsWUFBWTtZQUNsQixNQUFNLEVBQUUsYUFBYTtZQUNyQixVQUFVLEVBQUU7Z0JBQ1YsT0FBTyxFQUFFO29CQUNQLElBQUksRUFBRSxTQUFTO29CQUNmLElBQUksRUFBRSxRQUFRO2lCQUNmO2dCQUNELE1BQU0sRUFBRTtvQkFDTixJQUFJLEVBQUUsUUFBUTtvQkFDZCxJQUFJLEVBQUUsUUFBUTtpQkFDZjtnQkFDRCxJQUFJLEVBQUU7b0JBQ0osSUFBSSxFQUFFLE1BQU07b0JBQ1osSUFBSSxFQUFFLFFBQVE7aUJBQ2Y7Z0JBQ0QsVUFBVSxFQUFFO29CQUNWLElBQUksRUFBRSxZQUFZO29CQUNsQixJQUFJLEVBQUUsTUFBTTtpQkFDYjtnQkFDRCxFQUFFLEVBQUU7b0JBQ0YsSUFBSSxFQUFFLElBQUk7b0JBQ1YsSUFBSSxFQUFFLEtBQUs7aUJBQ1o7Z0JBQ0QsU0FBUyxFQUFFO29CQUNULElBQUksRUFBRSxXQUFXO29CQUNqQixJQUFJLEVBQUUsTUFBTTtvQkFDWixPQUFPLEVBQUUsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO2lCQUNyQjtnQkFDRCxTQUFTLEVBQUU7b0JBQ1QsSUFBSSxFQUFFLFdBQVc7b0JBQ2pCLElBQUksRUFBRSxRQUFRO2lCQUNmO2dCQUNELFVBQVUsRUFBRTtvQkFDVixJQUFJLEVBQUUsWUFBWTtvQkFDbEIsSUFBSSxFQUFFLE1BQU07b0JBQ1osT0FBTyxFQUFFLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQztpQkFDckI7Z0JBQ0QsVUFBVSxFQUFFO29CQUNWLElBQUksRUFBRSxZQUFZO29CQUNsQixJQUFJLEVBQUUsUUFBUTtpQkFDZjthQUNGO1lBQ0QsU0FBUyxFQUFFLEVBQ1Y7U0FDRixDQUFBO0lBQ0gsQ0FBQztJQUNILGlCQUFDO0FBQUQsQ0FBQyxBQXBGRCxJQW9GQztBQXBGWSxnQ0FBVSIsInNvdXJjZXNDb250ZW50IjpbIi8qIHRzbGludDpkaXNhYmxlICovXG5cbmRlY2xhcmUgdmFyIE9iamVjdDogYW55O1xuZXhwb3J0IGludGVyZmFjZSBJbnZpdGF0aW9uSW50ZXJmYWNlIHtcbiAgZ3JvdXBJZD86IHN0cmluZztcbiAgdXNlcklkPzogc3RyaW5nO1xuICB0eXBlPzogc3RyaW5nO1xuICBleHBpcmVEYXRlPzogRGF0ZTtcbiAgaWQ/OiBhbnk7XG4gIGNyZWF0ZWRBdD86IERhdGU7XG4gIGNyZWF0ZWRCeT86IHN0cmluZztcbiAgbW9kaWZpZWRBdD86IERhdGU7XG4gIG1vZGlmaWVkQnk/OiBzdHJpbmc7XG59XG5cbmV4cG9ydCBjbGFzcyBJbnZpdGF0aW9uIGltcGxlbWVudHMgSW52aXRhdGlvbkludGVyZmFjZSB7XG4gIGdyb3VwSWQ6IHN0cmluZztcbiAgdXNlcklkOiBzdHJpbmc7XG4gIHR5cGU6IHN0cmluZztcbiAgZXhwaXJlRGF0ZTogRGF0ZTtcbiAgaWQ6IGFueTtcbiAgY3JlYXRlZEF0OiBEYXRlO1xuICBjcmVhdGVkQnk6IHN0cmluZztcbiAgbW9kaWZpZWRBdDogRGF0ZTtcbiAgbW9kaWZpZWRCeTogc3RyaW5nO1xuICBjb25zdHJ1Y3RvcihkYXRhPzogSW52aXRhdGlvbkludGVyZmFjZSkge1xuICAgIE9iamVjdC5hc3NpZ24odGhpcywgZGF0YSk7XG4gIH1cbiAgLyoqXG4gICAqIFRoZSBuYW1lIG9mIHRoZSBtb2RlbCByZXByZXNlbnRlZCBieSB0aGlzICRyZXNvdXJjZSxcbiAgICogaS5lLiBgSW52aXRhdGlvbmAuXG4gICAqL1xuICBwdWJsaWMgc3RhdGljIGdldE1vZGVsTmFtZSgpIHtcbiAgICByZXR1cm4gXCJJbnZpdGF0aW9uXCI7XG4gIH1cbiAgLyoqXG4gICogQG1ldGhvZCBmYWN0b3J5XG4gICogQGF1dGhvciBKb25hdGhhbiBDYXNhcnJ1Ymlhc1xuICAqIEBsaWNlbnNlIE1JVFxuICAqIFRoaXMgbWV0aG9kIGNyZWF0ZXMgYW4gaW5zdGFuY2Ugb2YgSW52aXRhdGlvbiBmb3IgZHluYW1pYyBwdXJwb3Nlcy5cbiAgKiovXG4gIHB1YmxpYyBzdGF0aWMgZmFjdG9yeShkYXRhOiBJbnZpdGF0aW9uSW50ZXJmYWNlKTogSW52aXRhdGlvbntcbiAgICByZXR1cm4gbmV3IEludml0YXRpb24oZGF0YSk7XG4gIH0gIFxuICAvKipcbiAgKiBAbWV0aG9kIGdldE1vZGVsRGVmaW5pdGlvblxuICAqIEBhdXRob3IgSnVsaWVuIExlZHVuXG4gICogQGxpY2Vuc2UgTUlUXG4gICogVGhpcyBtZXRob2QgcmV0dXJucyBhbiBvYmplY3QgdGhhdCByZXByZXNlbnRzIHNvbWUgb2YgdGhlIG1vZGVsXG4gICogZGVmaW5pdGlvbnMuXG4gICoqL1xuICBwdWJsaWMgc3RhdGljIGdldE1vZGVsRGVmaW5pdGlvbigpIHtcbiAgICByZXR1cm4ge1xuICAgICAgbmFtZTogJ0ludml0YXRpb24nLFxuICAgICAgcGx1cmFsOiAnSW52aXRhdGlvbnMnLFxuICAgICAgcHJvcGVydGllczoge1xuICAgICAgICBncm91cElkOiB7XG4gICAgICAgICAgbmFtZTogJ2dyb3VwSWQnLFxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnXG4gICAgICAgIH0sXG4gICAgICAgIHVzZXJJZDoge1xuICAgICAgICAgIG5hbWU6ICd1c2VySWQnLFxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnXG4gICAgICAgIH0sXG4gICAgICAgIHR5cGU6IHtcbiAgICAgICAgICBuYW1lOiAndHlwZScsXG4gICAgICAgICAgdHlwZTogJ3N0cmluZydcbiAgICAgICAgfSxcbiAgICAgICAgZXhwaXJlRGF0ZToge1xuICAgICAgICAgIG5hbWU6ICdleHBpcmVEYXRlJyxcbiAgICAgICAgICB0eXBlOiAnRGF0ZSdcbiAgICAgICAgfSxcbiAgICAgICAgaWQ6IHtcbiAgICAgICAgICBuYW1lOiAnaWQnLFxuICAgICAgICAgIHR5cGU6ICdhbnknXG4gICAgICAgIH0sXG4gICAgICAgIGNyZWF0ZWRBdDoge1xuICAgICAgICAgIG5hbWU6ICdjcmVhdGVkQXQnLFxuICAgICAgICAgIHR5cGU6ICdEYXRlJyxcbiAgICAgICAgICBkZWZhdWx0OiBuZXcgRGF0ZSgwKVxuICAgICAgICB9LFxuICAgICAgICBjcmVhdGVkQnk6IHtcbiAgICAgICAgICBuYW1lOiAnY3JlYXRlZEJ5JyxcbiAgICAgICAgICB0eXBlOiAnc3RyaW5nJ1xuICAgICAgICB9LFxuICAgICAgICBtb2RpZmllZEF0OiB7XG4gICAgICAgICAgbmFtZTogJ21vZGlmaWVkQXQnLFxuICAgICAgICAgIHR5cGU6ICdEYXRlJyxcbiAgICAgICAgICBkZWZhdWx0OiBuZXcgRGF0ZSgwKVxuICAgICAgICB9LFxuICAgICAgICBtb2RpZmllZEJ5OiB7XG4gICAgICAgICAgbmFtZTogJ21vZGlmaWVkQnknLFxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnXG4gICAgICAgIH0sXG4gICAgICB9LFxuICAgICAgcmVsYXRpb25zOiB7XG4gICAgICB9XG4gICAgfVxuICB9XG59XG4iXX0=