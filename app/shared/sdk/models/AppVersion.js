/* tslint:disable */
"use strict";
var AppVersion = (function () {
    function AppVersion(data) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `AppVersion`.
     */
    AppVersion.getModelName = function () {
        return "AppVersion";
    };
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of AppVersion for dynamic purposes.
    **/
    AppVersion.factory = function (data) {
        return new AppVersion(data);
    };
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    AppVersion.getModelDefinition = function () {
        return {
            name: 'AppVersion',
            plural: 'AppVersions',
            properties: {
                os: {
                    name: 'os',
                    type: 'string'
                },
                version: {
                    name: 'version',
                    type: 'string'
                },
                id: {
                    name: 'id',
                    type: 'any'
                },
                createdAt: {
                    name: 'createdAt',
                    type: 'Date',
                    default: new Date(0)
                },
                createdBy: {
                    name: 'createdBy',
                    type: 'string'
                },
                modifiedAt: {
                    name: 'modifiedAt',
                    type: 'Date',
                    default: new Date(0)
                },
                modifiedBy: {
                    name: 'modifiedBy',
                    type: 'string'
                },
            },
            relations: {}
        };
    };
    return AppVersion;
}());
exports.AppVersion = AppVersion;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQXBwVmVyc2lvbi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkFwcFZlcnNpb24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsb0JBQW9COztBQWFwQjtJQVFFLG9CQUFZLElBQTBCO1FBQ3BDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzVCLENBQUM7SUFDRDs7O09BR0c7SUFDVyx1QkFBWSxHQUExQjtRQUNFLE1BQU0sQ0FBQyxZQUFZLENBQUM7SUFDdEIsQ0FBQztJQUNEOzs7OztPQUtHO0lBQ1csa0JBQU8sR0FBckIsVUFBc0IsSUFBeUI7UUFDN0MsTUFBTSxDQUFDLElBQUksVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzlCLENBQUM7SUFDRDs7Ozs7O09BTUc7SUFDVyw2QkFBa0IsR0FBaEM7UUFDRSxNQUFNLENBQUM7WUFDTCxJQUFJLEVBQUUsWUFBWTtZQUNsQixNQUFNLEVBQUUsYUFBYTtZQUNyQixVQUFVLEVBQUU7Z0JBQ1YsRUFBRSxFQUFFO29CQUNGLElBQUksRUFBRSxJQUFJO29CQUNWLElBQUksRUFBRSxRQUFRO2lCQUNmO2dCQUNELE9BQU8sRUFBRTtvQkFDUCxJQUFJLEVBQUUsU0FBUztvQkFDZixJQUFJLEVBQUUsUUFBUTtpQkFDZjtnQkFDRCxFQUFFLEVBQUU7b0JBQ0YsSUFBSSxFQUFFLElBQUk7b0JBQ1YsSUFBSSxFQUFFLEtBQUs7aUJBQ1o7Z0JBQ0QsU0FBUyxFQUFFO29CQUNULElBQUksRUFBRSxXQUFXO29CQUNqQixJQUFJLEVBQUUsTUFBTTtvQkFDWixPQUFPLEVBQUUsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO2lCQUNyQjtnQkFDRCxTQUFTLEVBQUU7b0JBQ1QsSUFBSSxFQUFFLFdBQVc7b0JBQ2pCLElBQUksRUFBRSxRQUFRO2lCQUNmO2dCQUNELFVBQVUsRUFBRTtvQkFDVixJQUFJLEVBQUUsWUFBWTtvQkFDbEIsSUFBSSxFQUFFLE1BQU07b0JBQ1osT0FBTyxFQUFFLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQztpQkFDckI7Z0JBQ0QsVUFBVSxFQUFFO29CQUNWLElBQUksRUFBRSxZQUFZO29CQUNsQixJQUFJLEVBQUUsUUFBUTtpQkFDZjthQUNGO1lBQ0QsU0FBUyxFQUFFLEVBQ1Y7U0FDRixDQUFBO0lBQ0gsQ0FBQztJQUNILGlCQUFDO0FBQUQsQ0FBQyxBQTFFRCxJQTBFQztBQTFFWSxnQ0FBVSIsInNvdXJjZXNDb250ZW50IjpbIi8qIHRzbGludDpkaXNhYmxlICovXG5cbmRlY2xhcmUgdmFyIE9iamVjdDogYW55O1xuZXhwb3J0IGludGVyZmFjZSBBcHBWZXJzaW9uSW50ZXJmYWNlIHtcbiAgb3M/OiBzdHJpbmc7XG4gIHZlcnNpb24/OiBzdHJpbmc7XG4gIGlkPzogYW55O1xuICBjcmVhdGVkQXQ/OiBEYXRlO1xuICBjcmVhdGVkQnk/OiBzdHJpbmc7XG4gIG1vZGlmaWVkQXQ/OiBEYXRlO1xuICBtb2RpZmllZEJ5Pzogc3RyaW5nO1xufVxuXG5leHBvcnQgY2xhc3MgQXBwVmVyc2lvbiBpbXBsZW1lbnRzIEFwcFZlcnNpb25JbnRlcmZhY2Uge1xuICBvczogc3RyaW5nO1xuICB2ZXJzaW9uOiBzdHJpbmc7XG4gIGlkOiBhbnk7XG4gIGNyZWF0ZWRBdDogRGF0ZTtcbiAgY3JlYXRlZEJ5OiBzdHJpbmc7XG4gIG1vZGlmaWVkQXQ6IERhdGU7XG4gIG1vZGlmaWVkQnk6IHN0cmluZztcbiAgY29uc3RydWN0b3IoZGF0YT86IEFwcFZlcnNpb25JbnRlcmZhY2UpIHtcbiAgICBPYmplY3QuYXNzaWduKHRoaXMsIGRhdGEpO1xuICB9XG4gIC8qKlxuICAgKiBUaGUgbmFtZSBvZiB0aGUgbW9kZWwgcmVwcmVzZW50ZWQgYnkgdGhpcyAkcmVzb3VyY2UsXG4gICAqIGkuZS4gYEFwcFZlcnNpb25gLlxuICAgKi9cbiAgcHVibGljIHN0YXRpYyBnZXRNb2RlbE5hbWUoKSB7XG4gICAgcmV0dXJuIFwiQXBwVmVyc2lvblwiO1xuICB9XG4gIC8qKlxuICAqIEBtZXRob2QgZmFjdG9yeVxuICAqIEBhdXRob3IgSm9uYXRoYW4gQ2FzYXJydWJpYXNcbiAgKiBAbGljZW5zZSBNSVRcbiAgKiBUaGlzIG1ldGhvZCBjcmVhdGVzIGFuIGluc3RhbmNlIG9mIEFwcFZlcnNpb24gZm9yIGR5bmFtaWMgcHVycG9zZXMuXG4gICoqL1xuICBwdWJsaWMgc3RhdGljIGZhY3RvcnkoZGF0YTogQXBwVmVyc2lvbkludGVyZmFjZSk6IEFwcFZlcnNpb257XG4gICAgcmV0dXJuIG5ldyBBcHBWZXJzaW9uKGRhdGEpO1xuICB9ICBcbiAgLyoqXG4gICogQG1ldGhvZCBnZXRNb2RlbERlZmluaXRpb25cbiAgKiBAYXV0aG9yIEp1bGllbiBMZWR1blxuICAqIEBsaWNlbnNlIE1JVFxuICAqIFRoaXMgbWV0aG9kIHJldHVybnMgYW4gb2JqZWN0IHRoYXQgcmVwcmVzZW50cyBzb21lIG9mIHRoZSBtb2RlbFxuICAqIGRlZmluaXRpb25zLlxuICAqKi9cbiAgcHVibGljIHN0YXRpYyBnZXRNb2RlbERlZmluaXRpb24oKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIG5hbWU6ICdBcHBWZXJzaW9uJyxcbiAgICAgIHBsdXJhbDogJ0FwcFZlcnNpb25zJyxcbiAgICAgIHByb3BlcnRpZXM6IHtcbiAgICAgICAgb3M6IHtcbiAgICAgICAgICBuYW1lOiAnb3MnLFxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnXG4gICAgICAgIH0sXG4gICAgICAgIHZlcnNpb246IHtcbiAgICAgICAgICBuYW1lOiAndmVyc2lvbicsXG4gICAgICAgICAgdHlwZTogJ3N0cmluZydcbiAgICAgICAgfSxcbiAgICAgICAgaWQ6IHtcbiAgICAgICAgICBuYW1lOiAnaWQnLFxuICAgICAgICAgIHR5cGU6ICdhbnknXG4gICAgICAgIH0sXG4gICAgICAgIGNyZWF0ZWRBdDoge1xuICAgICAgICAgIG5hbWU6ICdjcmVhdGVkQXQnLFxuICAgICAgICAgIHR5cGU6ICdEYXRlJyxcbiAgICAgICAgICBkZWZhdWx0OiBuZXcgRGF0ZSgwKVxuICAgICAgICB9LFxuICAgICAgICBjcmVhdGVkQnk6IHtcbiAgICAgICAgICBuYW1lOiAnY3JlYXRlZEJ5JyxcbiAgICAgICAgICB0eXBlOiAnc3RyaW5nJ1xuICAgICAgICB9LFxuICAgICAgICBtb2RpZmllZEF0OiB7XG4gICAgICAgICAgbmFtZTogJ21vZGlmaWVkQXQnLFxuICAgICAgICAgIHR5cGU6ICdEYXRlJyxcbiAgICAgICAgICBkZWZhdWx0OiBuZXcgRGF0ZSgwKVxuICAgICAgICB9LFxuICAgICAgICBtb2RpZmllZEJ5OiB7XG4gICAgICAgICAgbmFtZTogJ21vZGlmaWVkQnknLFxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnXG4gICAgICAgIH0sXG4gICAgICB9LFxuICAgICAgcmVsYXRpb25zOiB7XG4gICAgICB9XG4gICAgfVxuICB9XG59XG4iXX0=