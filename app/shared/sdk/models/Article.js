/* tslint:disable */
"use strict";
var Article = (function () {
    function Article(data) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Article`.
     */
    Article.getModelName = function () {
        return "Article";
    };
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Article for dynamic purposes.
    **/
    Article.factory = function (data) {
        return new Article(data);
    };
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    Article.getModelDefinition = function () {
        return {
            name: 'Article',
            plural: 'Articles',
            properties: {
                groupId: {
                    name: 'groupId',
                    type: 'any'
                },
                pageId: {
                    name: 'pageId',
                    type: 'string'
                },
                userId: {
                    name: 'userId',
                    type: 'string'
                },
                contents: {
                    name: 'contents',
                    type: 'Array&lt;any&gt;',
                    default: []
                },
                marks: {
                    name: 'marks',
                    type: 'Array&lt;any&gt;',
                    default: []
                },
                comments: {
                    name: 'comments',
                    type: 'Array&lt;any&gt;',
                    default: []
                },
                statusDeleted: {
                    name: 'statusDeleted',
                    type: 'string',
                    default: '0'
                },
                statusAlready: {
                    name: 'statusAlready',
                    type: 'string',
                    default: '0'
                },
                statusUploaded: {
                    name: 'statusUploaded',
                    type: 'string',
                    default: '0'
                },
                statusEdited: {
                    name: 'statusEdited',
                    type: 'string',
                    default: '0'
                },
                articleDate: {
                    name: 'articleDate',
                    type: 'number'
                },
                id: {
                    name: 'id',
                    type: 'any'
                },
                createdAt: {
                    name: 'createdAt',
                    type: 'Date',
                    default: new Date(0)
                },
                createdBy: {
                    name: 'createdBy',
                    type: 'string'
                },
                modifiedAt: {
                    name: 'modifiedAt',
                    type: 'Date',
                    default: new Date(0)
                },
                modifiedBy: {
                    name: 'modifiedBy',
                    type: 'string'
                },
            },
            relations: {
                group: {
                    name: 'group',
                    type: 'any',
                    model: ''
                },
                markList: {
                    name: 'markList',
                    type: 'any[]',
                    model: ''
                },
                commentList: {
                    name: 'commentList',
                    type: 'any[]',
                    model: ''
                },
                contentList: {
                    name: 'contentList',
                    type: 'any[]',
                    model: ''
                },
            }
        };
    };
    return Article;
}());
exports.Article = Article;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQXJ0aWNsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIkFydGljbGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsb0JBQW9COztBQTBCcEI7SUFxQkUsaUJBQVksSUFBdUI7UUFDakMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDNUIsQ0FBQztJQUNEOzs7T0FHRztJQUNXLG9CQUFZLEdBQTFCO1FBQ0UsTUFBTSxDQUFDLFNBQVMsQ0FBQztJQUNuQixDQUFDO0lBQ0Q7Ozs7O09BS0c7SUFDVyxlQUFPLEdBQXJCLFVBQXNCLElBQXNCO1FBQzFDLE1BQU0sQ0FBQyxJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMzQixDQUFDO0lBQ0Q7Ozs7OztPQU1HO0lBQ1csMEJBQWtCLEdBQWhDO1FBQ0UsTUFBTSxDQUFDO1lBQ0wsSUFBSSxFQUFFLFNBQVM7WUFDZixNQUFNLEVBQUUsVUFBVTtZQUNsQixVQUFVLEVBQUU7Z0JBQ1YsT0FBTyxFQUFFO29CQUNQLElBQUksRUFBRSxTQUFTO29CQUNmLElBQUksRUFBRSxLQUFLO2lCQUNaO2dCQUNELE1BQU0sRUFBRTtvQkFDTixJQUFJLEVBQUUsUUFBUTtvQkFDZCxJQUFJLEVBQUUsUUFBUTtpQkFDZjtnQkFDRCxNQUFNLEVBQUU7b0JBQ04sSUFBSSxFQUFFLFFBQVE7b0JBQ2QsSUFBSSxFQUFFLFFBQVE7aUJBQ2Y7Z0JBQ0QsUUFBUSxFQUFFO29CQUNSLElBQUksRUFBRSxVQUFVO29CQUNoQixJQUFJLEVBQUUsa0JBQWtCO29CQUN4QixPQUFPLEVBQU8sRUFBRTtpQkFDakI7Z0JBQ0QsS0FBSyxFQUFFO29CQUNMLElBQUksRUFBRSxPQUFPO29CQUNiLElBQUksRUFBRSxrQkFBa0I7b0JBQ3hCLE9BQU8sRUFBTyxFQUFFO2lCQUNqQjtnQkFDRCxRQUFRLEVBQUU7b0JBQ1IsSUFBSSxFQUFFLFVBQVU7b0JBQ2hCLElBQUksRUFBRSxrQkFBa0I7b0JBQ3hCLE9BQU8sRUFBTyxFQUFFO2lCQUNqQjtnQkFDRCxhQUFhLEVBQUU7b0JBQ2IsSUFBSSxFQUFFLGVBQWU7b0JBQ3JCLElBQUksRUFBRSxRQUFRO29CQUNkLE9BQU8sRUFBRSxHQUFHO2lCQUNiO2dCQUNELGFBQWEsRUFBRTtvQkFDYixJQUFJLEVBQUUsZUFBZTtvQkFDckIsSUFBSSxFQUFFLFFBQVE7b0JBQ2QsT0FBTyxFQUFFLEdBQUc7aUJBQ2I7Z0JBQ0QsY0FBYyxFQUFFO29CQUNkLElBQUksRUFBRSxnQkFBZ0I7b0JBQ3RCLElBQUksRUFBRSxRQUFRO29CQUNkLE9BQU8sRUFBRSxHQUFHO2lCQUNiO2dCQUNELFlBQVksRUFBRTtvQkFDWixJQUFJLEVBQUUsY0FBYztvQkFDcEIsSUFBSSxFQUFFLFFBQVE7b0JBQ2QsT0FBTyxFQUFFLEdBQUc7aUJBQ2I7Z0JBQ0QsV0FBVyxFQUFFO29CQUNYLElBQUksRUFBRSxhQUFhO29CQUNuQixJQUFJLEVBQUUsUUFBUTtpQkFDZjtnQkFDRCxFQUFFLEVBQUU7b0JBQ0YsSUFBSSxFQUFFLElBQUk7b0JBQ1YsSUFBSSxFQUFFLEtBQUs7aUJBQ1o7Z0JBQ0QsU0FBUyxFQUFFO29CQUNULElBQUksRUFBRSxXQUFXO29CQUNqQixJQUFJLEVBQUUsTUFBTTtvQkFDWixPQUFPLEVBQUUsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO2lCQUNyQjtnQkFDRCxTQUFTLEVBQUU7b0JBQ1QsSUFBSSxFQUFFLFdBQVc7b0JBQ2pCLElBQUksRUFBRSxRQUFRO2lCQUNmO2dCQUNELFVBQVUsRUFBRTtvQkFDVixJQUFJLEVBQUUsWUFBWTtvQkFDbEIsSUFBSSxFQUFFLE1BQU07b0JBQ1osT0FBTyxFQUFFLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQztpQkFDckI7Z0JBQ0QsVUFBVSxFQUFFO29CQUNWLElBQUksRUFBRSxZQUFZO29CQUNsQixJQUFJLEVBQUUsUUFBUTtpQkFDZjthQUNGO1lBQ0QsU0FBUyxFQUFFO2dCQUNULEtBQUssRUFBRTtvQkFDTCxJQUFJLEVBQUUsT0FBTztvQkFDYixJQUFJLEVBQUUsS0FBSztvQkFDWCxLQUFLLEVBQUUsRUFBRTtpQkFDVjtnQkFDRCxRQUFRLEVBQUU7b0JBQ1IsSUFBSSxFQUFFLFVBQVU7b0JBQ2hCLElBQUksRUFBRSxPQUFPO29CQUNiLEtBQUssRUFBRSxFQUFFO2lCQUNWO2dCQUNELFdBQVcsRUFBRTtvQkFDWCxJQUFJLEVBQUUsYUFBYTtvQkFDbkIsSUFBSSxFQUFFLE9BQU87b0JBQ2IsS0FBSyxFQUFFLEVBQUU7aUJBQ1Y7Z0JBQ0QsV0FBVyxFQUFFO29CQUNYLElBQUksRUFBRSxhQUFhO29CQUNuQixJQUFJLEVBQUUsT0FBTztvQkFDYixLQUFLLEVBQUUsRUFBRTtpQkFDVjthQUNGO1NBQ0YsQ0FBQTtJQUNILENBQUM7SUFDSCxjQUFDO0FBQUQsQ0FBQyxBQXRKRCxJQXNKQztBQXRKWSwwQkFBTyIsInNvdXJjZXNDb250ZW50IjpbIi8qIHRzbGludDpkaXNhYmxlICovXG5cbmRlY2xhcmUgdmFyIE9iamVjdDogYW55O1xuZXhwb3J0IGludGVyZmFjZSBBcnRpY2xlSW50ZXJmYWNlIHtcbiAgZ3JvdXBJZD86IGFueTtcbiAgcGFnZUlkPzogc3RyaW5nO1xuICB1c2VySWQ/OiBzdHJpbmc7XG4gIGNvbnRlbnRzPzogQXJyYXk8YW55PjtcbiAgbWFya3M/OiBBcnJheTxhbnk+O1xuICBjb21tZW50cz86IEFycmF5PGFueT47XG4gIHN0YXR1c0RlbGV0ZWQ/OiBzdHJpbmc7XG4gIHN0YXR1c0FscmVhZHk/OiBzdHJpbmc7XG4gIHN0YXR1c1VwbG9hZGVkPzogc3RyaW5nO1xuICBzdGF0dXNFZGl0ZWQ/OiBzdHJpbmc7XG4gIGFydGljbGVEYXRlPzogbnVtYmVyO1xuICBpZD86IGFueTtcbiAgY3JlYXRlZEF0PzogRGF0ZTtcbiAgY3JlYXRlZEJ5Pzogc3RyaW5nO1xuICBtb2RpZmllZEF0PzogRGF0ZTtcbiAgbW9kaWZpZWRCeT86IHN0cmluZztcbiAgZ3JvdXA/OiBhbnk7XG4gIG1hcmtMaXN0PzogYW55W107XG4gIGNvbW1lbnRMaXN0PzogYW55W107XG4gIGNvbnRlbnRMaXN0PzogYW55W107XG59XG5cbmV4cG9ydCBjbGFzcyBBcnRpY2xlIGltcGxlbWVudHMgQXJ0aWNsZUludGVyZmFjZSB7XG4gIGdyb3VwSWQ6IGFueTtcbiAgcGFnZUlkOiBzdHJpbmc7XG4gIHVzZXJJZDogc3RyaW5nO1xuICBjb250ZW50czogQXJyYXk8YW55PjtcbiAgbWFya3M6IEFycmF5PGFueT47XG4gIGNvbW1lbnRzOiBBcnJheTxhbnk+O1xuICBzdGF0dXNEZWxldGVkOiBzdHJpbmc7XG4gIHN0YXR1c0FscmVhZHk6IHN0cmluZztcbiAgc3RhdHVzVXBsb2FkZWQ6IHN0cmluZztcbiAgc3RhdHVzRWRpdGVkOiBzdHJpbmc7XG4gIGFydGljbGVEYXRlOiBudW1iZXI7XG4gIGlkOiBhbnk7XG4gIGNyZWF0ZWRBdDogRGF0ZTtcbiAgY3JlYXRlZEJ5OiBzdHJpbmc7XG4gIG1vZGlmaWVkQXQ6IERhdGU7XG4gIG1vZGlmaWVkQnk6IHN0cmluZztcbiAgZ3JvdXA6IGFueTtcbiAgbWFya0xpc3Q6IGFueVtdO1xuICBjb21tZW50TGlzdDogYW55W107XG4gIGNvbnRlbnRMaXN0OiBhbnlbXTtcbiAgY29uc3RydWN0b3IoZGF0YT86IEFydGljbGVJbnRlcmZhY2UpIHtcbiAgICBPYmplY3QuYXNzaWduKHRoaXMsIGRhdGEpO1xuICB9XG4gIC8qKlxuICAgKiBUaGUgbmFtZSBvZiB0aGUgbW9kZWwgcmVwcmVzZW50ZWQgYnkgdGhpcyAkcmVzb3VyY2UsXG4gICAqIGkuZS4gYEFydGljbGVgLlxuICAgKi9cbiAgcHVibGljIHN0YXRpYyBnZXRNb2RlbE5hbWUoKSB7XG4gICAgcmV0dXJuIFwiQXJ0aWNsZVwiO1xuICB9XG4gIC8qKlxuICAqIEBtZXRob2QgZmFjdG9yeVxuICAqIEBhdXRob3IgSm9uYXRoYW4gQ2FzYXJydWJpYXNcbiAgKiBAbGljZW5zZSBNSVRcbiAgKiBUaGlzIG1ldGhvZCBjcmVhdGVzIGFuIGluc3RhbmNlIG9mIEFydGljbGUgZm9yIGR5bmFtaWMgcHVycG9zZXMuXG4gICoqL1xuICBwdWJsaWMgc3RhdGljIGZhY3RvcnkoZGF0YTogQXJ0aWNsZUludGVyZmFjZSk6IEFydGljbGV7XG4gICAgcmV0dXJuIG5ldyBBcnRpY2xlKGRhdGEpO1xuICB9ICBcbiAgLyoqXG4gICogQG1ldGhvZCBnZXRNb2RlbERlZmluaXRpb25cbiAgKiBAYXV0aG9yIEp1bGllbiBMZWR1blxuICAqIEBsaWNlbnNlIE1JVFxuICAqIFRoaXMgbWV0aG9kIHJldHVybnMgYW4gb2JqZWN0IHRoYXQgcmVwcmVzZW50cyBzb21lIG9mIHRoZSBtb2RlbFxuICAqIGRlZmluaXRpb25zLlxuICAqKi9cbiAgcHVibGljIHN0YXRpYyBnZXRNb2RlbERlZmluaXRpb24oKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIG5hbWU6ICdBcnRpY2xlJyxcbiAgICAgIHBsdXJhbDogJ0FydGljbGVzJyxcbiAgICAgIHByb3BlcnRpZXM6IHtcbiAgICAgICAgZ3JvdXBJZDoge1xuICAgICAgICAgIG5hbWU6ICdncm91cElkJyxcbiAgICAgICAgICB0eXBlOiAnYW55J1xuICAgICAgICB9LFxuICAgICAgICBwYWdlSWQ6IHtcbiAgICAgICAgICBuYW1lOiAncGFnZUlkJyxcbiAgICAgICAgICB0eXBlOiAnc3RyaW5nJ1xuICAgICAgICB9LFxuICAgICAgICB1c2VySWQ6IHtcbiAgICAgICAgICBuYW1lOiAndXNlcklkJyxcbiAgICAgICAgICB0eXBlOiAnc3RyaW5nJ1xuICAgICAgICB9LFxuICAgICAgICBjb250ZW50czoge1xuICAgICAgICAgIG5hbWU6ICdjb250ZW50cycsXG4gICAgICAgICAgdHlwZTogJ0FycmF5Jmx0O2FueSZndDsnLFxuICAgICAgICAgIGRlZmF1bHQ6IDxhbnk+W11cbiAgICAgICAgfSxcbiAgICAgICAgbWFya3M6IHtcbiAgICAgICAgICBuYW1lOiAnbWFya3MnLFxuICAgICAgICAgIHR5cGU6ICdBcnJheSZsdDthbnkmZ3Q7JyxcbiAgICAgICAgICBkZWZhdWx0OiA8YW55PltdXG4gICAgICAgIH0sXG4gICAgICAgIGNvbW1lbnRzOiB7XG4gICAgICAgICAgbmFtZTogJ2NvbW1lbnRzJyxcbiAgICAgICAgICB0eXBlOiAnQXJyYXkmbHQ7YW55Jmd0OycsXG4gICAgICAgICAgZGVmYXVsdDogPGFueT5bXVxuICAgICAgICB9LFxuICAgICAgICBzdGF0dXNEZWxldGVkOiB7XG4gICAgICAgICAgbmFtZTogJ3N0YXR1c0RlbGV0ZWQnLFxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnLFxuICAgICAgICAgIGRlZmF1bHQ6ICcwJ1xuICAgICAgICB9LFxuICAgICAgICBzdGF0dXNBbHJlYWR5OiB7XG4gICAgICAgICAgbmFtZTogJ3N0YXR1c0FscmVhZHknLFxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnLFxuICAgICAgICAgIGRlZmF1bHQ6ICcwJ1xuICAgICAgICB9LFxuICAgICAgICBzdGF0dXNVcGxvYWRlZDoge1xuICAgICAgICAgIG5hbWU6ICdzdGF0dXNVcGxvYWRlZCcsXG4gICAgICAgICAgdHlwZTogJ3N0cmluZycsXG4gICAgICAgICAgZGVmYXVsdDogJzAnXG4gICAgICAgIH0sXG4gICAgICAgIHN0YXR1c0VkaXRlZDoge1xuICAgICAgICAgIG5hbWU6ICdzdGF0dXNFZGl0ZWQnLFxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnLFxuICAgICAgICAgIGRlZmF1bHQ6ICcwJ1xuICAgICAgICB9LFxuICAgICAgICBhcnRpY2xlRGF0ZToge1xuICAgICAgICAgIG5hbWU6ICdhcnRpY2xlRGF0ZScsXG4gICAgICAgICAgdHlwZTogJ251bWJlcidcbiAgICAgICAgfSxcbiAgICAgICAgaWQ6IHtcbiAgICAgICAgICBuYW1lOiAnaWQnLFxuICAgICAgICAgIHR5cGU6ICdhbnknXG4gICAgICAgIH0sXG4gICAgICAgIGNyZWF0ZWRBdDoge1xuICAgICAgICAgIG5hbWU6ICdjcmVhdGVkQXQnLFxuICAgICAgICAgIHR5cGU6ICdEYXRlJyxcbiAgICAgICAgICBkZWZhdWx0OiBuZXcgRGF0ZSgwKVxuICAgICAgICB9LFxuICAgICAgICBjcmVhdGVkQnk6IHtcbiAgICAgICAgICBuYW1lOiAnY3JlYXRlZEJ5JyxcbiAgICAgICAgICB0eXBlOiAnc3RyaW5nJ1xuICAgICAgICB9LFxuICAgICAgICBtb2RpZmllZEF0OiB7XG4gICAgICAgICAgbmFtZTogJ21vZGlmaWVkQXQnLFxuICAgICAgICAgIHR5cGU6ICdEYXRlJyxcbiAgICAgICAgICBkZWZhdWx0OiBuZXcgRGF0ZSgwKVxuICAgICAgICB9LFxuICAgICAgICBtb2RpZmllZEJ5OiB7XG4gICAgICAgICAgbmFtZTogJ21vZGlmaWVkQnknLFxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnXG4gICAgICAgIH0sXG4gICAgICB9LFxuICAgICAgcmVsYXRpb25zOiB7XG4gICAgICAgIGdyb3VwOiB7XG4gICAgICAgICAgbmFtZTogJ2dyb3VwJyxcbiAgICAgICAgICB0eXBlOiAnYW55JyxcbiAgICAgICAgICBtb2RlbDogJydcbiAgICAgICAgfSxcbiAgICAgICAgbWFya0xpc3Q6IHtcbiAgICAgICAgICBuYW1lOiAnbWFya0xpc3QnLFxuICAgICAgICAgIHR5cGU6ICdhbnlbXScsXG4gICAgICAgICAgbW9kZWw6ICcnXG4gICAgICAgIH0sXG4gICAgICAgIGNvbW1lbnRMaXN0OiB7XG4gICAgICAgICAgbmFtZTogJ2NvbW1lbnRMaXN0JyxcbiAgICAgICAgICB0eXBlOiAnYW55W10nLFxuICAgICAgICAgIG1vZGVsOiAnJ1xuICAgICAgICB9LFxuICAgICAgICBjb250ZW50TGlzdDoge1xuICAgICAgICAgIG5hbWU6ICdjb250ZW50TGlzdCcsXG4gICAgICAgICAgdHlwZTogJ2FueVtdJyxcbiAgICAgICAgICBtb2RlbDogJydcbiAgICAgICAgfSxcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cbiJdfQ==