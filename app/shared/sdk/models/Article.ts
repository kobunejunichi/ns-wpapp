/* tslint:disable */

declare var Object: any;
export interface ArticleInterface {
  groupId?: any;
  pageId?: string;
  userId?: string;
  contents?: Array<any>;
  marks?: Array<any>;
  comments?: Array<any>;
  statusDeleted?: string;
  statusAlready?: string;
  statusUploaded?: string;
  statusEdited?: string;
  articleDate?: number;
  id?: any;
  createdAt?: Date;
  createdBy?: string;
  modifiedAt?: Date;
  modifiedBy?: string;
  group?: any;
  markList?: any[];
  commentList?: any[];
  contentList?: any[];
}

export class Article implements ArticleInterface {
  groupId: any;
  pageId: string;
  userId: string;
  contents: Array<any>;
  marks: Array<any>;
  comments: Array<any>;
  statusDeleted: string;
  statusAlready: string;
  statusUploaded: string;
  statusEdited: string;
  articleDate: number;
  id: any;
  createdAt: Date;
  createdBy: string;
  modifiedAt: Date;
  modifiedBy: string;
  group: any;
  markList: any[];
  commentList: any[];
  contentList: any[];
  constructor(data?: ArticleInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Article`.
   */
  public static getModelName() {
    return "Article";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Article for dynamic purposes.
  **/
  public static factory(data: ArticleInterface): Article{
    return new Article(data);
  }  
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Article',
      plural: 'Articles',
      properties: {
        groupId: {
          name: 'groupId',
          type: 'any'
        },
        pageId: {
          name: 'pageId',
          type: 'string'
        },
        userId: {
          name: 'userId',
          type: 'string'
        },
        contents: {
          name: 'contents',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        marks: {
          name: 'marks',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        comments: {
          name: 'comments',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        statusDeleted: {
          name: 'statusDeleted',
          type: 'string',
          default: '0'
        },
        statusAlready: {
          name: 'statusAlready',
          type: 'string',
          default: '0'
        },
        statusUploaded: {
          name: 'statusUploaded',
          type: 'string',
          default: '0'
        },
        statusEdited: {
          name: 'statusEdited',
          type: 'string',
          default: '0'
        },
        articleDate: {
          name: 'articleDate',
          type: 'number'
        },
        id: {
          name: 'id',
          type: 'any'
        },
        createdAt: {
          name: 'createdAt',
          type: 'Date',
          default: new Date(0)
        },
        createdBy: {
          name: 'createdBy',
          type: 'string'
        },
        modifiedAt: {
          name: 'modifiedAt',
          type: 'Date',
          default: new Date(0)
        },
        modifiedBy: {
          name: 'modifiedBy',
          type: 'string'
        },
      },
      relations: {
        group: {
          name: 'group',
          type: 'any',
          model: ''
        },
        markList: {
          name: 'markList',
          type: 'any[]',
          model: ''
        },
        commentList: {
          name: 'commentList',
          type: 'any[]',
          model: ''
        },
        contentList: {
          name: 'contentList',
          type: 'any[]',
          model: ''
        },
      }
    }
  }
}
