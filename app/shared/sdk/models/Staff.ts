/* tslint:disable */

declare var Object: any;
export interface StaffInterface {
  name?: string;
  email?: string;
  password?: string;
  passwordStatus?: string;
  tmpPasswordExipireDate?: Date;
  auth?: any;
  revokeCount?: number;
  statusValid?: string;
  statusDeleted?: string;
  dateLastLogined?: string;
  createdAt?: Date;
  createdBy?: string;
  updatedAt?: Date;
  updatedBy?: string;
  realm?: string;
  username?: string;
  challenges?: any;
  emailVerified?: boolean;
  verificationToken?: string;
  status?: string;
  created?: Date;
  lastUpdated?: Date;
  id?: any;
  modifiedAt?: Date;
  modifiedBy?: string;
  accessTokens?: any[];
}

export class Staff implements StaffInterface {
  name: string;
  email: string;
  password: string;
  passwordStatus: string;
  tmpPasswordExipireDate: Date;
  auth: any;
  revokeCount: number;
  statusValid: string;
  statusDeleted: string;
  dateLastLogined: string;
  createdAt: Date;
  createdBy: string;
  updatedAt: Date;
  updatedBy: string;
  realm: string;
  username: string;
  challenges: any;
  emailVerified: boolean;
  verificationToken: string;
  status: string;
  created: Date;
  lastUpdated: Date;
  id: any;
  modifiedAt: Date;
  modifiedBy: string;
  accessTokens: any[];
  constructor(data?: StaffInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Staff`.
   */
  public static getModelName() {
    return "Staff";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Staff for dynamic purposes.
  **/
  public static factory(data: StaffInterface): Staff{
    return new Staff(data);
  }  
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Staff',
      plural: 'Staffs',
      properties: {
        name: {
          name: 'name',
          type: 'string'
        },
        email: {
          name: 'email',
          type: 'string'
        },
        password: {
          name: 'password',
          type: 'string'
        },
        passwordStatus: {
          name: 'passwordStatus',
          type: 'string',
          default: '0'
        },
        tmpPasswordExipireDate: {
          name: 'tmpPasswordExipireDate',
          type: 'Date'
        },
        auth: {
          name: 'auth',
          type: 'any'
        },
        revokeCount: {
          name: 'revokeCount',
          type: 'number',
          default: 0
        },
        statusValid: {
          name: 'statusValid',
          type: 'string',
          default: '0'
        },
        statusDeleted: {
          name: 'statusDeleted',
          type: 'string',
          default: '0'
        },
        dateLastLogined: {
          name: 'dateLastLogined',
          type: 'string'
        },
        createdAt: {
          name: 'createdAt',
          type: 'Date',
          default: new Date(0)
        },
        createdBy: {
          name: 'createdBy',
          type: 'string'
        },
        updatedAt: {
          name: 'updatedAt',
          type: 'Date'
        },
        updatedBy: {
          name: 'updatedBy',
          type: 'string'
        },
        realm: {
          name: 'realm',
          type: 'string'
        },
        username: {
          name: 'username',
          type: 'string'
        },
        credentials: {
          name: 'credentials',
          type: 'any'
        },
        challenges: {
          name: 'challenges',
          type: 'any'
        },
        emailVerified: {
          name: 'emailVerified',
          type: 'boolean'
        },
        verificationToken: {
          name: 'verificationToken',
          type: 'string'
        },
        status: {
          name: 'status',
          type: 'string'
        },
        created: {
          name: 'created',
          type: 'Date'
        },
        lastUpdated: {
          name: 'lastUpdated',
          type: 'Date'
        },
        id: {
          name: 'id',
          type: 'any'
        },
        modifiedAt: {
          name: 'modifiedAt',
          type: 'Date',
          default: new Date(0)
        },
        modifiedBy: {
          name: 'modifiedBy',
          type: 'string'
        },
      },
      relations: {
        accessTokens: {
          name: 'accessTokens',
          type: 'any[]',
          model: ''
        },
      }
    }
  }
}
