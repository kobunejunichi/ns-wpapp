"use strict";
/* tslint:disable */
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
/**
* @author Jonathan Casarrubias <twitter:@johncasarrubias> <github:@mean-expert-official>
* @module JSONSearchParams
* @license MIT
* @description
* JSON Parser and Wrapper for the Angular2 URLSearchParams
* This module correctly encodes a json object into a query string and then creates
* an instance of the URLSearchParams component for later use in HTTP Calls
**/
var JSONSearchParams = (function () {
    function JSONSearchParams() {
    }
    JSONSearchParams.prototype.setJSON = function (obj) {
        this._usp = new http_1.URLSearchParams(this._JSON2URL(obj, false));
    };
    JSONSearchParams.prototype.getURLSearchParams = function () {
        return this._usp;
    };
    JSONSearchParams.prototype._JSON2URL = function (obj, parent) {
        var parts = [];
        for (var key in obj)
            parts.push(this._parseParam(key, obj[key], parent));
        return parts.join('&');
    };
    JSONSearchParams.prototype._parseParam = function (key, value, parent) {
        if (typeof value !== 'object' && typeof value !== 'array') {
            return parent ? parent + '[' + key + ']=' + value
                : key + '=' + value;
        }
        else if (typeof value === 'object' || typeof value === 'array') {
            return parent ? this._JSON2URL(value, parent + '[' + key + ']')
                : this._JSON2URL(value, key);
        }
        else {
            throw new Error('Unexpected Type');
        }
    };
    return JSONSearchParams;
}());
JSONSearchParams = __decorate([
    core_1.Injectable()
], JSONSearchParams);
exports.JSONSearchParams = JSONSearchParams;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLnBhcmFtcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNlYXJjaC5wYXJhbXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLG9CQUFvQjtBQUNwQixzQ0FBMkM7QUFDM0Msc0NBQWdEO0FBQ2hEOzs7Ozs7OztHQVFHO0FBRUgsSUFBYSxnQkFBZ0I7SUFBN0I7SUE4QkEsQ0FBQztJQTFCVSxrQ0FBTyxHQUFkLFVBQWUsR0FBUTtRQUNuQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksc0JBQWUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDO0lBQ2hFLENBQUM7SUFFTSw2Q0FBa0IsR0FBekI7UUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztJQUNyQixDQUFDO0lBRU8sb0NBQVMsR0FBakIsVUFBa0IsR0FBUSxFQUFFLE1BQVc7UUFDbkMsSUFBSSxLQUFLLEdBQVEsRUFBRSxDQUFDO1FBQ3BCLEdBQUcsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLEdBQUcsQ0FBQztZQUNwQixLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQyxDQUFDO1FBQ3BELE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQzNCLENBQUM7SUFFTyxzQ0FBVyxHQUFuQixVQUFvQixHQUFXLEVBQUUsS0FBVSxFQUFFLE1BQWM7UUFDdkQsRUFBRSxDQUFDLENBQUMsT0FBTyxLQUFLLEtBQUssUUFBUSxJQUFJLE9BQU8sS0FBSyxLQUFLLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDeEQsTUFBTSxDQUFDLE1BQU0sR0FBRyxNQUFNLEdBQUcsR0FBRyxHQUFHLEdBQUcsR0FBRyxJQUFJLEdBQUcsS0FBSztrQkFDakMsR0FBRyxHQUFHLEdBQUcsR0FBRyxLQUFLLENBQUM7UUFDdEMsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxPQUFPLEtBQUssS0FBSyxRQUFRLElBQUssT0FBTyxLQUFLLEtBQUssT0FBTyxDQUFDLENBQUMsQ0FBQztZQUNoRSxNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLE1BQU0sR0FBRyxHQUFHLEdBQUcsR0FBRyxHQUFHLEdBQUcsQ0FBQztrQkFDL0MsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDL0MsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxJQUFJLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQ3ZDLENBQUM7SUFDTCxDQUFDO0lBQ0wsdUJBQUM7QUFBRCxDQUFDLEFBOUJELElBOEJDO0FBOUJZLGdCQUFnQjtJQUQ1QixpQkFBVSxFQUFFO0dBQ0EsZ0JBQWdCLENBOEI1QjtBQTlCWSw0Q0FBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyIvKiB0c2xpbnQ6ZGlzYWJsZSAqL1xuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgVVJMU2VhcmNoUGFyYW1zIH0gZnJvbSAnQGFuZ3VsYXIvaHR0cCc7XG4vKipcbiogQGF1dGhvciBKb25hdGhhbiBDYXNhcnJ1YmlhcyA8dHdpdHRlcjpAam9obmNhc2FycnViaWFzPiA8Z2l0aHViOkBtZWFuLWV4cGVydC1vZmZpY2lhbD5cbiogQG1vZHVsZSBKU09OU2VhcmNoUGFyYW1zXG4qIEBsaWNlbnNlIE1JVFxuKiBAZGVzY3JpcHRpb25cbiogSlNPTiBQYXJzZXIgYW5kIFdyYXBwZXIgZm9yIHRoZSBBbmd1bGFyMiBVUkxTZWFyY2hQYXJhbXNcbiogVGhpcyBtb2R1bGUgY29ycmVjdGx5IGVuY29kZXMgYSBqc29uIG9iamVjdCBpbnRvIGEgcXVlcnkgc3RyaW5nIGFuZCB0aGVuIGNyZWF0ZXNcbiogYW4gaW5zdGFuY2Ugb2YgdGhlIFVSTFNlYXJjaFBhcmFtcyBjb21wb25lbnQgZm9yIGxhdGVyIHVzZSBpbiBIVFRQIENhbGxzXG4qKi9cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBKU09OU2VhcmNoUGFyYW1zIHtcblxuICAgIHByaXZhdGUgX3VzcDogVVJMU2VhcmNoUGFyYW1zO1xuXG4gICAgcHVibGljIHNldEpTT04ob2JqOiBhbnkpIHtcbiAgICAgICAgdGhpcy5fdXNwID0gbmV3IFVSTFNlYXJjaFBhcmFtcyh0aGlzLl9KU09OMlVSTChvYmosIGZhbHNlKSk7XG4gICAgfVxuXG4gICAgcHVibGljIGdldFVSTFNlYXJjaFBhcmFtcygpOiBVUkxTZWFyY2hQYXJhbXMge1xuICAgICAgICByZXR1cm4gdGhpcy5fdXNwO1xuICAgIH1cblxuICAgIHByaXZhdGUgX0pTT04yVVJMKG9iajogYW55LCBwYXJlbnQ6IGFueSkge1xuICAgICAgICB2YXIgcGFydHM6IGFueSA9IFtdO1xuICAgICAgICBmb3IgKHZhciBrZXkgaW4gb2JqKVxuICAgICAgICBwYXJ0cy5wdXNoKHRoaXMuX3BhcnNlUGFyYW0oa2V5LCBvYmpba2V5XSwgcGFyZW50KSk7XG4gICAgICAgIHJldHVybiBwYXJ0cy5qb2luKCcmJyk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBfcGFyc2VQYXJhbShrZXk6IHN0cmluZywgdmFsdWU6IGFueSwgcGFyZW50OiBzdHJpbmcpIHtcbiAgICAgICAgaWYgKHR5cGVvZiB2YWx1ZSAhPT0gJ29iamVjdCcgJiYgdHlwZW9mIHZhbHVlICE9PSAnYXJyYXknKSB7XG4gICAgICAgICAgICByZXR1cm4gcGFyZW50ID8gcGFyZW50ICsgJ1snICsga2V5ICsgJ109JyArIHZhbHVlXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDoga2V5ICsgJz0nICsgdmFsdWU7XG4gICAgICAgIH0gZWxzZSBpZiAodHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyB8fCDCoHR5cGVvZiB2YWx1ZSA9PT0gJ2FycmF5Jykge1xuICAgICAgICAgICAgcmV0dXJuIHBhcmVudCA/IHRoaXMuX0pTT04yVVJMKHZhbHVlLCBwYXJlbnQgKyAnWycgKyBrZXkgKyAnXScpXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDogdGhpcy5fSlNPTjJVUkwodmFsdWUsIGtleSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1VuZXhwZWN0ZWQgVHlwZScpO1xuICAgICAgICB9XG4gICAgfVxufVxuIl19