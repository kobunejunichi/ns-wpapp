"use strict";
/* tslint:disable */
var core_1 = require("@angular/core");
var Observable_1 = require("rxjs/Observable");
//import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
require("rxjs/add/observable/throw");
/**
 * Default error handler
 */
var ErrorHandler = (function () {
    function ErrorHandler() {
    }
    // ErrorObservable when rxjs version < rc.5
    // ErrorObservable<string> when rxjs version = rc.5
    // I'm leaving any for now to avoid breaking apps using both versions
    ErrorHandler.prototype.handleError = function (error) {
        return Observable_1.Observable.throw(error.json().error || 'Server error');
    };
    return ErrorHandler;
}());
ErrorHandler = __decorate([
    core_1.Injectable()
], ErrorHandler);
exports.ErrorHandler = ErrorHandler;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXJyb3Iuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImVycm9yLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLG9CQUFvQjtBQUNwQixzQ0FBMkM7QUFFM0MsOENBQTZDO0FBQzdDLG9FQUFvRTtBQUNwRSxxQ0FBbUM7QUFDbkM7O0dBRUc7QUFFSCxJQUFhLFlBQVk7SUFBekI7SUFPQSxDQUFDO0lBTkMsMkNBQTJDO0lBQzNDLG1EQUFtRDtJQUNuRCxxRUFBcUU7SUFDOUQsa0NBQVcsR0FBbEIsVUFBbUIsS0FBZTtRQUNoQyxNQUFNLENBQUMsdUJBQVUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDLEtBQUssSUFBSSxjQUFjLENBQUMsQ0FBQztJQUNoRSxDQUFDO0lBQ0gsbUJBQUM7QUFBRCxDQUFDLEFBUEQsSUFPQztBQVBZLFlBQVk7SUFEeEIsaUJBQVUsRUFBRTtHQUNBLFlBQVksQ0FPeEI7QUFQWSxvQ0FBWSIsInNvdXJjZXNDb250ZW50IjpbIi8qIHRzbGludDpkaXNhYmxlICovXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBSZXNwb25zZSB9IGZyb20gJ0Bhbmd1bGFyL2h0dHAnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMvT2JzZXJ2YWJsZSc7XG4vL2ltcG9ydCB7IEVycm9yT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMvb2JzZXJ2YWJsZS9FcnJvck9ic2VydmFibGUnO1xuaW1wb3J0ICdyeGpzL2FkZC9vYnNlcnZhYmxlL3Rocm93Jztcbi8qKlxuICogRGVmYXVsdCBlcnJvciBoYW5kbGVyXG4gKi9cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBFcnJvckhhbmRsZXIge1xuICAvLyBFcnJvck9ic2VydmFibGUgd2hlbiByeGpzIHZlcnNpb24gPCByYy41XG4gIC8vIEVycm9yT2JzZXJ2YWJsZTxzdHJpbmc+IHdoZW4gcnhqcyB2ZXJzaW9uID0gcmMuNVxuICAvLyBJJ20gbGVhdmluZyBhbnkgZm9yIG5vdyB0byBhdm9pZCBicmVha2luZyBhcHBzIHVzaW5nIGJvdGggdmVyc2lvbnNcbiAgcHVibGljIGhhbmRsZUVycm9yKGVycm9yOiBSZXNwb25zZSk6IGFueSB7XG4gICAgcmV0dXJuIE9ic2VydmFibGUudGhyb3coZXJyb3IuanNvbigpLmVycm9yIHx8ICdTZXJ2ZXIgZXJyb3InKTtcbiAgfVxufVxuIl19