"use strict";
/* tslint:disable */
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var search_params_1 = require("./search.params");
var error_service_1 = require("./error.service");
var auth_service_1 = require("./auth.service");
var lb_config_1 = require("../../lb.config");
var SDKModels_1 = require("../custom/SDKModels");
var Subject_1 = require("rxjs/Subject");
require("rxjs/add/operator/catch");
require("rxjs/add/operator/map");
/**
* @module BaseLoopBackApi
* @author Jonathan Casarrubias <@johncasarrubias> <github:jonathan-casarrubias>
* @author Nikolay Matiushenkov <https://github.com/mnvx>
* @license MIT
* @description
* Abstract class that will be implemented in every custom service automatically built
* by the sdk builder.
* It provides the core functionallity for every API call, either by HTTP Calls or by
* WebSockets.
**/
var BaseLoopBackApi = (function () {
    function BaseLoopBackApi(http, models, auth, searchParams, errorHandler) {
        this.http = http;
        this.models = models;
        this.auth = auth;
        this.searchParams = searchParams;
        this.errorHandler = errorHandler;
        this.model = this.models.get(this.getModelName());
    }
    /**
     * @method request
     * @param {string}  method      Request method (GET, POST, PUT)
     * @param {string}  url         Request url (my-host/my-url/:id)
     * @param {any}     routeParams Values of url parameters
     * @param {any}     urlParams   Parameters for building url (filter and other)
     * @param {any}     postBody    Request postBody
     * @return {Observable<any>}
     * @description
     * This is a core method, every HTTP Call will be done from here, every API Service will
     * extend this class and use this method to get RESTful communication.
     **/
    BaseLoopBackApi.prototype.request = function (method, url, routeParams, urlParams, postBody) {
        var _this = this;
        if (routeParams === void 0) { routeParams = {}; }
        if (urlParams === void 0) { urlParams = {}; }
        if (postBody === void 0) { postBody = {}; }
        // Headers to be sent
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        // Authenticate request
        this.authenticate(url, headers);
        // Transpile route variables to the actual request Values
        Object.keys(routeParams).forEach(function (key) {
            url = url.replace(new RegExp(":" + key + "(\/|$)", "g"), routeParams[key] + "$1");
        });
        // Body fix for built in remote methods using "data", "options" or "credentials
        // that are the actual body, Custom remote method properties are different and need
        // to be wrapped into a body object
        var body;
        var postBodyKeys = typeof postBody === 'object' ? Object.keys(postBody) : [];
        if (postBodyKeys.length === 1) {
            body = postBody[postBodyKeys[0]];
        }
        else {
            body = postBody;
        }
        // Separate filter object from url params and add to search query
        if (urlParams.filter) {
            headers.append('filter', JSON.stringify(urlParams.filter));
            delete urlParams.filter;
        }
        // Separate where object from url params and add to search query
        if (urlParams.where) {
            headers.append('where', JSON.stringify(urlParams.where));
            delete urlParams.where;
        }
        this.searchParams.setJSON(urlParams);
        var request = new http_1.Request({
            headers: headers,
            method: method,
            url: url,
            search: Object.keys(urlParams).length > 0
                ? this.searchParams.getURLSearchParams() : null,
            body: body ? JSON.stringify(body) : undefined
        });
        return this.http.request(request)
            .map(function (res) { return (res.text() != "" ? res.json() : {}); })
            .catch(function (e) { return _this.errorHandler.handleError(e); });
    };
    /**
     * @method authenticate
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @param {string} url Server URL
     * @param {Headers} headers HTTP Headers
     * @return {void}
     * @description
     * This method will try to authenticate using either an access_token or basic http auth
     */
    BaseLoopBackApi.prototype.authenticate = function (url, headers) {
        if (this.auth.getAccessTokenId()) {
            headers.append('Authorization', lb_config_1.LoopBackConfig.getAuthPrefix() + this.auth.getAccessTokenId());
        }
    };
    /**
     * @method create
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @param {T} data Generic data type
     * @return {Observable<T>}
     * @description
     * Generic create method
     */
    BaseLoopBackApi.prototype.create = function (data) {
        var _this = this;
        return this.request('POST', [
            lb_config_1.LoopBackConfig.getPath(),
            lb_config_1.LoopBackConfig.getApiVersion(),
            this.model.getModelDefinition().plural
        ].join('/'), undefined, undefined, { data: data }).map(function (data) { return _this.model.factory(data); });
    };
    /**
     * @method create
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @param {T[]} data Generic data type array
     * @return {Observable<T[]>}
     * @description
     * Generic create many method
     */
    BaseLoopBackApi.prototype.createMany = function (data) {
        var _this = this;
        return this.request('POST', [
            lb_config_1.LoopBackConfig.getPath(),
            lb_config_1.LoopBackConfig.getApiVersion(),
            this.model.getModelDefinition().plural
        ].join('/'), undefined, undefined, { data: data })
            .map(function (datum) { return datum.map(function (data) { return _this.model.factory(data); }); });
    };
    /**
     * @method findById
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @param {any} data Generic data type
     * @return {Observable<T>}
     * @description
     * Generic findById method
     */
    BaseLoopBackApi.prototype.findById = function (id, filter) {
        var _this = this;
        if (filter === void 0) { filter = {}; }
        var _urlParams = {};
        if (filter)
            _urlParams.filter = filter;
        return this.request('GET', [
            lb_config_1.LoopBackConfig.getPath(),
            lb_config_1.LoopBackConfig.getApiVersion(),
            this.model.getModelDefinition().plural,
            ':id'
        ].join('/'), { id: id }, _urlParams, undefined).map(function (data) { return _this.model.factory(data); });
    };
    /**
     * @method find
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T[+>}
     * @description
     * Generic find method
     */
    BaseLoopBackApi.prototype.find = function (filter) {
        var _this = this;
        if (filter === void 0) { filter = {}; }
        return this.request('GET', [
            lb_config_1.LoopBackConfig.getPath(),
            lb_config_1.LoopBackConfig.getApiVersion(),
            this.model.getModelDefinition().plural
        ].join('/'), undefined, { filter: filter }, undefined)
            .map(function (datum) { return datum.map(function (data) { return _this.model.factory(data); }); });
    };
    /**
     * @method exists
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T[]>}
     * @description
     * Generic exists method
     */
    BaseLoopBackApi.prototype.exists = function (id) {
        return this.request('GET', [
            lb_config_1.LoopBackConfig.getPath(),
            lb_config_1.LoopBackConfig.getApiVersion(),
            this.model.getModelDefinition().plural,
            ':id/exists'
        ].join('/'), { id: id }, undefined, undefined);
    };
    /**
     * @method findOne
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T>}
     * @description
     * Generic findOne method
     */
    BaseLoopBackApi.prototype.findOne = function (filter) {
        var _this = this;
        if (filter === void 0) { filter = {}; }
        return this.request('GET', [
            lb_config_1.LoopBackConfig.getPath(),
            lb_config_1.LoopBackConfig.getApiVersion(),
            this.model.getModelDefinition().plural,
            'findOne'
        ].join('/'), undefined, { filter: filter }, undefined).map(function (data) { return _this.model.factory(data); });
    };
    /**
     * @method updateAll
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T[]>}
     * @description
     * Generic updateAll method
     */
    BaseLoopBackApi.prototype.updateAll = function (where, data) {
        if (where === void 0) { where = {}; }
        var _urlParams = {};
        if (where)
            _urlParams.where = where;
        return this.request('POST', [
            lb_config_1.LoopBackConfig.getPath(),
            lb_config_1.LoopBackConfig.getApiVersion(),
            this.model.getModelDefinition().plural,
            'update'
        ].join('/'), undefined, _urlParams, { data: data });
    };
    /**
     * @method deleteById
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T>}
     * @description
     * Generic deleteById method
     */
    BaseLoopBackApi.prototype.deleteById = function (id) {
        var _this = this;
        return this.request('DELETE', [
            lb_config_1.LoopBackConfig.getPath(),
            lb_config_1.LoopBackConfig.getApiVersion(),
            this.model.getModelDefinition().plural,
            ':id'
        ].join('/'), { id: id }, undefined, undefined).map(function (data) { return _this.model.factory(data); });
    };
    /**
     * @method count
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<{ count: number }>}
     * @description
     * Generic count method
     */
    BaseLoopBackApi.prototype.count = function (where) {
        if (where === void 0) { where = {}; }
        var _urlParams = {};
        if (where)
            _urlParams.where = where;
        return this.request('GET', [
            lb_config_1.LoopBackConfig.getPath(),
            lb_config_1.LoopBackConfig.getApiVersion(),
            this.model.getModelDefinition().plural,
            'count'
        ].join('/'), undefined, _urlParams, undefined);
    };
    /**
     * @method updateAttributes
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T>}
     * @description
     * Generic updateAttributes method
     */
    BaseLoopBackApi.prototype.updateAttributes = function (id, data) {
        var _this = this;
        return this.request('PUT', [
            lb_config_1.LoopBackConfig.getPath(),
            lb_config_1.LoopBackConfig.getApiVersion(),
            this.model.getModelDefinition().plural,
            ':id'
        ].join('/'), { id: id }, undefined, { data: data }).map(function (data) { return _this.model.factory(data); });
    };
    /**
     * @method upsert
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T>}
     * @description
     * Generic upsert method
     */
    BaseLoopBackApi.prototype.upsert = function (data) {
        var _this = this;
        if (data === void 0) { data = {}; }
        return this.request('PUT', [
            lb_config_1.LoopBackConfig.getPath(),
            lb_config_1.LoopBackConfig.getApiVersion(),
            this.model.getModelDefinition().plural,
        ].join('/'), undefined, undefined, { data: data }).map(function (data) { return _this.model.factory(data); });
    };
    /**
     * @method upsertPatch
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T>}
     * @description
     * Generic upsert method using patch http method
     */
    BaseLoopBackApi.prototype.upsertPatch = function (data) {
        var _this = this;
        if (data === void 0) { data = {}; }
        return this.request('PATCH', [
            lb_config_1.LoopBackConfig.getPath(),
            lb_config_1.LoopBackConfig.getApiVersion(),
            this.model.getModelDefinition().plural,
        ].join('/'), undefined, undefined, { data: data }).map(function (data) { return _this.model.factory(data); });
    };
    /**
     * @method upsertWithWhere
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T>}
     * @description
     * Generic upsertWithWhere method
     */
    BaseLoopBackApi.prototype.upsertWithWhere = function (where, data) {
        var _this = this;
        if (where === void 0) { where = {}; }
        if (data === void 0) { data = {}; }
        var _urlParams = {};
        if (where)
            _urlParams.where = where;
        return this.request('POST', [
            lb_config_1.LoopBackConfig.getPath(),
            lb_config_1.LoopBackConfig.getApiVersion(),
            this.model.getModelDefinition().plural,
            'upsertWithWhere'
        ].join('/'), undefined, _urlParams, { data: data }).map(function (data) { return _this.model.factory(data); });
    };
    /**
     * @method replaceOrCreate
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T>}
     * @description
     * Generic replaceOrCreate method
     */
    BaseLoopBackApi.prototype.replaceOrCreate = function (data) {
        var _this = this;
        if (data === void 0) { data = {}; }
        return this.request('POST', [
            lb_config_1.LoopBackConfig.getPath(),
            lb_config_1.LoopBackConfig.getApiVersion(),
            this.model.getModelDefinition().plural,
            'replaceOrCreate'
        ].join('/'), undefined, undefined, { data: data }).map(function (data) { return _this.model.factory(data); });
    };
    /**
     * @method replaceById
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<T>}
     * @description
     * Generic replaceById method
     */
    BaseLoopBackApi.prototype.replaceById = function (id, data) {
        var _this = this;
        if (data === void 0) { data = {}; }
        return this.request('POST', [
            lb_config_1.LoopBackConfig.getPath(),
            lb_config_1.LoopBackConfig.getApiVersion(),
            this.model.getModelDefinition().plural,
            ':id', 'replace'
        ].join('/'), { id: id }, undefined, { data: data }).map(function (data) { return _this.model.factory(data); });
    };
    /**
     * @method createChangeStream
     * @author Jonathan Casarrubias <t: johncasarrubias, gh: mean-expert-official>
     * @license MIT
     * @return {Observable<any>}
     * @description
     * Generic createChangeStream method
     */
    BaseLoopBackApi.prototype.createChangeStream = function () {
        var subject = new Subject_1.Subject();
        if (typeof EventSource !== 'undefined') {
            var emit = function (msg) { return subject.next(JSON.parse(msg.data)); };
            var source = new EventSource([
                lb_config_1.LoopBackConfig.getPath(),
                lb_config_1.LoopBackConfig.getApiVersion(),
                this.model.getModelDefinition().plural,
                'change-stream'
            ].join('/'));
            source.addEventListener('data', emit);
            source.onerror = emit;
        }
        else {
            console.warn('SDK Builder: EventSource is not supported');
        }
        return subject.asObservable();
    };
    return BaseLoopBackApi;
}());
BaseLoopBackApi = __decorate([
    core_1.Injectable(),
    __param(0, core_1.Inject(http_1.Http)),
    __param(1, core_1.Inject(SDKModels_1.SDKModels)),
    __param(2, core_1.Inject(auth_service_1.LoopBackAuth)),
    __param(3, core_1.Inject(search_params_1.JSONSearchParams)),
    __param(4, core_1.Optional()), __param(4, core_1.Inject(error_service_1.ErrorHandler)),
    __metadata("design:paramtypes", [http_1.Http,
        SDKModels_1.SDKModels,
        auth_service_1.LoopBackAuth,
        search_params_1.JSONSearchParams,
        error_service_1.ErrorHandler])
], BaseLoopBackApi);
exports.BaseLoopBackApi = BaseLoopBackApi;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYmFzZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxvQkFBb0I7QUFDcEIsc0NBQTZEO0FBQzdELHNDQUF1RDtBQUV2RCxpREFBbUQ7QUFDbkQsaURBQStDO0FBQy9DLCtDQUE4QztBQUM5Qyw2Q0FBaUQ7QUFFakQsaURBQWdEO0FBRWhELHdDQUF1QztBQUV2QyxtQ0FBaUM7QUFDakMsaUNBQStCO0FBRy9COzs7Ozs7Ozs7O0dBVUc7QUFFSCxJQUFzQixlQUFlO0lBS25DLHlCQUMwQixJQUFVLEVBQ0wsTUFBaUIsRUFDZCxJQUFrQixFQUNkLFlBQThCLEVBQ3RCLFlBQTBCO1FBSjlDLFNBQUksR0FBSixJQUFJLENBQU07UUFDTCxXQUFNLEdBQU4sTUFBTSxDQUFXO1FBQ2QsU0FBSSxHQUFKLElBQUksQ0FBYztRQUNkLGlCQUFZLEdBQVosWUFBWSxDQUFrQjtRQUN0QixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUV0RSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFDRDs7Ozs7Ozs7Ozs7UUFXSTtJQUNHLGlDQUFPLEdBQWQsVUFDRSxNQUFvQixFQUNwQixHQUFvQixFQUNwQixXQUFzQixFQUN0QixTQUFzQixFQUN0QixRQUFzQjtRQUx4QixpQkFnREM7UUE3Q0MsNEJBQUEsRUFBQSxnQkFBc0I7UUFDdEIsMEJBQUEsRUFBQSxjQUFzQjtRQUN0Qix5QkFBQSxFQUFBLGFBQXNCO1FBRXRCLHFCQUFxQjtRQUNyQixJQUFJLE9BQU8sR0FBWSxJQUFJLGNBQU8sRUFBRSxDQUFDO1FBQ3JDLE9BQU8sQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFLGtCQUFrQixDQUFDLENBQUM7UUFDbkQsdUJBQXVCO1FBQ3ZCLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ2hDLHlEQUF5RDtRQUN6RCxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFDLEdBQVc7WUFDM0MsR0FBRyxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxNQUFNLENBQUMsR0FBRyxHQUFHLEdBQUcsR0FBRyxRQUFRLEVBQUUsR0FBRyxDQUFDLEVBQUUsV0FBVyxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFBO1FBQ25GLENBQUMsQ0FBQyxDQUFDO1FBQ0gsK0VBQStFO1FBQy9FLG1GQUFtRjtRQUNuRixtQ0FBbUM7UUFDbkMsSUFBSSxJQUFTLENBQUM7UUFDZCxJQUFJLFlBQVksR0FBRyxPQUFPLFFBQVEsS0FBSyxRQUFRLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLENBQUE7UUFDNUUsRUFBRSxDQUFDLENBQUMsWUFBWSxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzlCLElBQUksR0FBRyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbkMsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sSUFBSSxHQUFHLFFBQVEsQ0FBQztRQUNsQixDQUFDO1FBQ0QsaUVBQWlFO1FBQ2pFLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ3JCLE9BQU8sQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDM0QsT0FBTyxTQUFTLENBQUMsTUFBTSxDQUFDO1FBQzFCLENBQUM7UUFDRCxnRUFBZ0U7UUFDaEUsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDcEIsT0FBTyxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUN6RCxPQUFPLFNBQVMsQ0FBQyxLQUFLLENBQUM7UUFDekIsQ0FBQztRQUNELElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3JDLElBQUksT0FBTyxHQUFZLElBQUksY0FBTyxDQUFDO1lBQ2pDLE9BQU8sRUFBRyxPQUFPO1lBQ2pCLE1BQU0sRUFBSSxNQUFNO1lBQ2hCLEdBQUcsRUFBTyxHQUFHO1lBQ2IsTUFBTSxFQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUM7a0JBQ2pDLElBQUksQ0FBQyxZQUFZLENBQUMsa0JBQWtCLEVBQUUsR0FBRyxJQUFJO1lBQ3ZELElBQUksRUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxTQUFTO1NBQ2xELENBQUMsQ0FBQztRQUNILE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUM7YUFDOUIsR0FBRyxDQUFDLFVBQUMsR0FBUSxJQUFLLE9BQUEsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxHQUFHLEdBQUcsQ0FBQyxJQUFJLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBcEMsQ0FBb0MsQ0FBQzthQUN2RCxLQUFLLENBQUMsVUFBQyxDQUFDLElBQUssT0FBQSxLQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBaEMsQ0FBZ0MsQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFDRDs7Ozs7Ozs7O09BU0c7SUFDSSxzQ0FBWSxHQUFuQixVQUF1QixHQUFXLEVBQUUsT0FBZ0I7UUFDbEQsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNqQyxPQUFPLENBQUMsTUFBTSxDQUNaLGVBQWUsRUFDZiwwQkFBYyxDQUFDLGFBQWEsRUFBRSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FDOUQsQ0FBQztRQUNKLENBQUM7SUFDSCxDQUFDO0lBQ0Q7Ozs7Ozs7O09BUUc7SUFDSSxnQ0FBTSxHQUFiLFVBQWlCLElBQU87UUFBeEIsaUJBTUM7UUFMQyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUU7WUFDMUIsMEJBQWMsQ0FBQyxPQUFPLEVBQUU7WUFDeEIsMEJBQWMsQ0FBQyxhQUFhLEVBQUU7WUFDOUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLE1BQU07U0FDdkMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBQyxJQUFPLElBQUssT0FBQSxLQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBeEIsQ0FBd0IsQ0FBQyxDQUFDO0lBQzFGLENBQUM7SUFDRDs7Ozs7Ozs7T0FRRztJQUNJLG9DQUFVLEdBQWpCLFVBQXFCLElBQVM7UUFBOUIsaUJBT0M7UUFOQyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUU7WUFDMUIsMEJBQWMsQ0FBQyxPQUFPLEVBQUU7WUFDeEIsMEJBQWMsQ0FBQyxhQUFhLEVBQUU7WUFDOUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLE1BQU07U0FDdkMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxFQUFFLElBQUksTUFBQSxFQUFFLENBQUM7YUFDM0MsR0FBRyxDQUFDLFVBQUMsS0FBVSxJQUFLLE9BQUEsS0FBSyxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQU8sSUFBSyxPQUFBLEtBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUF4QixDQUF3QixDQUFDLEVBQWhELENBQWdELENBQUMsQ0FBQztJQUN6RSxDQUFDO0lBQ0Q7Ozs7Ozs7O09BUUc7SUFDSSxrQ0FBUSxHQUFmLFVBQW1CLEVBQU8sRUFBRSxNQUEyQjtRQUF2RCxpQkFTQztRQVQyQix1QkFBQSxFQUFBLFdBQTJCO1FBQ3JELElBQUksVUFBVSxHQUFRLEVBQUUsQ0FBQztRQUN6QixFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUM7WUFBQyxVQUFVLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUN2QyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUU7WUFDekIsMEJBQWMsQ0FBQyxPQUFPLEVBQUU7WUFDeEIsMEJBQWMsQ0FBQyxhQUFhLEVBQUU7WUFDOUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLE1BQU07WUFDdEMsS0FBSztTQUNOLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEVBQUUsRUFBRSxJQUFBLEVBQUUsRUFBRSxVQUFVLEVBQUUsU0FBUyxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQUMsSUFBTyxJQUFLLE9BQUEsS0FBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQXhCLENBQXdCLENBQUMsQ0FBQztJQUN6RixDQUFDO0lBQ0Q7Ozs7Ozs7T0FPRztJQUNJLDhCQUFJLEdBQVgsVUFBZSxNQUEyQjtRQUExQyxpQkFPQztRQVBjLHVCQUFBLEVBQUEsV0FBMkI7UUFDeEMsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFO1lBQ3pCLDBCQUFjLENBQUMsT0FBTyxFQUFFO1lBQ3hCLDBCQUFjLENBQUMsYUFBYSxFQUFFO1lBQzlCLElBQUksQ0FBQyxLQUFLLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxNQUFNO1NBQ3ZDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLFNBQVMsRUFBRSxFQUFFLE1BQU0sUUFBQSxFQUFFLEVBQUUsU0FBUyxDQUFDO2FBQzdDLEdBQUcsQ0FBQyxVQUFDLEtBQVUsSUFBSyxPQUFBLEtBQUssQ0FBQyxHQUFHLENBQUMsVUFBQyxJQUFPLElBQUssT0FBQSxLQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBeEIsQ0FBd0IsQ0FBQyxFQUFoRCxDQUFnRCxDQUFDLENBQUM7SUFDekUsQ0FBQztJQUNEOzs7Ozs7O09BT0c7SUFDSSxnQ0FBTSxHQUFiLFVBQWlCLEVBQU87UUFDdEIsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFO1lBQ3pCLDBCQUFjLENBQUMsT0FBTyxFQUFFO1lBQ3hCLDBCQUFjLENBQUMsYUFBYSxFQUFFO1lBQzlCLElBQUksQ0FBQyxLQUFLLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxNQUFNO1lBQ3RDLFlBQVk7U0FDYixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxFQUFFLEVBQUUsSUFBQSxFQUFFLEVBQUUsU0FBUyxFQUFFLFNBQVMsQ0FBQyxDQUFDO0lBQzdDLENBQUM7SUFDRDs7Ozs7OztPQU9HO0lBQ0ksaUNBQU8sR0FBZCxVQUFrQixNQUEyQjtRQUE3QyxpQkFPQztRQVBpQix1QkFBQSxFQUFBLFdBQTJCO1FBQzNDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRTtZQUN6QiwwQkFBYyxDQUFDLE9BQU8sRUFBRTtZQUN4QiwwQkFBYyxDQUFDLGFBQWEsRUFBRTtZQUM5QixJQUFJLENBQUMsS0FBSyxDQUFDLGtCQUFrQixFQUFFLENBQUMsTUFBTTtZQUN0QyxTQUFTO1NBQ1YsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsU0FBUyxFQUFFLEVBQUUsTUFBTSxRQUFBLEVBQUUsRUFBRSxTQUFTLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBQyxJQUFPLElBQUssT0FBQSxLQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBeEIsQ0FBd0IsQ0FBQyxDQUFDO0lBQzVGLENBQUM7SUFDRDs7Ozs7OztPQU9HO0lBQ0ksbUNBQVMsR0FBaEIsVUFBb0IsS0FBZSxFQUFFLElBQU87UUFBeEIsc0JBQUEsRUFBQSxVQUFlO1FBQ2pDLElBQUksVUFBVSxHQUFRLEVBQUUsQ0FBQztRQUN6QixFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUM7WUFBQyxVQUFVLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNwQyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUU7WUFDMUIsMEJBQWMsQ0FBQyxPQUFPLEVBQUU7WUFDeEIsMEJBQWMsQ0FBQyxhQUFhLEVBQUU7WUFDOUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLE1BQU07WUFDdEMsUUFBUTtTQUNULENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsRUFBRSxJQUFJLE1BQUEsRUFBRSxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUNEOzs7Ozs7O09BT0c7SUFDSSxvQ0FBVSxHQUFqQixVQUFxQixFQUFPO1FBQTVCLGlCQU9DO1FBTkMsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFO1lBQzVCLDBCQUFjLENBQUMsT0FBTyxFQUFFO1lBQ3hCLDBCQUFjLENBQUMsYUFBYSxFQUFFO1lBQzlCLElBQUksQ0FBQyxLQUFLLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxNQUFNO1lBQ3RDLEtBQUs7U0FDTixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxFQUFFLEVBQUUsSUFBQSxFQUFFLEVBQUUsU0FBUyxFQUFFLFNBQVMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQU8sSUFBSyxPQUFBLEtBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUF4QixDQUF3QixDQUFDLENBQUM7SUFDeEYsQ0FBQztJQUNEOzs7Ozs7O09BT0c7SUFDSSwrQkFBSyxHQUFaLFVBQWEsS0FBZTtRQUFmLHNCQUFBLEVBQUEsVUFBZTtRQUMxQixJQUFJLFVBQVUsR0FBUSxFQUFFLENBQUM7UUFDekIsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDO1lBQUMsVUFBVSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDcEMsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFO1lBQ3pCLDBCQUFjLENBQUMsT0FBTyxFQUFFO1lBQ3hCLDBCQUFjLENBQUMsYUFBYSxFQUFFO1lBQzlCLElBQUksQ0FBQyxLQUFLLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxNQUFNO1lBQ3RDLE9BQU87U0FDUixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLFNBQVMsQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFDRDs7Ozs7OztPQU9HO0lBQ0ksMENBQWdCLEdBQXZCLFVBQTJCLEVBQU8sRUFBRSxJQUFPO1FBQTNDLGlCQU9DO1FBTkMsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFO1lBQ3pCLDBCQUFjLENBQUMsT0FBTyxFQUFFO1lBQ3hCLDBCQUFjLENBQUMsYUFBYSxFQUFFO1lBQzlCLElBQUksQ0FBQyxLQUFLLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxNQUFNO1lBQ3RDLEtBQUs7U0FDTixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxFQUFFLEVBQUUsSUFBQSxFQUFFLEVBQUUsU0FBUyxFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQU8sSUFBSyxPQUFBLEtBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUF4QixDQUF3QixDQUFDLENBQUM7SUFDdkYsQ0FBQztJQUNEOzs7Ozs7O09BT0c7SUFDSSxnQ0FBTSxHQUFiLFVBQWlCLElBQWM7UUFBL0IsaUJBTUM7UUFOZ0IscUJBQUEsRUFBQSxTQUFjO1FBQzdCLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRTtZQUN6QiwwQkFBYyxDQUFDLE9BQU8sRUFBRTtZQUN4QiwwQkFBYyxDQUFDLGFBQWEsRUFBRTtZQUM5QixJQUFJLENBQUMsS0FBSyxDQUFDLGtCQUFrQixFQUFFLENBQUMsTUFBTTtTQUN2QyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQU8sSUFBSyxPQUFBLEtBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUF4QixDQUF3QixDQUFDLENBQUM7SUFDMUYsQ0FBQztJQUNEOzs7Ozs7O09BT0c7SUFDSSxxQ0FBVyxHQUFsQixVQUFzQixJQUFjO1FBQXBDLGlCQU1DO1FBTnFCLHFCQUFBLEVBQUEsU0FBYztRQUNsQyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUU7WUFDM0IsMEJBQWMsQ0FBQyxPQUFPLEVBQUU7WUFDeEIsMEJBQWMsQ0FBQyxhQUFhLEVBQUU7WUFDOUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLE1BQU07U0FDdkMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBQyxJQUFPLElBQUssT0FBQSxLQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBeEIsQ0FBd0IsQ0FBQyxDQUFDO0lBQzFGLENBQUM7SUFDRDs7Ozs7OztPQU9HO0lBQ0kseUNBQWUsR0FBdEIsVUFBMEIsS0FBZSxFQUFFLElBQWM7UUFBekQsaUJBU0M7UUFUeUIsc0JBQUEsRUFBQSxVQUFlO1FBQUUscUJBQUEsRUFBQSxTQUFjO1FBQ3ZELElBQUksVUFBVSxHQUFRLEVBQUUsQ0FBQztRQUN6QixFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUM7WUFBQyxVQUFVLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNwQyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUU7WUFDMUIsMEJBQWMsQ0FBQyxPQUFPLEVBQUU7WUFDeEIsMEJBQWMsQ0FBQyxhQUFhLEVBQUU7WUFDOUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLE1BQU07WUFDdEMsaUJBQWlCO1NBQ2xCLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsRUFBRSxJQUFJLE1BQUEsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQUMsSUFBTyxJQUFLLE9BQUEsS0FBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQXhCLENBQXdCLENBQUMsQ0FBQztJQUMzRixDQUFDO0lBQ0Q7Ozs7Ozs7T0FPRztJQUNJLHlDQUFlLEdBQXRCLFVBQTBCLElBQWM7UUFBeEMsaUJBT0M7UUFQeUIscUJBQUEsRUFBQSxTQUFjO1FBQ3RDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRTtZQUMxQiwwQkFBYyxDQUFDLE9BQU8sRUFBRTtZQUN4QiwwQkFBYyxDQUFDLGFBQWEsRUFBRTtZQUM5QixJQUFJLENBQUMsS0FBSyxDQUFDLGtCQUFrQixFQUFFLENBQUMsTUFBTTtZQUN0QyxpQkFBaUI7U0FDbEIsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBQyxJQUFPLElBQUssT0FBQSxLQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBeEIsQ0FBd0IsQ0FBQyxDQUFDO0lBQzFGLENBQUM7SUFDRDs7Ozs7OztPQU9HO0lBQ0kscUNBQVcsR0FBbEIsVUFBc0IsRUFBTyxFQUFFLElBQWM7UUFBN0MsaUJBT0M7UUFQOEIscUJBQUEsRUFBQSxTQUFjO1FBQzNDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRTtZQUMxQiwwQkFBYyxDQUFDLE9BQU8sRUFBRTtZQUN4QiwwQkFBYyxDQUFDLGFBQWEsRUFBRTtZQUM5QixJQUFJLENBQUMsS0FBSyxDQUFDLGtCQUFrQixFQUFFLENBQUMsTUFBTTtZQUN0QyxLQUFLLEVBQUUsU0FBUztTQUNqQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxFQUFFLEVBQUUsSUFBQSxFQUFFLEVBQUUsU0FBUyxFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQU8sSUFBSyxPQUFBLEtBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUF4QixDQUF3QixDQUFDLENBQUM7SUFDdkYsQ0FBQztJQUNEOzs7Ozs7O09BT0c7SUFDSSw0Q0FBa0IsR0FBekI7UUFDRSxJQUFJLE9BQU8sR0FBRyxJQUFJLGlCQUFPLEVBQUUsQ0FBQztRQUM1QixFQUFFLENBQUMsQ0FBQyxPQUFPLFdBQVcsS0FBSyxXQUFXLENBQUMsQ0FBQyxDQUFDO1lBQ3ZDLElBQUksSUFBSSxHQUFLLFVBQUMsR0FBUSxJQUFLLE9BQUEsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFsQyxDQUFrQyxDQUFDO1lBQzlELElBQUksTUFBTSxHQUFHLElBQUksV0FBVyxDQUFDO2dCQUMzQiwwQkFBYyxDQUFDLE9BQU8sRUFBRTtnQkFDeEIsMEJBQWMsQ0FBQyxhQUFhLEVBQUU7Z0JBQzlCLElBQUksQ0FBQyxLQUFLLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxNQUFNO2dCQUN0QyxlQUFlO2FBQ2hCLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDYixNQUFNLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ3RDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3hCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLE9BQU8sQ0FBQyxJQUFJLENBQUMsMkNBQTJDLENBQUMsQ0FBQztRQUM1RCxDQUFDO1FBQ0QsTUFBTSxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUNoQyxDQUFDO0lBVUgsc0JBQUM7QUFBRCxDQUFDLEFBdlhELElBdVhDO0FBdlhxQixlQUFlO0lBRHBDLGlCQUFVLEVBQUU7SUFPUixXQUFBLGFBQU0sQ0FBQyxXQUFJLENBQUMsQ0FBQTtJQUNaLFdBQUEsYUFBTSxDQUFDLHFCQUFTLENBQUMsQ0FBQTtJQUNqQixXQUFBLGFBQU0sQ0FBQywyQkFBWSxDQUFDLENBQUE7SUFDcEIsV0FBQSxhQUFNLENBQUMsZ0NBQWdCLENBQUMsQ0FBQTtJQUN4QixXQUFBLGVBQVEsRUFBRSxDQUFBLEVBQUUsV0FBQSxhQUFNLENBQUMsNEJBQVksQ0FBQyxDQUFBO3FDQUpILFdBQUk7UUFDRyxxQkFBUztRQUNSLDJCQUFZO1FBQ0EsZ0NBQWdCO1FBQ1IsNEJBQVk7R0FWcEQsZUFBZSxDQXVYcEM7QUF2WHFCLDBDQUFlIiwic291cmNlc0NvbnRlbnQiOlsiLyogdHNsaW50OmRpc2FibGUgKi9cbmltcG9ydCB7IEluamVjdGFibGUsIEluamVjdCwgT3B0aW9uYWwgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHAsIEhlYWRlcnMsIFJlcXVlc3QgfSBmcm9tICdAYW5ndWxhci9odHRwJztcbmltcG9ydCB7IE5nTW9kdWxlLCBNb2R1bGVXaXRoUHJvdmlkZXJzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBKU09OU2VhcmNoUGFyYW1zIH0gZnJvbSAnLi9zZWFyY2gucGFyYW1zJztcbmltcG9ydCB7IEVycm9ySGFuZGxlciB9IGZyb20gJy4vZXJyb3Iuc2VydmljZSc7XG5pbXBvcnQgeyBMb29wQmFja0F1dGggfSBmcm9tICcuL2F1dGguc2VydmljZSc7XG5pbXBvcnQgeyBMb29wQmFja0NvbmZpZyB9IGZyb20gJy4uLy4uL2xiLmNvbmZpZyc7XG5pbXBvcnQgeyBMb29wQmFja0ZpbHRlciwgQWNjZXNzVG9rZW4gfSBmcm9tICcuLi8uLi9tb2RlbHMvQmFzZU1vZGVscyc7XG5pbXBvcnQgeyBTREtNb2RlbHMgfSBmcm9tICcuLi9jdXN0b20vU0RLTW9kZWxzJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzL09ic2VydmFibGUnO1xuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMvU3ViamVjdCc7XG5pbXBvcnQgeyBFcnJvck9ic2VydmFibGUgfSBmcm9tICdyeGpzL29ic2VydmFibGUvRXJyb3JPYnNlcnZhYmxlJztcbmltcG9ydCAncnhqcy9hZGQvb3BlcmF0b3IvY2F0Y2gnO1xuaW1wb3J0ICdyeGpzL2FkZC9vcGVyYXRvci9tYXAnO1xuLy8gTWFraW5nIFN1cmUgRXZlbnRTb3VyY2UgVHlwZSBpcyBhdmFpbGFibGUgdG8gYXZvaWQgY29tcGlsYXRpb24gaXNzdWVzLlxuZGVjbGFyZSB2YXIgRXZlbnRTb3VyY2U6IGFueTtcbi8qKlxuKiBAbW9kdWxlIEJhc2VMb29wQmFja0FwaVxuKiBAYXV0aG9yIEpvbmF0aGFuIENhc2FycnViaWFzIDxAam9obmNhc2FycnViaWFzPiA8Z2l0aHViOmpvbmF0aGFuLWNhc2FycnViaWFzPlxuKiBAYXV0aG9yIE5pa29sYXkgTWF0aXVzaGVua292IDxodHRwczovL2dpdGh1Yi5jb20vbW52eD5cbiogQGxpY2Vuc2UgTUlUXG4qIEBkZXNjcmlwdGlvblxuKiBBYnN0cmFjdCBjbGFzcyB0aGF0IHdpbGwgYmUgaW1wbGVtZW50ZWQgaW4gZXZlcnkgY3VzdG9tIHNlcnZpY2UgYXV0b21hdGljYWxseSBidWlsdFxuKiBieSB0aGUgc2RrIGJ1aWxkZXIuXG4qIEl0IHByb3ZpZGVzIHRoZSBjb3JlIGZ1bmN0aW9uYWxsaXR5IGZvciBldmVyeSBBUEkgY2FsbCwgZWl0aGVyIGJ5IEhUVFAgQ2FsbHMgb3IgYnlcbiogV2ViU29ja2V0cy5cbioqL1xuQEluamVjdGFibGUoKVxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEJhc2VMb29wQmFja0FwaSB7XG5cbiAgcHJvdGVjdGVkIHBhdGg6IHN0cmluZztcbiAgcHJvdGVjdGVkIG1vZGVsOiBhbnk7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgQEluamVjdChIdHRwKSBwcm90ZWN0ZWQgaHR0cDogSHR0cCxcbiAgICBASW5qZWN0KFNES01vZGVscykgcHJvdGVjdGVkIG1vZGVsczogU0RLTW9kZWxzLFxuICAgIEBJbmplY3QoTG9vcEJhY2tBdXRoKSBwcm90ZWN0ZWQgYXV0aDogTG9vcEJhY2tBdXRoLFxuICAgIEBJbmplY3QoSlNPTlNlYXJjaFBhcmFtcykgcHJvdGVjdGVkIHNlYXJjaFBhcmFtczogSlNPTlNlYXJjaFBhcmFtcyxcbiAgICBAT3B0aW9uYWwoKSBASW5qZWN0KEVycm9ySGFuZGxlcikgcHJvdGVjdGVkIGVycm9ySGFuZGxlcjogRXJyb3JIYW5kbGVyXG4gICkge1xuICAgIHRoaXMubW9kZWwgPSB0aGlzLm1vZGVscy5nZXQodGhpcy5nZXRNb2RlbE5hbWUoKSk7XG4gIH1cbiAgLyoqXG4gICAqIEBtZXRob2QgcmVxdWVzdFxuICAgKiBAcGFyYW0ge3N0cmluZ30gIG1ldGhvZCAgICAgIFJlcXVlc3QgbWV0aG9kIChHRVQsIFBPU1QsIFBVVClcbiAgICogQHBhcmFtIHtzdHJpbmd9ICB1cmwgICAgICAgICBSZXF1ZXN0IHVybCAobXktaG9zdC9teS11cmwvOmlkKVxuICAgKiBAcGFyYW0ge2FueX0gICAgIHJvdXRlUGFyYW1zIFZhbHVlcyBvZiB1cmwgcGFyYW1ldGVyc1xuICAgKiBAcGFyYW0ge2FueX0gICAgIHVybFBhcmFtcyAgIFBhcmFtZXRlcnMgZm9yIGJ1aWxkaW5nIHVybCAoZmlsdGVyIGFuZCBvdGhlcilcbiAgICogQHBhcmFtIHthbnl9ICAgICBwb3N0Qm9keSAgICBSZXF1ZXN0IHBvc3RCb2R5XG4gICAqIEByZXR1cm4ge09ic2VydmFibGU8YW55Pn1cbiAgICogQGRlc2NyaXB0aW9uXG4gICAqIFRoaXMgaXMgYSBjb3JlIG1ldGhvZCwgZXZlcnkgSFRUUCBDYWxsIHdpbGwgYmUgZG9uZSBmcm9tIGhlcmUsIGV2ZXJ5IEFQSSBTZXJ2aWNlIHdpbGxcbiAgICogZXh0ZW5kIHRoaXMgY2xhc3MgYW5kIHVzZSB0aGlzIG1ldGhvZCB0byBnZXQgUkVTVGZ1bCBjb21tdW5pY2F0aW9uLlxuICAgKiovXG4gIHB1YmxpYyByZXF1ZXN0KFxuICAgIG1ldGhvZCAgICAgIDogc3RyaW5nLFxuICAgIHVybCAgICAgICAgIDogc3RyaW5nLFxuICAgIHJvdXRlUGFyYW1zIDogYW55ID0ge30sXG4gICAgdXJsUGFyYW1zICAgOiBhbnkgPSB7fSxcbiAgICBwb3N0Qm9keSAgICA6IGFueSA9IHt9XG4gICk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgLy8gSGVhZGVycyB0byBiZSBzZW50XG4gICAgbGV0IGhlYWRlcnM6IEhlYWRlcnMgPSBuZXcgSGVhZGVycygpO1xuICAgIGhlYWRlcnMuYXBwZW5kKCdDb250ZW50LVR5cGUnLCAnYXBwbGljYXRpb24vanNvbicpO1xuICAgIC8vIEF1dGhlbnRpY2F0ZSByZXF1ZXN0XG4gICAgdGhpcy5hdXRoZW50aWNhdGUodXJsLCBoZWFkZXJzKTtcbiAgICAvLyBUcmFuc3BpbGUgcm91dGUgdmFyaWFibGVzIHRvIHRoZSBhY3R1YWwgcmVxdWVzdCBWYWx1ZXNcbiAgICBPYmplY3Qua2V5cyhyb3V0ZVBhcmFtcykuZm9yRWFjaCgoa2V5OiBzdHJpbmcpID0+IHtcbiAgICAgIHVybCA9IHVybC5yZXBsYWNlKG5ldyBSZWdFeHAoXCI6XCIgKyBrZXkgKyBcIihcXC98JClcIiwgXCJnXCIpLCByb3V0ZVBhcmFtc1trZXldICsgXCIkMVwiKVxuICAgIH0pO1xuICAgIC8vIEJvZHkgZml4IGZvciBidWlsdCBpbiByZW1vdGUgbWV0aG9kcyB1c2luZyBcImRhdGFcIiwgXCJvcHRpb25zXCIgb3IgXCJjcmVkZW50aWFsc1xuICAgIC8vIHRoYXQgYXJlIHRoZSBhY3R1YWwgYm9keSwgQ3VzdG9tIHJlbW90ZSBtZXRob2QgcHJvcGVydGllcyBhcmUgZGlmZmVyZW50IGFuZCBuZWVkXG4gICAgLy8gdG8gYmUgd3JhcHBlZCBpbnRvIGEgYm9keSBvYmplY3RcbiAgICBsZXQgYm9keTogYW55O1xuICAgIGxldCBwb3N0Qm9keUtleXMgPSB0eXBlb2YgcG9zdEJvZHkgPT09ICdvYmplY3QnID8gT2JqZWN0LmtleXMocG9zdEJvZHkpIDogW11cbiAgICBpZiAocG9zdEJvZHlLZXlzLmxlbmd0aCA9PT0gMSkge1xuICAgICAgYm9keSA9IHBvc3RCb2R5W3Bvc3RCb2R5S2V5c1swXV07XG4gICAgfSBlbHNlIHtcbiAgICAgIGJvZHkgPSBwb3N0Qm9keTtcbiAgICB9XG4gICAgLy8gU2VwYXJhdGUgZmlsdGVyIG9iamVjdCBmcm9tIHVybCBwYXJhbXMgYW5kIGFkZCB0byBzZWFyY2ggcXVlcnlcbiAgICBpZiAodXJsUGFyYW1zLmZpbHRlcikge1xuICAgICAgaGVhZGVycy5hcHBlbmQoJ2ZpbHRlcicsIEpTT04uc3RyaW5naWZ5KHVybFBhcmFtcy5maWx0ZXIpKTtcbiAgICAgIGRlbGV0ZSB1cmxQYXJhbXMuZmlsdGVyO1xuICAgIH1cbiAgICAvLyBTZXBhcmF0ZSB3aGVyZSBvYmplY3QgZnJvbSB1cmwgcGFyYW1zIGFuZCBhZGQgdG8gc2VhcmNoIHF1ZXJ5XG4gICAgaWYgKHVybFBhcmFtcy53aGVyZSkge1xuICAgICAgaGVhZGVycy5hcHBlbmQoJ3doZXJlJywgSlNPTi5zdHJpbmdpZnkodXJsUGFyYW1zLndoZXJlKSk7XG4gICAgICBkZWxldGUgdXJsUGFyYW1zLndoZXJlO1xuICAgIH1cbiAgICB0aGlzLnNlYXJjaFBhcmFtcy5zZXRKU09OKHVybFBhcmFtcyk7XG4gICAgbGV0IHJlcXVlc3Q6IFJlcXVlc3QgPSBuZXcgUmVxdWVzdCh7XG4gICAgICBoZWFkZXJzIDogaGVhZGVycyxcbiAgICAgIG1ldGhvZCAgOiBtZXRob2QsXG4gICAgICB1cmwgICAgIDogdXJsLFxuICAgICAgc2VhcmNoICA6IE9iamVjdC5rZXlzKHVybFBhcmFtcykubGVuZ3RoID4gMFxuICAgICAgICAgICAgICA/IHRoaXMuc2VhcmNoUGFyYW1zLmdldFVSTFNlYXJjaFBhcmFtcygpIDogbnVsbCxcbiAgICAgIGJvZHkgICAgOiBib2R5ID8gSlNPTi5zdHJpbmdpZnkoYm9keSkgOiB1bmRlZmluZWRcbiAgICB9KTtcbiAgICByZXR1cm4gdGhpcy5odHRwLnJlcXVlc3QocmVxdWVzdClcbiAgICAgIC5tYXAoKHJlczogYW55KSA9PiAocmVzLnRleHQoKSAhPSBcIlwiID8gcmVzLmpzb24oKSA6IHt9KSlcbiAgICAgIC5jYXRjaCgoZSkgPT4gdGhpcy5lcnJvckhhbmRsZXIuaGFuZGxlRXJyb3IoZSkpO1xuICB9XG4gIC8qKlxuICAgKiBAbWV0aG9kIGF1dGhlbnRpY2F0ZVxuICAgKiBAYXV0aG9yIEpvbmF0aGFuIENhc2FycnViaWFzIDx0OiBqb2huY2FzYXJydWJpYXMsIGdoOiBtZWFuLWV4cGVydC1vZmZpY2lhbD5cbiAgICogQGxpY2Vuc2UgTUlUXG4gICAqIEBwYXJhbSB7c3RyaW5nfSB1cmwgU2VydmVyIFVSTFxuICAgKiBAcGFyYW0ge0hlYWRlcnN9IGhlYWRlcnMgSFRUUCBIZWFkZXJzXG4gICAqIEByZXR1cm4ge3ZvaWR9XG4gICAqIEBkZXNjcmlwdGlvblxuICAgKiBUaGlzIG1ldGhvZCB3aWxsIHRyeSB0byBhdXRoZW50aWNhdGUgdXNpbmcgZWl0aGVyIGFuIGFjY2Vzc190b2tlbiBvciBiYXNpYyBodHRwIGF1dGhcbiAgICovXG4gIHB1YmxpYyBhdXRoZW50aWNhdGU8VD4odXJsOiBzdHJpbmcsIGhlYWRlcnM6IEhlYWRlcnMpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5hdXRoLmdldEFjY2Vzc1Rva2VuSWQoKSkge1xuICAgICAgaGVhZGVycy5hcHBlbmQoXG4gICAgICAgICdBdXRob3JpemF0aW9uJyxcbiAgICAgICAgTG9vcEJhY2tDb25maWcuZ2V0QXV0aFByZWZpeCgpICsgdGhpcy5hdXRoLmdldEFjY2Vzc1Rva2VuSWQoKVxuICAgICAgKTtcbiAgICB9XG4gIH1cbiAgLyoqXG4gICAqIEBtZXRob2QgY3JlYXRlXG4gICAqIEBhdXRob3IgSm9uYXRoYW4gQ2FzYXJydWJpYXMgPHQ6IGpvaG5jYXNhcnJ1YmlhcywgZ2g6IG1lYW4tZXhwZXJ0LW9mZmljaWFsPlxuICAgKiBAbGljZW5zZSBNSVRcbiAgICogQHBhcmFtIHtUfSBkYXRhIEdlbmVyaWMgZGF0YSB0eXBlXG4gICAqIEByZXR1cm4ge09ic2VydmFibGU8VD59XG4gICAqIEBkZXNjcmlwdGlvblxuICAgKiBHZW5lcmljIGNyZWF0ZSBtZXRob2RcbiAgICovXG4gIHB1YmxpYyBjcmVhdGU8VD4oZGF0YTogVCk6IE9ic2VydmFibGU8VD4ge1xuICAgIHJldHVybiB0aGlzLnJlcXVlc3QoJ1BPU1QnLCBbXG4gICAgICBMb29wQmFja0NvbmZpZy5nZXRQYXRoKCksXG4gICAgICBMb29wQmFja0NvbmZpZy5nZXRBcGlWZXJzaW9uKCksXG4gICAgICB0aGlzLm1vZGVsLmdldE1vZGVsRGVmaW5pdGlvbigpLnBsdXJhbFxuICAgIF0uam9pbignLycpLCB1bmRlZmluZWQsIHVuZGVmaW5lZCwgeyBkYXRhIH0pLm1hcCgoZGF0YTogVCkgPT4gdGhpcy5tb2RlbC5mYWN0b3J5KGRhdGEpKTtcbiAgfVxuICAvKipcbiAgICogQG1ldGhvZCBjcmVhdGVcbiAgICogQGF1dGhvciBKb25hdGhhbiBDYXNhcnJ1YmlhcyA8dDogam9obmNhc2FycnViaWFzLCBnaDogbWVhbi1leHBlcnQtb2ZmaWNpYWw+XG4gICAqIEBsaWNlbnNlIE1JVFxuICAgKiBAcGFyYW0ge1RbXX0gZGF0YSBHZW5lcmljIGRhdGEgdHlwZSBhcnJheVxuICAgKiBAcmV0dXJuIHtPYnNlcnZhYmxlPFRbXT59XG4gICAqIEBkZXNjcmlwdGlvblxuICAgKiBHZW5lcmljIGNyZWF0ZSBtYW55IG1ldGhvZFxuICAgKi9cbiAgcHVibGljIGNyZWF0ZU1hbnk8VD4oZGF0YTogVFtdKTogT2JzZXJ2YWJsZTxUW10+IHtcbiAgICByZXR1cm4gdGhpcy5yZXF1ZXN0KCdQT1NUJywgW1xuICAgICAgTG9vcEJhY2tDb25maWcuZ2V0UGF0aCgpLFxuICAgICAgTG9vcEJhY2tDb25maWcuZ2V0QXBpVmVyc2lvbigpLFxuICAgICAgdGhpcy5tb2RlbC5nZXRNb2RlbERlZmluaXRpb24oKS5wbHVyYWxcbiAgICBdLmpvaW4oJy8nKSwgdW5kZWZpbmVkLCB1bmRlZmluZWQsIHsgZGF0YSB9KVxuICAgIC5tYXAoKGRhdHVtOiBUW10pID0+IGRhdHVtLm1hcCgoZGF0YTogVCkgPT4gdGhpcy5tb2RlbC5mYWN0b3J5KGRhdGEpKSk7XG4gIH1cbiAgLyoqXG4gICAqIEBtZXRob2QgZmluZEJ5SWRcbiAgICogQGF1dGhvciBKb25hdGhhbiBDYXNhcnJ1YmlhcyA8dDogam9obmNhc2FycnViaWFzLCBnaDogbWVhbi1leHBlcnQtb2ZmaWNpYWw+XG4gICAqIEBsaWNlbnNlIE1JVFxuICAgKiBAcGFyYW0ge2FueX0gZGF0YSBHZW5lcmljIGRhdGEgdHlwZVxuICAgKiBAcmV0dXJuIHtPYnNlcnZhYmxlPFQ+fVxuICAgKiBAZGVzY3JpcHRpb25cbiAgICogR2VuZXJpYyBmaW5kQnlJZCBtZXRob2RcbiAgICovXG4gIHB1YmxpYyBmaW5kQnlJZDxUPihpZDogYW55LCBmaWx0ZXI6IExvb3BCYWNrRmlsdGVyID0ge30pOiBPYnNlcnZhYmxlPFQ+IHtcbiAgICBsZXQgX3VybFBhcmFtczogYW55ID0ge307XG4gICAgaWYgKGZpbHRlcikgX3VybFBhcmFtcy5maWx0ZXIgPSBmaWx0ZXI7XG4gICAgcmV0dXJuIHRoaXMucmVxdWVzdCgnR0VUJywgW1xuICAgICAgTG9vcEJhY2tDb25maWcuZ2V0UGF0aCgpLFxuICAgICAgTG9vcEJhY2tDb25maWcuZ2V0QXBpVmVyc2lvbigpLFxuICAgICAgdGhpcy5tb2RlbC5nZXRNb2RlbERlZmluaXRpb24oKS5wbHVyYWwsXG4gICAgICAnOmlkJ1xuICAgIF0uam9pbignLycpLCB7IGlkIH0sIF91cmxQYXJhbXMsIHVuZGVmaW5lZCkubWFwKChkYXRhOiBUKSA9PiB0aGlzLm1vZGVsLmZhY3RvcnkoZGF0YSkpO1xuICB9XG4gIC8qKlxuICAgKiBAbWV0aG9kIGZpbmRcbiAgICogQGF1dGhvciBKb25hdGhhbiBDYXNhcnJ1YmlhcyA8dDogam9obmNhc2FycnViaWFzLCBnaDogbWVhbi1leHBlcnQtb2ZmaWNpYWw+XG4gICAqIEBsaWNlbnNlIE1JVFxuICAgKiBAcmV0dXJuIHtPYnNlcnZhYmxlPFRbKz59XG4gICAqIEBkZXNjcmlwdGlvblxuICAgKiBHZW5lcmljIGZpbmQgbWV0aG9kXG4gICAqL1xuICBwdWJsaWMgZmluZDxUPihmaWx0ZXI6IExvb3BCYWNrRmlsdGVyID0ge30pOiBPYnNlcnZhYmxlPFRbXT4ge1xuICAgIHJldHVybiB0aGlzLnJlcXVlc3QoJ0dFVCcsIFtcbiAgICAgIExvb3BCYWNrQ29uZmlnLmdldFBhdGgoKSxcbiAgICAgIExvb3BCYWNrQ29uZmlnLmdldEFwaVZlcnNpb24oKSxcbiAgICAgIHRoaXMubW9kZWwuZ2V0TW9kZWxEZWZpbml0aW9uKCkucGx1cmFsXG4gICAgXS5qb2luKCcvJyksIHVuZGVmaW5lZCwgeyBmaWx0ZXIgfSwgdW5kZWZpbmVkKVxuICAgIC5tYXAoKGRhdHVtOiBUW10pID0+IGRhdHVtLm1hcCgoZGF0YTogVCkgPT4gdGhpcy5tb2RlbC5mYWN0b3J5KGRhdGEpKSk7XG4gIH1cbiAgLyoqXG4gICAqIEBtZXRob2QgZXhpc3RzXG4gICAqIEBhdXRob3IgSm9uYXRoYW4gQ2FzYXJydWJpYXMgPHQ6IGpvaG5jYXNhcnJ1YmlhcywgZ2g6IG1lYW4tZXhwZXJ0LW9mZmljaWFsPlxuICAgKiBAbGljZW5zZSBNSVRcbiAgICogQHJldHVybiB7T2JzZXJ2YWJsZTxUW10+fVxuICAgKiBAZGVzY3JpcHRpb25cbiAgICogR2VuZXJpYyBleGlzdHMgbWV0aG9kXG4gICAqL1xuICBwdWJsaWMgZXhpc3RzPFQ+KGlkOiBhbnkpOiBPYnNlcnZhYmxlPFRbXT4ge1xuICAgIHJldHVybiB0aGlzLnJlcXVlc3QoJ0dFVCcsIFtcbiAgICAgIExvb3BCYWNrQ29uZmlnLmdldFBhdGgoKSxcbiAgICAgIExvb3BCYWNrQ29uZmlnLmdldEFwaVZlcnNpb24oKSxcbiAgICAgIHRoaXMubW9kZWwuZ2V0TW9kZWxEZWZpbml0aW9uKCkucGx1cmFsLFxuICAgICAgJzppZC9leGlzdHMnXG4gICAgXS5qb2luKCcvJyksIHsgaWQgfSwgdW5kZWZpbmVkLCB1bmRlZmluZWQpO1xuICB9XG4gIC8qKlxuICAgKiBAbWV0aG9kIGZpbmRPbmVcbiAgICogQGF1dGhvciBKb25hdGhhbiBDYXNhcnJ1YmlhcyA8dDogam9obmNhc2FycnViaWFzLCBnaDogbWVhbi1leHBlcnQtb2ZmaWNpYWw+XG4gICAqIEBsaWNlbnNlIE1JVFxuICAgKiBAcmV0dXJuIHtPYnNlcnZhYmxlPFQ+fVxuICAgKiBAZGVzY3JpcHRpb25cbiAgICogR2VuZXJpYyBmaW5kT25lIG1ldGhvZFxuICAgKi9cbiAgcHVibGljIGZpbmRPbmU8VD4oZmlsdGVyOiBMb29wQmFja0ZpbHRlciA9IHt9KTogT2JzZXJ2YWJsZTxUPiB7XG4gICAgcmV0dXJuIHRoaXMucmVxdWVzdCgnR0VUJywgW1xuICAgICAgTG9vcEJhY2tDb25maWcuZ2V0UGF0aCgpLFxuICAgICAgTG9vcEJhY2tDb25maWcuZ2V0QXBpVmVyc2lvbigpLFxuICAgICAgdGhpcy5tb2RlbC5nZXRNb2RlbERlZmluaXRpb24oKS5wbHVyYWwsXG4gICAgICAnZmluZE9uZSdcbiAgICBdLmpvaW4oJy8nKSwgdW5kZWZpbmVkLCB7IGZpbHRlciB9LCB1bmRlZmluZWQpLm1hcCgoZGF0YTogVCkgPT4gdGhpcy5tb2RlbC5mYWN0b3J5KGRhdGEpKTtcbiAgfVxuICAvKipcbiAgICogQG1ldGhvZCB1cGRhdGVBbGxcbiAgICogQGF1dGhvciBKb25hdGhhbiBDYXNhcnJ1YmlhcyA8dDogam9obmNhc2FycnViaWFzLCBnaDogbWVhbi1leHBlcnQtb2ZmaWNpYWw+XG4gICAqIEBsaWNlbnNlIE1JVFxuICAgKiBAcmV0dXJuIHtPYnNlcnZhYmxlPFRbXT59XG4gICAqIEBkZXNjcmlwdGlvblxuICAgKiBHZW5lcmljIHVwZGF0ZUFsbCBtZXRob2RcbiAgICovXG4gIHB1YmxpYyB1cGRhdGVBbGw8VD4od2hlcmU6IGFueSA9IHt9LCBkYXRhOiBUKTogT2JzZXJ2YWJsZTx7IGNvdW50OiAnbnVtYmVyJyB9PiB7XG4gICAgbGV0IF91cmxQYXJhbXM6IGFueSA9IHt9O1xuICAgIGlmICh3aGVyZSkgX3VybFBhcmFtcy53aGVyZSA9IHdoZXJlO1xuICAgIHJldHVybiB0aGlzLnJlcXVlc3QoJ1BPU1QnLCBbXG4gICAgICBMb29wQmFja0NvbmZpZy5nZXRQYXRoKCksXG4gICAgICBMb29wQmFja0NvbmZpZy5nZXRBcGlWZXJzaW9uKCksXG4gICAgICB0aGlzLm1vZGVsLmdldE1vZGVsRGVmaW5pdGlvbigpLnBsdXJhbCxcbiAgICAgICd1cGRhdGUnXG4gICAgXS5qb2luKCcvJyksIHVuZGVmaW5lZCwgX3VybFBhcmFtcywgeyBkYXRhIH0pO1xuICB9XG4gIC8qKlxuICAgKiBAbWV0aG9kIGRlbGV0ZUJ5SWRcbiAgICogQGF1dGhvciBKb25hdGhhbiBDYXNhcnJ1YmlhcyA8dDogam9obmNhc2FycnViaWFzLCBnaDogbWVhbi1leHBlcnQtb2ZmaWNpYWw+XG4gICAqIEBsaWNlbnNlIE1JVFxuICAgKiBAcmV0dXJuIHtPYnNlcnZhYmxlPFQ+fVxuICAgKiBAZGVzY3JpcHRpb25cbiAgICogR2VuZXJpYyBkZWxldGVCeUlkIG1ldGhvZFxuICAgKi9cbiAgcHVibGljIGRlbGV0ZUJ5SWQ8VD4oaWQ6IGFueSk6IE9ic2VydmFibGU8VD4ge1xuICAgIHJldHVybiB0aGlzLnJlcXVlc3QoJ0RFTEVURScsIFtcbiAgICAgIExvb3BCYWNrQ29uZmlnLmdldFBhdGgoKSxcbiAgICAgIExvb3BCYWNrQ29uZmlnLmdldEFwaVZlcnNpb24oKSxcbiAgICAgIHRoaXMubW9kZWwuZ2V0TW9kZWxEZWZpbml0aW9uKCkucGx1cmFsLFxuICAgICAgJzppZCdcbiAgICBdLmpvaW4oJy8nKSwgeyBpZCB9LCB1bmRlZmluZWQsIHVuZGVmaW5lZCkubWFwKChkYXRhOiBUKSA9PiB0aGlzLm1vZGVsLmZhY3RvcnkoZGF0YSkpO1xuICB9XG4gIC8qKlxuICAgKiBAbWV0aG9kIGNvdW50XG4gICAqIEBhdXRob3IgSm9uYXRoYW4gQ2FzYXJydWJpYXMgPHQ6IGpvaG5jYXNhcnJ1YmlhcywgZ2g6IG1lYW4tZXhwZXJ0LW9mZmljaWFsPlxuICAgKiBAbGljZW5zZSBNSVRcbiAgICogQHJldHVybiB7T2JzZXJ2YWJsZTx7IGNvdW50OiBudW1iZXIgfT59XG4gICAqIEBkZXNjcmlwdGlvblxuICAgKiBHZW5lcmljIGNvdW50IG1ldGhvZFxuICAgKi9cbiAgcHVibGljIGNvdW50KHdoZXJlOiBhbnkgPSB7fSk6IE9ic2VydmFibGU8eyBjb3VudDogbnVtYmVyIH0+IHtcbiAgICBsZXQgX3VybFBhcmFtczogYW55ID0ge307XG4gICAgaWYgKHdoZXJlKSBfdXJsUGFyYW1zLndoZXJlID0gd2hlcmU7XG4gICAgcmV0dXJuIHRoaXMucmVxdWVzdCgnR0VUJywgW1xuICAgICAgTG9vcEJhY2tDb25maWcuZ2V0UGF0aCgpLFxuICAgICAgTG9vcEJhY2tDb25maWcuZ2V0QXBpVmVyc2lvbigpLFxuICAgICAgdGhpcy5tb2RlbC5nZXRNb2RlbERlZmluaXRpb24oKS5wbHVyYWwsXG4gICAgICAnY291bnQnXG4gICAgXS5qb2luKCcvJyksIHVuZGVmaW5lZCwgX3VybFBhcmFtcywgdW5kZWZpbmVkKTtcbiAgfVxuICAvKipcbiAgICogQG1ldGhvZCB1cGRhdGVBdHRyaWJ1dGVzXG4gICAqIEBhdXRob3IgSm9uYXRoYW4gQ2FzYXJydWJpYXMgPHQ6IGpvaG5jYXNhcnJ1YmlhcywgZ2g6IG1lYW4tZXhwZXJ0LW9mZmljaWFsPlxuICAgKiBAbGljZW5zZSBNSVRcbiAgICogQHJldHVybiB7T2JzZXJ2YWJsZTxUPn1cbiAgICogQGRlc2NyaXB0aW9uXG4gICAqIEdlbmVyaWMgdXBkYXRlQXR0cmlidXRlcyBtZXRob2RcbiAgICovXG4gIHB1YmxpYyB1cGRhdGVBdHRyaWJ1dGVzPFQ+KGlkOiBhbnksIGRhdGE6IFQpOiBPYnNlcnZhYmxlPFQ+IHtcbiAgICByZXR1cm4gdGhpcy5yZXF1ZXN0KCdQVVQnLCBbXG4gICAgICBMb29wQmFja0NvbmZpZy5nZXRQYXRoKCksXG4gICAgICBMb29wQmFja0NvbmZpZy5nZXRBcGlWZXJzaW9uKCksXG4gICAgICB0aGlzLm1vZGVsLmdldE1vZGVsRGVmaW5pdGlvbigpLnBsdXJhbCxcbiAgICAgICc6aWQnXG4gICAgXS5qb2luKCcvJyksIHsgaWQgfSwgdW5kZWZpbmVkLCB7IGRhdGEgfSkubWFwKChkYXRhOiBUKSA9PiB0aGlzLm1vZGVsLmZhY3RvcnkoZGF0YSkpO1xuICB9XG4gIC8qKlxuICAgKiBAbWV0aG9kIHVwc2VydFxuICAgKiBAYXV0aG9yIEpvbmF0aGFuIENhc2FycnViaWFzIDx0OiBqb2huY2FzYXJydWJpYXMsIGdoOiBtZWFuLWV4cGVydC1vZmZpY2lhbD5cbiAgICogQGxpY2Vuc2UgTUlUXG4gICAqIEByZXR1cm4ge09ic2VydmFibGU8VD59XG4gICAqIEBkZXNjcmlwdGlvblxuICAgKiBHZW5lcmljIHVwc2VydCBtZXRob2RcbiAgICovXG4gIHB1YmxpYyB1cHNlcnQ8VD4oZGF0YTogYW55ID0ge30pOiBPYnNlcnZhYmxlPFQ+IHtcbiAgICByZXR1cm4gdGhpcy5yZXF1ZXN0KCdQVVQnLCBbXG4gICAgICBMb29wQmFja0NvbmZpZy5nZXRQYXRoKCksXG4gICAgICBMb29wQmFja0NvbmZpZy5nZXRBcGlWZXJzaW9uKCksXG4gICAgICB0aGlzLm1vZGVsLmdldE1vZGVsRGVmaW5pdGlvbigpLnBsdXJhbCxcbiAgICBdLmpvaW4oJy8nKSwgdW5kZWZpbmVkLCB1bmRlZmluZWQsIHsgZGF0YSB9KS5tYXAoKGRhdGE6IFQpID0+IHRoaXMubW9kZWwuZmFjdG9yeShkYXRhKSk7XG4gIH1cbiAgLyoqXG4gICAqIEBtZXRob2QgdXBzZXJ0UGF0Y2hcbiAgICogQGF1dGhvciBKb25hdGhhbiBDYXNhcnJ1YmlhcyA8dDogam9obmNhc2FycnViaWFzLCBnaDogbWVhbi1leHBlcnQtb2ZmaWNpYWw+XG4gICAqIEBsaWNlbnNlIE1JVFxuICAgKiBAcmV0dXJuIHtPYnNlcnZhYmxlPFQ+fVxuICAgKiBAZGVzY3JpcHRpb25cbiAgICogR2VuZXJpYyB1cHNlcnQgbWV0aG9kIHVzaW5nIHBhdGNoIGh0dHAgbWV0aG9kXG4gICAqL1xuICBwdWJsaWMgdXBzZXJ0UGF0Y2g8VD4oZGF0YTogYW55ID0ge30pOiBPYnNlcnZhYmxlPFQ+IHtcbiAgICByZXR1cm4gdGhpcy5yZXF1ZXN0KCdQQVRDSCcsIFtcbiAgICAgIExvb3BCYWNrQ29uZmlnLmdldFBhdGgoKSxcbiAgICAgIExvb3BCYWNrQ29uZmlnLmdldEFwaVZlcnNpb24oKSxcbiAgICAgIHRoaXMubW9kZWwuZ2V0TW9kZWxEZWZpbml0aW9uKCkucGx1cmFsLFxuICAgIF0uam9pbignLycpLCB1bmRlZmluZWQsIHVuZGVmaW5lZCwgeyBkYXRhIH0pLm1hcCgoZGF0YTogVCkgPT4gdGhpcy5tb2RlbC5mYWN0b3J5KGRhdGEpKTtcbiAgfVxuICAvKipcbiAgICogQG1ldGhvZCB1cHNlcnRXaXRoV2hlcmVcbiAgICogQGF1dGhvciBKb25hdGhhbiBDYXNhcnJ1YmlhcyA8dDogam9obmNhc2FycnViaWFzLCBnaDogbWVhbi1leHBlcnQtb2ZmaWNpYWw+XG4gICAqIEBsaWNlbnNlIE1JVFxuICAgKiBAcmV0dXJuIHtPYnNlcnZhYmxlPFQ+fVxuICAgKiBAZGVzY3JpcHRpb25cbiAgICogR2VuZXJpYyB1cHNlcnRXaXRoV2hlcmUgbWV0aG9kXG4gICAqL1xuICBwdWJsaWMgdXBzZXJ0V2l0aFdoZXJlPFQ+KHdoZXJlOiBhbnkgPSB7fSwgZGF0YTogYW55ID0ge30pOiBPYnNlcnZhYmxlPFQ+IHtcbiAgICBsZXQgX3VybFBhcmFtczogYW55ID0ge307XG4gICAgaWYgKHdoZXJlKSBfdXJsUGFyYW1zLndoZXJlID0gd2hlcmU7XG4gICAgcmV0dXJuIHRoaXMucmVxdWVzdCgnUE9TVCcsIFtcbiAgICAgIExvb3BCYWNrQ29uZmlnLmdldFBhdGgoKSxcbiAgICAgIExvb3BCYWNrQ29uZmlnLmdldEFwaVZlcnNpb24oKSxcbiAgICAgIHRoaXMubW9kZWwuZ2V0TW9kZWxEZWZpbml0aW9uKCkucGx1cmFsLFxuICAgICAgJ3Vwc2VydFdpdGhXaGVyZSdcbiAgICBdLmpvaW4oJy8nKSwgdW5kZWZpbmVkLCBfdXJsUGFyYW1zLCB7IGRhdGEgfSkubWFwKChkYXRhOiBUKSA9PiB0aGlzLm1vZGVsLmZhY3RvcnkoZGF0YSkpO1xuICB9XG4gIC8qKlxuICAgKiBAbWV0aG9kIHJlcGxhY2VPckNyZWF0ZVxuICAgKiBAYXV0aG9yIEpvbmF0aGFuIENhc2FycnViaWFzIDx0OiBqb2huY2FzYXJydWJpYXMsIGdoOiBtZWFuLWV4cGVydC1vZmZpY2lhbD5cbiAgICogQGxpY2Vuc2UgTUlUXG4gICAqIEByZXR1cm4ge09ic2VydmFibGU8VD59XG4gICAqIEBkZXNjcmlwdGlvblxuICAgKiBHZW5lcmljIHJlcGxhY2VPckNyZWF0ZSBtZXRob2RcbiAgICovXG4gIHB1YmxpYyByZXBsYWNlT3JDcmVhdGU8VD4oZGF0YTogYW55ID0ge30pOiBPYnNlcnZhYmxlPFQ+IHtcbiAgICByZXR1cm4gdGhpcy5yZXF1ZXN0KCdQT1NUJywgW1xuICAgICAgTG9vcEJhY2tDb25maWcuZ2V0UGF0aCgpLFxuICAgICAgTG9vcEJhY2tDb25maWcuZ2V0QXBpVmVyc2lvbigpLFxuICAgICAgdGhpcy5tb2RlbC5nZXRNb2RlbERlZmluaXRpb24oKS5wbHVyYWwsXG4gICAgICAncmVwbGFjZU9yQ3JlYXRlJ1xuICAgIF0uam9pbignLycpLCB1bmRlZmluZWQsIHVuZGVmaW5lZCwgeyBkYXRhIH0pLm1hcCgoZGF0YTogVCkgPT4gdGhpcy5tb2RlbC5mYWN0b3J5KGRhdGEpKTtcbiAgfVxuICAvKipcbiAgICogQG1ldGhvZCByZXBsYWNlQnlJZFxuICAgKiBAYXV0aG9yIEpvbmF0aGFuIENhc2FycnViaWFzIDx0OiBqb2huY2FzYXJydWJpYXMsIGdoOiBtZWFuLWV4cGVydC1vZmZpY2lhbD5cbiAgICogQGxpY2Vuc2UgTUlUXG4gICAqIEByZXR1cm4ge09ic2VydmFibGU8VD59XG4gICAqIEBkZXNjcmlwdGlvblxuICAgKiBHZW5lcmljIHJlcGxhY2VCeUlkIG1ldGhvZFxuICAgKi9cbiAgcHVibGljIHJlcGxhY2VCeUlkPFQ+KGlkOiBhbnksIGRhdGE6IGFueSA9IHt9KTogT2JzZXJ2YWJsZTxUPiB7XG4gICAgcmV0dXJuIHRoaXMucmVxdWVzdCgnUE9TVCcsIFtcbiAgICAgIExvb3BCYWNrQ29uZmlnLmdldFBhdGgoKSxcbiAgICAgIExvb3BCYWNrQ29uZmlnLmdldEFwaVZlcnNpb24oKSxcbiAgICAgIHRoaXMubW9kZWwuZ2V0TW9kZWxEZWZpbml0aW9uKCkucGx1cmFsLFxuICAgICAgJzppZCcsICdyZXBsYWNlJ1xuICAgIF0uam9pbignLycpLCB7IGlkIH0sIHVuZGVmaW5lZCwgeyBkYXRhIH0pLm1hcCgoZGF0YTogVCkgPT4gdGhpcy5tb2RlbC5mYWN0b3J5KGRhdGEpKTtcbiAgfVxuICAvKipcbiAgICogQG1ldGhvZCBjcmVhdGVDaGFuZ2VTdHJlYW1cbiAgICogQGF1dGhvciBKb25hdGhhbiBDYXNhcnJ1YmlhcyA8dDogam9obmNhc2FycnViaWFzLCBnaDogbWVhbi1leHBlcnQtb2ZmaWNpYWw+XG4gICAqIEBsaWNlbnNlIE1JVFxuICAgKiBAcmV0dXJuIHtPYnNlcnZhYmxlPGFueT59XG4gICAqIEBkZXNjcmlwdGlvblxuICAgKiBHZW5lcmljIGNyZWF0ZUNoYW5nZVN0cmVhbSBtZXRob2RcbiAgICovXG4gIHB1YmxpYyBjcmVhdGVDaGFuZ2VTdHJlYW0oKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICBsZXQgc3ViamVjdCA9IG5ldyBTdWJqZWN0KCk7XG4gICAgaWYgKHR5cGVvZiBFdmVudFNvdXJjZSAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgIGxldCBlbWl0ICAgPSAobXNnOiBhbnkpID0+IHN1YmplY3QubmV4dChKU09OLnBhcnNlKG1zZy5kYXRhKSk7XG4gICAgICB2YXIgc291cmNlID0gbmV3IEV2ZW50U291cmNlKFtcbiAgICAgICAgTG9vcEJhY2tDb25maWcuZ2V0UGF0aCgpLFxuICAgICAgICBMb29wQmFja0NvbmZpZy5nZXRBcGlWZXJzaW9uKCksXG4gICAgICAgIHRoaXMubW9kZWwuZ2V0TW9kZWxEZWZpbml0aW9uKCkucGx1cmFsLFxuICAgICAgICAnY2hhbmdlLXN0cmVhbSdcbiAgICAgIF0uam9pbignLycpKTtcbiAgICAgIHNvdXJjZS5hZGRFdmVudExpc3RlbmVyKCdkYXRhJywgZW1pdCk7XG4gICAgICBzb3VyY2Uub25lcnJvciA9IGVtaXQ7XG4gICAgfSBlbHNlIHtcbiAgICAgIGNvbnNvbGUud2FybignU0RLIEJ1aWxkZXI6IEV2ZW50U291cmNlIGlzIG5vdCBzdXBwb3J0ZWQnKTsgXG4gICAgfVxuICAgIHJldHVybiBzdWJqZWN0LmFzT2JzZXJ2YWJsZSgpO1xuICB9XG4gIC8qKlxuICAgKiBAbWV0aG9kIGdldE1vZGVsTmFtZVxuICAgKiBAYXV0aG9yIEpvbmF0aGFuIENhc2FycnViaWFzIDx0OiBqb2huY2FzYXJydWJpYXMsIGdoOiBtZWFuLWV4cGVydC1vZmZpY2lhbD5cbiAgICogQGxpY2Vuc2UgTUlUXG4gICAqIEByZXR1cm4ge3N0cmluZ31cbiAgICogQGRlc2NyaXB0aW9uXG4gICAqIEFic3RyYWN0IGdldE1vZGVsTmFtZSBtZXRob2RcbiAgICovXG4gIGFic3RyYWN0IGdldE1vZGVsTmFtZSgpOiBzdHJpbmc7XG59XG4iXX0=