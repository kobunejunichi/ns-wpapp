"use strict";
var core_1 = require("@angular/core");
var storage_swaps_1 = require("../../storage/storage.swaps");
var BaseModels_1 = require("../../models/BaseModels");
/**
* @author Jonathan Casarrubias <twitter:@johncasarrubias> <github:@mean-expert-official>
* @module SocketConnection
* @license MIT
* @description
* This module handle socket connections and return singleton instances for each
* connection, it will use the SDK Socket Driver Available currently supporting
* Angular 2 for web, NativeScript 2 and Angular Universal.
**/
var LoopBackAuth = (function () {
    /**
     * @method constructor
     * @param {InternalStorage} storage Internal Storage Driver
     * @description
     * The constructor will initialize the token loading data from storage
     **/
    function LoopBackAuth(storage) {
        this.storage = storage;
        /**
         * @type {SDKToken}
         **/
        this.token = new BaseModels_1.SDKToken();
        /**
         * @type {string}
         **/
        this.prefix = '$LoopBackSDK$';
        this.token.id = this.load('id');
        this.token.user = this.load('user');
        this.token.userId = this.load('userId');
        this.token.issuedAt = this.load('issuedAt');
        this.token.created = this.load('created');
        this.token.ttl = this.load('ttl');
        this.token.rememberMe = this.load('rememberMe');
    }
    /**
     * @method setRememberMe
     * @param {boolean} value Flag to remember credentials
     * @return {void}
     * @description
     * This method will set a flag in order to remember the current credentials
     **/
    LoopBackAuth.prototype.setRememberMe = function (value) {
        this.token.rememberMe = value;
    };
    /**
     * @method setUser
     * @param {any} user Any type of user model
     * @return {void}
     * @description
     * This method will update the user information and persist it if the
     * rememberMe flag is set.
     **/
    LoopBackAuth.prototype.setUser = function (user) {
        this.token.user = user;
        this.save();
    };
    /**
     * @method setToken
     * @param {SDKToken} token SDKToken or casted AccessToken instance
     * @return {void}
     * @description
     * This method will set a flag in order to remember the current credentials
     **/
    LoopBackAuth.prototype.setToken = function (token) {
        this.token = Object.assign(this.token, token);
        this.save();
    };
    /**
     * @method getToken
     * @return {void}
     * @description
     * This method will set a flag in order to remember the current credentials.
     **/
    LoopBackAuth.prototype.getToken = function () {
        return this.token;
    };
    /**
     * @method getAccessTokenId
     * @return {string}
     * @description
     * This method will return the actual token string, not the object instance.
     **/
    LoopBackAuth.prototype.getAccessTokenId = function () {
        return this.token.id;
    };
    /**
     * @method getCurrentUserId
     * @return {any}
     * @description
     * This method will return the current user id, it can be number or string.
     **/
    LoopBackAuth.prototype.getCurrentUserId = function () {
        return this.token.userId;
    };
    /**
     * @method getCurrentUserData
     * @return {any}
     * @description
     * This method will return the current user instance.
     **/
    LoopBackAuth.prototype.getCurrentUserData = function () {
        return (typeof this.token.user === 'string') ? JSON.parse(this.token.user) : this.token.user;
    };
    /**
     * @method save
     * @return {boolean} Wether or not the information was saved
     * @description
     * This method will save in either local storage or cookies the current credentials.
     * But only if rememberMe is enabled.
     **/
    LoopBackAuth.prototype.save = function () {
        if (this.token.rememberMe) {
            this.persist('id', this.token.id);
            this.persist('user', this.token.user);
            this.persist('userId', this.token.userId);
            this.persist('issuedAt', this.token.issuedAt);
            this.persist('created', this.token.created);
            this.persist('ttl', this.token.ttl);
            this.persist('rememberMe', this.token.rememberMe);
            return true;
        }
        else {
            return false;
        }
    };
    ;
    /**
     * @method load
     * @param {string} prop Property name
     * @return {any} Any information persisted in storage
     * @description
     * This method will load either from local storage or cookies the provided property.
     **/
    LoopBackAuth.prototype.load = function (prop) {
        return this.storage.get("" + this.prefix + prop);
    };
    /**
     * @method clear
     * @return {void}
     * @description
     * This method will clear cookies or the local storage.
     **/
    LoopBackAuth.prototype.clear = function () {
        var _this = this;
        Object.keys(this.token).forEach(function (prop) { return _this.storage.remove("" + _this.prefix + prop); });
        this.token = new BaseModels_1.SDKToken();
    };
    /**
     * @method clear
     * @return {void}
     * @description
     * This method will clear cookies or the local storage.
     **/
    LoopBackAuth.prototype.persist = function (prop, value) {
        try {
            this.storage.set("" + this.prefix + prop, (typeof value === 'object') ? JSON.stringify(value) : value);
        }
        catch (err) {
            console.error('Cannot access local/session storage:', err);
        }
    };
    return LoopBackAuth;
}());
LoopBackAuth = __decorate([
    core_1.Injectable(),
    __param(0, core_1.Inject(storage_swaps_1.InternalStorage)),
    __metadata("design:paramtypes", [storage_swaps_1.InternalStorage])
], LoopBackAuth);
exports.LoopBackAuth = LoopBackAuth;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXV0aC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFFQSxzQ0FBbUQ7QUFDbkQsNkRBQThEO0FBQzlELHNEQUFtRDtBQUNuRDs7Ozs7Ozs7R0FRRztBQUVILElBQWEsWUFBWTtJQVN2Qjs7Ozs7UUFLSTtJQUNKLHNCQUErQyxPQUF3QjtRQUF4QixZQUFPLEdBQVAsT0FBTyxDQUFpQjtRQWR2RTs7WUFFSTtRQUNJLFVBQUssR0FBYSxJQUFJLHFCQUFRLEVBQUUsQ0FBQztRQUN6Qzs7WUFFSTtRQUNNLFdBQU0sR0FBVyxlQUFlLENBQUM7UUFRekMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLEdBQVcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN4QyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksR0FBUyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDNUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUssSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBTSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzdDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFVLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDekMsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUNsRCxDQUFDO0lBQ0Q7Ozs7OztRQU1JO0lBQ0csb0NBQWEsR0FBcEIsVUFBcUIsS0FBYztRQUNqQyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7SUFDaEMsQ0FBQztJQUNEOzs7Ozs7O1FBT0k7SUFDRyw4QkFBTyxHQUFkLFVBQWUsSUFBUztRQUN0QixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFDdkIsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ2QsQ0FBQztJQUNEOzs7Ozs7UUFNSTtJQUNHLCtCQUFRLEdBQWYsVUFBZ0IsS0FBZTtRQUM3QixJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDZCxDQUFDO0lBQ0Q7Ozs7O1FBS0k7SUFDRywrQkFBUSxHQUFmO1FBQ0UsTUFBTSxDQUFZLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDL0IsQ0FBQztJQUNEOzs7OztRQUtJO0lBQ0csdUNBQWdCLEdBQXZCO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDO0lBQ3ZCLENBQUM7SUFDRDs7Ozs7UUFLSTtJQUNHLHVDQUFnQixHQUF2QjtRQUNFLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztJQUMzQixDQUFDO0lBQ0Q7Ozs7O1FBS0k7SUFDRyx5Q0FBa0IsR0FBekI7UUFDRSxNQUFNLENBQUMsQ0FBQyxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLFFBQVEsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztJQUMvRixDQUFDO0lBQ0Q7Ozs7OztRQU1JO0lBQ0csMkJBQUksR0FBWDtRQUNFLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztZQUMxQixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ2xDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDdEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUMxQyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzlDLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDNUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNwQyxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ2xELE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDZCxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixNQUFNLENBQUMsS0FBSyxDQUFDO1FBQ2YsQ0FBQztJQUNILENBQUM7SUFBQSxDQUFDO0lBQ0Y7Ozs7OztRQU1JO0lBQ00sMkJBQUksR0FBZCxVQUFlLElBQVk7UUFDekIsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFNLENBQUMsQ0FBQztJQUNuRCxDQUFDO0lBQ0Q7Ozs7O1FBS0k7SUFDRyw0QkFBSyxHQUFaO1FBQUEsaUJBR0M7UUFGQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFZLElBQUssT0FBQSxLQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFHLEtBQUksQ0FBQyxNQUFNLEdBQUcsSUFBTSxDQUFDLEVBQTVDLENBQTRDLENBQUMsQ0FBQztRQUNoRyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUkscUJBQVEsRUFBRSxDQUFDO0lBQzlCLENBQUM7SUFDRDs7Ozs7UUFLSTtJQUNNLDhCQUFPLEdBQWpCLFVBQWtCLElBQVksRUFBRSxLQUFVO1FBQ3hDLElBQUksQ0FBQztZQUNILElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUNkLEtBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFNLEVBQ3ZCLENBQUMsT0FBTyxLQUFLLEtBQUssUUFBUSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxLQUFLLENBQzVELENBQUM7UUFDSixDQUFDO1FBQ0QsS0FBSyxDQUFBLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNWLE9BQU8sQ0FBQyxLQUFLLENBQUMsc0NBQXNDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDN0QsQ0FBQztJQUNILENBQUM7SUFDSCxtQkFBQztBQUFELENBQUMsQUF2SkQsSUF1SkM7QUF2SlksWUFBWTtJQUR4QixpQkFBVSxFQUFFO0lBZ0JFLFdBQUEsYUFBTSxDQUFDLCtCQUFlLENBQUMsQ0FBQTtxQ0FBb0IsK0JBQWU7R0FmNUQsWUFBWSxDQXVKeEI7QUF2Slksb0NBQVkiLCJzb3VyY2VzQ29udGVudCI6WyIvKiB0c2xpbnQ6ZGlzYWJsZSAqL1xuZGVjbGFyZSB2YXIgT2JqZWN0OiBhbnk7XG5pbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3QgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEludGVybmFsU3RvcmFnZSB9IGZyb20gJy4uLy4uL3N0b3JhZ2Uvc3RvcmFnZS5zd2Fwcyc7XG5pbXBvcnQgeyBTREtUb2tlbiB9IGZyb20gJy4uLy4uL21vZGVscy9CYXNlTW9kZWxzJztcbi8qKlxuKiBAYXV0aG9yIEpvbmF0aGFuIENhc2FycnViaWFzIDx0d2l0dGVyOkBqb2huY2FzYXJydWJpYXM+IDxnaXRodWI6QG1lYW4tZXhwZXJ0LW9mZmljaWFsPlxuKiBAbW9kdWxlIFNvY2tldENvbm5lY3Rpb25cbiogQGxpY2Vuc2UgTUlUXG4qIEBkZXNjcmlwdGlvblxuKiBUaGlzIG1vZHVsZSBoYW5kbGUgc29ja2V0IGNvbm5lY3Rpb25zIGFuZCByZXR1cm4gc2luZ2xldG9uIGluc3RhbmNlcyBmb3IgZWFjaFxuKiBjb25uZWN0aW9uLCBpdCB3aWxsIHVzZSB0aGUgU0RLIFNvY2tldCBEcml2ZXIgQXZhaWxhYmxlIGN1cnJlbnRseSBzdXBwb3J0aW5nXG4qIEFuZ3VsYXIgMiBmb3Igd2ViLCBOYXRpdmVTY3JpcHQgMiBhbmQgQW5ndWxhciBVbml2ZXJzYWwuXG4qKi9cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBMb29wQmFja0F1dGgge1xuICAvKipcbiAgICogQHR5cGUge1NES1Rva2VufVxuICAgKiovXG4gIHByaXZhdGUgdG9rZW46IFNES1Rva2VuID0gbmV3IFNES1Rva2VuKCk7XG4gIC8qKlxuICAgKiBAdHlwZSB7c3RyaW5nfVxuICAgKiovXG4gIHByb3RlY3RlZCBwcmVmaXg6IHN0cmluZyA9ICckTG9vcEJhY2tTREskJztcbiAgLyoqXG4gICAqIEBtZXRob2QgY29uc3RydWN0b3JcbiAgICogQHBhcmFtIHtJbnRlcm5hbFN0b3JhZ2V9IHN0b3JhZ2UgSW50ZXJuYWwgU3RvcmFnZSBEcml2ZXJcbiAgICogQGRlc2NyaXB0aW9uXG4gICAqIFRoZSBjb25zdHJ1Y3RvciB3aWxsIGluaXRpYWxpemUgdGhlIHRva2VuIGxvYWRpbmcgZGF0YSBmcm9tIHN0b3JhZ2VcbiAgICoqL1xuICBjb25zdHJ1Y3RvcihASW5qZWN0KEludGVybmFsU3RvcmFnZSkgcHJvdGVjdGVkIHN0b3JhZ2U6IEludGVybmFsU3RvcmFnZSkge1xuICAgIHRoaXMudG9rZW4uaWQgICAgICAgICA9IHRoaXMubG9hZCgnaWQnKTtcbiAgICB0aGlzLnRva2VuLnVzZXIgICAgICAgPSB0aGlzLmxvYWQoJ3VzZXInKTtcbiAgICB0aGlzLnRva2VuLnVzZXJJZCAgICAgPSB0aGlzLmxvYWQoJ3VzZXJJZCcpO1xuICAgIHRoaXMudG9rZW4uaXNzdWVkQXQgICA9IHRoaXMubG9hZCgnaXNzdWVkQXQnKTtcbiAgICB0aGlzLnRva2VuLmNyZWF0ZWQgICAgPSB0aGlzLmxvYWQoJ2NyZWF0ZWQnKTtcbiAgICB0aGlzLnRva2VuLnR0bCAgICAgICAgPSB0aGlzLmxvYWQoJ3R0bCcpO1xuICAgIHRoaXMudG9rZW4ucmVtZW1iZXJNZSA9IHRoaXMubG9hZCgncmVtZW1iZXJNZScpO1xuICB9XG4gIC8qKlxuICAgKiBAbWV0aG9kIHNldFJlbWVtYmVyTWVcbiAgICogQHBhcmFtIHtib29sZWFufSB2YWx1ZSBGbGFnIHRvIHJlbWVtYmVyIGNyZWRlbnRpYWxzXG4gICAqIEByZXR1cm4ge3ZvaWR9XG4gICAqIEBkZXNjcmlwdGlvblxuICAgKiBUaGlzIG1ldGhvZCB3aWxsIHNldCBhIGZsYWcgaW4gb3JkZXIgdG8gcmVtZW1iZXIgdGhlIGN1cnJlbnQgY3JlZGVudGlhbHNcbiAgICoqL1xuICBwdWJsaWMgc2V0UmVtZW1iZXJNZSh2YWx1ZTogYm9vbGVhbik6IHZvaWQge1xuICAgIHRoaXMudG9rZW4ucmVtZW1iZXJNZSA9IHZhbHVlO1xuICB9XG4gIC8qKlxuICAgKiBAbWV0aG9kIHNldFVzZXJcbiAgICogQHBhcmFtIHthbnl9IHVzZXIgQW55IHR5cGUgb2YgdXNlciBtb2RlbFxuICAgKiBAcmV0dXJuIHt2b2lkfVxuICAgKiBAZGVzY3JpcHRpb25cbiAgICogVGhpcyBtZXRob2Qgd2lsbCB1cGRhdGUgdGhlIHVzZXIgaW5mb3JtYXRpb24gYW5kIHBlcnNpc3QgaXQgaWYgdGhlXG4gICAqIHJlbWVtYmVyTWUgZmxhZyBpcyBzZXQuXG4gICAqKi9cbiAgcHVibGljIHNldFVzZXIodXNlcjogYW55KSB7XG4gICAgdGhpcy50b2tlbi51c2VyID0gdXNlcjtcbiAgICB0aGlzLnNhdmUoKTtcbiAgfVxuICAvKipcbiAgICogQG1ldGhvZCBzZXRUb2tlblxuICAgKiBAcGFyYW0ge1NES1Rva2VufSB0b2tlbiBTREtUb2tlbiBvciBjYXN0ZWQgQWNjZXNzVG9rZW4gaW5zdGFuY2UgXG4gICAqIEByZXR1cm4ge3ZvaWR9XG4gICAqIEBkZXNjcmlwdGlvblxuICAgKiBUaGlzIG1ldGhvZCB3aWxsIHNldCBhIGZsYWcgaW4gb3JkZXIgdG8gcmVtZW1iZXIgdGhlIGN1cnJlbnQgY3JlZGVudGlhbHNcbiAgICoqL1xuICBwdWJsaWMgc2V0VG9rZW4odG9rZW46IFNES1Rva2VuKTogdm9pZCB7XG4gICAgdGhpcy50b2tlbiA9IE9iamVjdC5hc3NpZ24odGhpcy50b2tlbiwgdG9rZW4pO1xuICAgIHRoaXMuc2F2ZSgpO1xuICB9XG4gIC8qKlxuICAgKiBAbWV0aG9kIGdldFRva2VuXG4gICAqIEByZXR1cm4ge3ZvaWR9XG4gICAqIEBkZXNjcmlwdGlvblxuICAgKiBUaGlzIG1ldGhvZCB3aWxsIHNldCBhIGZsYWcgaW4gb3JkZXIgdG8gcmVtZW1iZXIgdGhlIGN1cnJlbnQgY3JlZGVudGlhbHMuXG4gICAqKi9cbiAgcHVibGljIGdldFRva2VuKCk6IFNES1Rva2VuIHtcbiAgICByZXR1cm4gPFNES1Rva2VuPiB0aGlzLnRva2VuO1xuICB9XG4gIC8qKlxuICAgKiBAbWV0aG9kIGdldEFjY2Vzc1Rva2VuSWRcbiAgICogQHJldHVybiB7c3RyaW5nfVxuICAgKiBAZGVzY3JpcHRpb25cbiAgICogVGhpcyBtZXRob2Qgd2lsbCByZXR1cm4gdGhlIGFjdHVhbCB0b2tlbiBzdHJpbmcsIG5vdCB0aGUgb2JqZWN0IGluc3RhbmNlLlxuICAgKiovXG4gIHB1YmxpYyBnZXRBY2Nlc3NUb2tlbklkKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMudG9rZW4uaWQ7XG4gIH1cbiAgLyoqXG4gICAqIEBtZXRob2QgZ2V0Q3VycmVudFVzZXJJZFxuICAgKiBAcmV0dXJuIHthbnl9XG4gICAqIEBkZXNjcmlwdGlvblxuICAgKiBUaGlzIG1ldGhvZCB3aWxsIHJldHVybiB0aGUgY3VycmVudCB1c2VyIGlkLCBpdCBjYW4gYmUgbnVtYmVyIG9yIHN0cmluZy5cbiAgICoqL1xuICBwdWJsaWMgZ2V0Q3VycmVudFVzZXJJZCgpOiBhbnkge1xuICAgIHJldHVybiB0aGlzLnRva2VuLnVzZXJJZDtcbiAgfVxuICAvKipcbiAgICogQG1ldGhvZCBnZXRDdXJyZW50VXNlckRhdGFcbiAgICogQHJldHVybiB7YW55fVxuICAgKiBAZGVzY3JpcHRpb25cbiAgICogVGhpcyBtZXRob2Qgd2lsbCByZXR1cm4gdGhlIGN1cnJlbnQgdXNlciBpbnN0YW5jZS5cbiAgICoqL1xuICBwdWJsaWMgZ2V0Q3VycmVudFVzZXJEYXRhKCk6IGFueSB7XG4gICAgcmV0dXJuICh0eXBlb2YgdGhpcy50b2tlbi51c2VyID09PSAnc3RyaW5nJykgPyBKU09OLnBhcnNlKHRoaXMudG9rZW4udXNlcikgOiB0aGlzLnRva2VuLnVzZXI7XG4gIH1cbiAgLyoqXG4gICAqIEBtZXRob2Qgc2F2ZVxuICAgKiBAcmV0dXJuIHtib29sZWFufSBXZXRoZXIgb3Igbm90IHRoZSBpbmZvcm1hdGlvbiB3YXMgc2F2ZWRcbiAgICogQGRlc2NyaXB0aW9uXG4gICAqIFRoaXMgbWV0aG9kIHdpbGwgc2F2ZSBpbiBlaXRoZXIgbG9jYWwgc3RvcmFnZSBvciBjb29raWVzIHRoZSBjdXJyZW50IGNyZWRlbnRpYWxzLlxuICAgKiBCdXQgb25seSBpZiByZW1lbWJlck1lIGlzIGVuYWJsZWQuXG4gICAqKi9cbiAgcHVibGljIHNhdmUoKTogYm9vbGVhbiB7XG4gICAgaWYgKHRoaXMudG9rZW4ucmVtZW1iZXJNZSkge1xuICAgICAgdGhpcy5wZXJzaXN0KCdpZCcsIHRoaXMudG9rZW4uaWQpO1xuICAgICAgdGhpcy5wZXJzaXN0KCd1c2VyJywgdGhpcy50b2tlbi51c2VyKTtcbiAgICAgIHRoaXMucGVyc2lzdCgndXNlcklkJywgdGhpcy50b2tlbi51c2VySWQpO1xuICAgICAgdGhpcy5wZXJzaXN0KCdpc3N1ZWRBdCcsIHRoaXMudG9rZW4uaXNzdWVkQXQpO1xuICAgICAgdGhpcy5wZXJzaXN0KCdjcmVhdGVkJywgdGhpcy50b2tlbi5jcmVhdGVkKTtcbiAgICAgIHRoaXMucGVyc2lzdCgndHRsJywgdGhpcy50b2tlbi50dGwpO1xuICAgICAgdGhpcy5wZXJzaXN0KCdyZW1lbWJlck1lJywgdGhpcy50b2tlbi5yZW1lbWJlck1lKTtcbiAgICAgIHJldHVybiB0cnVlO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfSAgICBcbiAgfTtcbiAgLyoqXG4gICAqIEBtZXRob2QgbG9hZFxuICAgKiBAcGFyYW0ge3N0cmluZ30gcHJvcCBQcm9wZXJ0eSBuYW1lXG4gICAqIEByZXR1cm4ge2FueX0gQW55IGluZm9ybWF0aW9uIHBlcnNpc3RlZCBpbiBzdG9yYWdlXG4gICAqIEBkZXNjcmlwdGlvblxuICAgKiBUaGlzIG1ldGhvZCB3aWxsIGxvYWQgZWl0aGVyIGZyb20gbG9jYWwgc3RvcmFnZSBvciBjb29raWVzIHRoZSBwcm92aWRlZCBwcm9wZXJ0eS5cbiAgICoqL1xuICBwcm90ZWN0ZWQgbG9hZChwcm9wOiBzdHJpbmcpOiBhbnkge1xuICAgIHJldHVybiB0aGlzLnN0b3JhZ2UuZ2V0KGAke3RoaXMucHJlZml4fSR7cHJvcH1gKTtcbiAgfVxuICAvKipcbiAgICogQG1ldGhvZCBjbGVhclxuICAgKiBAcmV0dXJuIHt2b2lkfVxuICAgKiBAZGVzY3JpcHRpb25cbiAgICogVGhpcyBtZXRob2Qgd2lsbCBjbGVhciBjb29raWVzIG9yIHRoZSBsb2NhbCBzdG9yYWdlLlxuICAgKiovXG4gIHB1YmxpYyBjbGVhcigpOiB2b2lkIHtcbiAgICBPYmplY3Qua2V5cyh0aGlzLnRva2VuKS5mb3JFYWNoKChwcm9wOiBzdHJpbmcpID0+IHRoaXMuc3RvcmFnZS5yZW1vdmUoYCR7dGhpcy5wcmVmaXh9JHtwcm9wfWApKTtcbiAgICB0aGlzLnRva2VuID0gbmV3IFNES1Rva2VuKCk7XG4gIH1cbiAgLyoqXG4gICAqIEBtZXRob2QgY2xlYXJcbiAgICogQHJldHVybiB7dm9pZH1cbiAgICogQGRlc2NyaXB0aW9uXG4gICAqIFRoaXMgbWV0aG9kIHdpbGwgY2xlYXIgY29va2llcyBvciB0aGUgbG9jYWwgc3RvcmFnZS5cbiAgICoqL1xuICBwcm90ZWN0ZWQgcGVyc2lzdChwcm9wOiBzdHJpbmcsIHZhbHVlOiBhbnkpOiB2b2lkIHtcbiAgICB0cnkge1xuICAgICAgdGhpcy5zdG9yYWdlLnNldChcbiAgICAgICAgYCR7dGhpcy5wcmVmaXh9JHtwcm9wfWAsXG4gICAgICAgICh0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnKSA/IEpTT04uc3RyaW5naWZ5KHZhbHVlKSA6IHZhbHVlXG4gICAgICApO1xuICAgIH1cbiAgICBjYXRjaChlcnIpIHtcbiAgICAgIGNvbnNvbGUuZXJyb3IoJ0Nhbm5vdCBhY2Nlc3MgbG9jYWwvc2Vzc2lvbiBzdG9yYWdlOicsIGVycik7XG4gICAgfVxuICB9XG59XG4iXX0=