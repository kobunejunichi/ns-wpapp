/* tslint:disable */
import { Injectable } from '@angular/core';
import { AppUser } from '../../models/AppUser';
import { Group } from '../../models/Group';
import { Article } from '../../models/Article';
import { Staff } from '../../models/Staff';
import { Invitation } from '../../models/Invitation';
import { Information } from '../../models/Information';
import { Design } from '../../models/Design';
import { Activity } from '../../models/Activity';
import { Util } from '../../models/Util';
import { AppVersion } from '../../models/AppVersion';

export interface Models { [name: string]: any }

@Injectable()
export class SDKModels {

  private models: Models = {
    AppUser: AppUser,
    Group: Group,
    Article: Article,
    Staff: Staff,
    Invitation: Invitation,
    Information: Information,
    Design: Design,
    Activity: Activity,
    Util: Util,
    AppVersion: AppVersion,
    
  };

  public get(modelName: string): any {
    return this.models[modelName];
  }

  public getAll(): Models {
    return this.models;
  }

  public getModelNames(): string[] {
    return Object.keys(this.models);
  }
}
