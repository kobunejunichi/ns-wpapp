"use strict";
/* tslint:disable */
var core_1 = require("@angular/core");
var AppUser_1 = require("../../models/AppUser");
var Group_1 = require("../../models/Group");
var Article_1 = require("../../models/Article");
var Staff_1 = require("../../models/Staff");
var Invitation_1 = require("../../models/Invitation");
var Information_1 = require("../../models/Information");
var Design_1 = require("../../models/Design");
var Activity_1 = require("../../models/Activity");
var Util_1 = require("../../models/Util");
var AppVersion_1 = require("../../models/AppVersion");
var SDKModels = (function () {
    function SDKModels() {
        this.models = {
            AppUser: AppUser_1.AppUser,
            Group: Group_1.Group,
            Article: Article_1.Article,
            Staff: Staff_1.Staff,
            Invitation: Invitation_1.Invitation,
            Information: Information_1.Information,
            Design: Design_1.Design,
            Activity: Activity_1.Activity,
            Util: Util_1.Util,
            AppVersion: AppVersion_1.AppVersion,
        };
    }
    SDKModels.prototype.get = function (modelName) {
        return this.models[modelName];
    };
    SDKModels.prototype.getAll = function () {
        return this.models;
    };
    SDKModels.prototype.getModelNames = function () {
        return Object.keys(this.models);
    };
    return SDKModels;
}());
SDKModels = __decorate([
    core_1.Injectable()
], SDKModels);
exports.SDKModels = SDKModels;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU0RLTW9kZWxzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiU0RLTW9kZWxzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxvQkFBb0I7QUFDcEIsc0NBQTJDO0FBQzNDLGdEQUErQztBQUMvQyw0Q0FBMkM7QUFDM0MsZ0RBQStDO0FBQy9DLDRDQUEyQztBQUMzQyxzREFBcUQ7QUFDckQsd0RBQXVEO0FBQ3ZELDhDQUE2QztBQUM3QyxrREFBaUQ7QUFDakQsMENBQXlDO0FBQ3pDLHNEQUFxRDtBQUtyRCxJQUFhLFNBQVM7SUFEdEI7UUFHVSxXQUFNLEdBQVc7WUFDdkIsT0FBTyxFQUFFLGlCQUFPO1lBQ2hCLEtBQUssRUFBRSxhQUFLO1lBQ1osT0FBTyxFQUFFLGlCQUFPO1lBQ2hCLEtBQUssRUFBRSxhQUFLO1lBQ1osVUFBVSxFQUFFLHVCQUFVO1lBQ3RCLFdBQVcsRUFBRSx5QkFBVztZQUN4QixNQUFNLEVBQUUsZUFBTTtZQUNkLFFBQVEsRUFBRSxtQkFBUTtZQUNsQixJQUFJLEVBQUUsV0FBSTtZQUNWLFVBQVUsRUFBRSx1QkFBVTtTQUV2QixDQUFDO0lBYUosQ0FBQztJQVhRLHVCQUFHLEdBQVYsVUFBVyxTQUFpQjtRQUMxQixNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUNoQyxDQUFDO0lBRU0sMEJBQU0sR0FBYjtRQUNFLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQ3JCLENBQUM7SUFFTSxpQ0FBYSxHQUFwQjtRQUNFLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNsQyxDQUFDO0lBQ0gsZ0JBQUM7QUFBRCxDQUFDLEFBM0JELElBMkJDO0FBM0JZLFNBQVM7SUFEckIsaUJBQVUsRUFBRTtHQUNBLFNBQVMsQ0EyQnJCO0FBM0JZLDhCQUFTIiwic291cmNlc0NvbnRlbnQiOlsiLyogdHNsaW50OmRpc2FibGUgKi9cbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEFwcFVzZXIgfSBmcm9tICcuLi8uLi9tb2RlbHMvQXBwVXNlcic7XG5pbXBvcnQgeyBHcm91cCB9IGZyb20gJy4uLy4uL21vZGVscy9Hcm91cCc7XG5pbXBvcnQgeyBBcnRpY2xlIH0gZnJvbSAnLi4vLi4vbW9kZWxzL0FydGljbGUnO1xuaW1wb3J0IHsgU3RhZmYgfSBmcm9tICcuLi8uLi9tb2RlbHMvU3RhZmYnO1xuaW1wb3J0IHsgSW52aXRhdGlvbiB9IGZyb20gJy4uLy4uL21vZGVscy9JbnZpdGF0aW9uJztcbmltcG9ydCB7IEluZm9ybWF0aW9uIH0gZnJvbSAnLi4vLi4vbW9kZWxzL0luZm9ybWF0aW9uJztcbmltcG9ydCB7IERlc2lnbiB9IGZyb20gJy4uLy4uL21vZGVscy9EZXNpZ24nO1xuaW1wb3J0IHsgQWN0aXZpdHkgfSBmcm9tICcuLi8uLi9tb2RlbHMvQWN0aXZpdHknO1xuaW1wb3J0IHsgVXRpbCB9IGZyb20gJy4uLy4uL21vZGVscy9VdGlsJztcbmltcG9ydCB7IEFwcFZlcnNpb24gfSBmcm9tICcuLi8uLi9tb2RlbHMvQXBwVmVyc2lvbic7XG5cbmV4cG9ydCBpbnRlcmZhY2UgTW9kZWxzIHsgW25hbWU6IHN0cmluZ106IGFueSB9XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBTREtNb2RlbHMge1xuXG4gIHByaXZhdGUgbW9kZWxzOiBNb2RlbHMgPSB7XG4gICAgQXBwVXNlcjogQXBwVXNlcixcbiAgICBHcm91cDogR3JvdXAsXG4gICAgQXJ0aWNsZTogQXJ0aWNsZSxcbiAgICBTdGFmZjogU3RhZmYsXG4gICAgSW52aXRhdGlvbjogSW52aXRhdGlvbixcbiAgICBJbmZvcm1hdGlvbjogSW5mb3JtYXRpb24sXG4gICAgRGVzaWduOiBEZXNpZ24sXG4gICAgQWN0aXZpdHk6IEFjdGl2aXR5LFxuICAgIFV0aWw6IFV0aWwsXG4gICAgQXBwVmVyc2lvbjogQXBwVmVyc2lvbixcbiAgICBcbiAgfTtcblxuICBwdWJsaWMgZ2V0KG1vZGVsTmFtZTogc3RyaW5nKTogYW55IHtcbiAgICByZXR1cm4gdGhpcy5tb2RlbHNbbW9kZWxOYW1lXTtcbiAgfVxuXG4gIHB1YmxpYyBnZXRBbGwoKTogTW9kZWxzIHtcbiAgICByZXR1cm4gdGhpcy5tb2RlbHM7XG4gIH1cblxuICBwdWJsaWMgZ2V0TW9kZWxOYW1lcygpOiBzdHJpbmdbXSB7XG4gICAgcmV0dXJuIE9iamVjdC5rZXlzKHRoaXMubW9kZWxzKTtcbiAgfVxufVxuIl19