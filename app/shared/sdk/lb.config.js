"use strict";
/* tslint:disable */
/**
* @module LoopBackConfig
* @description
*
* The LoopBackConfig module help developers to externally
* configure the base url and api version for loopback.io
*
* Example
*
* import { LoopBackConfig } from './sdk';
*
* @Component() // No metadata needed for this module
*
* export class MyApp {
*   constructor() {
*     LoopBackConfig.setBaseURL('http://localhost:3000');
*     LoopBackConfig.setApiVersion('api');
*   }
* }
**/
var LoopBackConfig = (function () {
    function LoopBackConfig() {
    }
    LoopBackConfig.setApiVersion = function (version) {
        if (version === void 0) { version = 'api'; }
        LoopBackConfig.version = version;
    };
    LoopBackConfig.getApiVersion = function () {
        return LoopBackConfig.version;
    };
    LoopBackConfig.setBaseURL = function (url) {
        if (url === void 0) { url = '/'; }
        LoopBackConfig.path = url;
    };
    LoopBackConfig.getPath = function () {
        return LoopBackConfig.path;
    };
    LoopBackConfig.setAuthPrefix = function (authPrefix) {
        if (authPrefix === void 0) { authPrefix = ''; }
        LoopBackConfig.authPrefix = authPrefix;
    };
    LoopBackConfig.getAuthPrefix = function () {
        return LoopBackConfig.authPrefix;
    };
    LoopBackConfig.setDebugMode = function (isEnabled) {
        LoopBackConfig.debug = isEnabled;
    };
    LoopBackConfig.debuggable = function () {
        return LoopBackConfig.debug;
    };
    return LoopBackConfig;
}());
LoopBackConfig.path = '//0.0.0.0:3000';
LoopBackConfig.version = 'v1';
LoopBackConfig.authPrefix = '';
LoopBackConfig.debug = true;
exports.LoopBackConfig = LoopBackConfig;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGIuY29uZmlnLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibGIuY29uZmlnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxvQkFBb0I7QUFDcEI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0FtQkc7QUFDSDtJQUFBO0lBcUNBLENBQUM7SUEvQmUsNEJBQWEsR0FBM0IsVUFBNEIsT0FBdUI7UUFBdkIsd0JBQUEsRUFBQSxlQUF1QjtRQUNqRCxjQUFjLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztJQUNuQyxDQUFDO0lBRWEsNEJBQWEsR0FBM0I7UUFDRSxNQUFNLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQztJQUNoQyxDQUFDO0lBRWEseUJBQVUsR0FBeEIsVUFBeUIsR0FBaUI7UUFBakIsb0JBQUEsRUFBQSxTQUFpQjtRQUN4QyxjQUFjLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQztJQUM1QixDQUFDO0lBRWEsc0JBQU8sR0FBckI7UUFDRSxNQUFNLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQztJQUM3QixDQUFDO0lBRWEsNEJBQWEsR0FBM0IsVUFBNEIsVUFBdUI7UUFBdkIsMkJBQUEsRUFBQSxlQUF1QjtRQUNqRCxjQUFjLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztJQUN6QyxDQUFDO0lBRWEsNEJBQWEsR0FBM0I7UUFDRSxNQUFNLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQztJQUNuQyxDQUFDO0lBRWEsMkJBQVksR0FBMUIsVUFBMkIsU0FBa0I7UUFDM0MsY0FBYyxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUM7SUFDbkMsQ0FBQztJQUVhLHlCQUFVLEdBQXhCO1FBQ0UsTUFBTSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUM7SUFDOUIsQ0FBQztJQUNILHFCQUFDO0FBQUQsQ0FBQyxBQXJDRDtBQUNpQixtQkFBSSxHQUFXLGdCQUFnQixDQUFDO0FBQ2hDLHNCQUFPLEdBQW9CLElBQUksQ0FBQztBQUNoQyx5QkFBVSxHQUFXLEVBQUUsQ0FBQztBQUN4QixvQkFBSyxHQUFZLElBQUksQ0FBQztBQUoxQix3Q0FBYyIsInNvdXJjZXNDb250ZW50IjpbIi8qIHRzbGludDpkaXNhYmxlICovXG4vKipcbiogQG1vZHVsZSBMb29wQmFja0NvbmZpZ1xuKiBAZGVzY3JpcHRpb25cbipcbiogVGhlIExvb3BCYWNrQ29uZmlnIG1vZHVsZSBoZWxwIGRldmVsb3BlcnMgdG8gZXh0ZXJuYWxseSBcbiogY29uZmlndXJlIHRoZSBiYXNlIHVybCBhbmQgYXBpIHZlcnNpb24gZm9yIGxvb3BiYWNrLmlvXG4qXG4qIEV4YW1wbGVcbipcbiogaW1wb3J0IHsgTG9vcEJhY2tDb25maWcgfSBmcm9tICcuL3Nkayc7XG4qIFxuKiBAQ29tcG9uZW50KCkgLy8gTm8gbWV0YWRhdGEgbmVlZGVkIGZvciB0aGlzIG1vZHVsZVxuKlxuKiBleHBvcnQgY2xhc3MgTXlBcHAge1xuKiAgIGNvbnN0cnVjdG9yKCkge1xuKiAgICAgTG9vcEJhY2tDb25maWcuc2V0QmFzZVVSTCgnaHR0cDovL2xvY2FsaG9zdDozMDAwJyk7XG4qICAgICBMb29wQmFja0NvbmZpZy5zZXRBcGlWZXJzaW9uKCdhcGknKTtcbiogICB9XG4qIH1cbioqL1xuZXhwb3J0IGNsYXNzIExvb3BCYWNrQ29uZmlnIHtcbiAgcHJpdmF0ZSBzdGF0aWMgcGF0aDogc3RyaW5nID0gJy8vMC4wLjAuMDozMDAwJztcbiAgcHJpdmF0ZSBzdGF0aWMgdmVyc2lvbjogc3RyaW5nIHzCoG51bWJlciA9ICd2MSc7XG4gIHByaXZhdGUgc3RhdGljIGF1dGhQcmVmaXg6IHN0cmluZyA9ICcnO1xuICBwcml2YXRlIHN0YXRpYyBkZWJ1ZzogYm9vbGVhbiA9IHRydWU7XG5cbiAgcHVibGljIHN0YXRpYyBzZXRBcGlWZXJzaW9uKHZlcnNpb246IHN0cmluZyA9ICdhcGknKTogdm9pZCB7XG4gICAgTG9vcEJhY2tDb25maWcudmVyc2lvbiA9IHZlcnNpb247XG4gIH1cbiAgXG4gIHB1YmxpYyBzdGF0aWMgZ2V0QXBpVmVyc2lvbigpOiBzdHJpbmcgfCBudW1iZXIge1xuICAgIHJldHVybiBMb29wQmFja0NvbmZpZy52ZXJzaW9uO1xuICB9XG5cbiAgcHVibGljIHN0YXRpYyBzZXRCYXNlVVJMKHVybDogc3RyaW5nID0gJy8nKTogdm9pZCB7XG4gICAgTG9vcEJhY2tDb25maWcucGF0aCA9IHVybDtcbiAgfVxuICBcbiAgcHVibGljIHN0YXRpYyBnZXRQYXRoKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIExvb3BCYWNrQ29uZmlnLnBhdGg7XG4gIH1cblxuICBwdWJsaWMgc3RhdGljIHNldEF1dGhQcmVmaXgoYXV0aFByZWZpeDogc3RyaW5nID0gJycpOiB2b2lkIHtcbiAgICBMb29wQmFja0NvbmZpZy5hdXRoUHJlZml4ID0gYXV0aFByZWZpeDtcbiAgfVxuXG4gIHB1YmxpYyBzdGF0aWMgZ2V0QXV0aFByZWZpeCgpOiBzdHJpbmcge1xuICAgIHJldHVybiBMb29wQmFja0NvbmZpZy5hdXRoUHJlZml4O1xuICB9XG5cbiAgcHVibGljIHN0YXRpYyBzZXREZWJ1Z01vZGUoaXNFbmFibGVkOiBib29sZWFuKTogdm9pZCB7XG4gICAgTG9vcEJhY2tDb25maWcuZGVidWcgPSBpc0VuYWJsZWQ7XG4gIH1cblxuICBwdWJsaWMgc3RhdGljIGRlYnVnZ2FibGUoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIExvb3BCYWNrQ29uZmlnLmRlYnVnO1xuICB9XG59XG4iXX0=