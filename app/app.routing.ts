import { HomeComponent } from "./pages/home/home.component";
import { GroupAddComponent } from "./pages/group-add/group-add.component";
import { GroupFeedComponent } from "./pages/group-feed/group-feed.component";

export const routes = [
  {path: "", component: HomeComponent},
  {path: "home", component: HomeComponent},
  {path: "group-add", component: GroupAddComponent},
  {path: "group-feed", component: GroupFeedComponent}
];

export const navigatableComponents = [
  HomeComponent,
  GroupAddComponent,
  GroupFeedComponent
];