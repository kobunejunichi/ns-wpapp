import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { Http, XHRBackend, RequestOptions } from "@angular/http";
import { platformNativeScriptDynamic } from "nativescript-angular/platform";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptFormsModule } from "nativescript-angular/forms"
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { SIDEDRAWER_DIRECTIVES } from "nativescript-telerik-ui/sidedrawer/angular";
import { SDKNativeModule } from './shared/sdk/index';

import { AppComponent } from "./app.component";

import { routes, navigatableComponents } from "./app.routing";

import { CustomHttp } from "./services/CustomHttp"

// HTTPヘッダーにカスタムヘッダーを追加するための設定
let LoopBackSDKModule = SDKNativeModule.forRoot();
LoopBackSDKModule.providers.push(
  {
    provide: Http, 
    useFactory: (backend: XHRBackend, defaultOptions: RequestOptions) => new CustomHttp(backend, defaultOptions),
    deps: [XHRBackend, RequestOptions]
  }
);

@NgModule({
  declarations: [
    AppComponent,
    SIDEDRAWER_DIRECTIVES,
    ...navigatableComponents
  ],
  bootstrap: [AppComponent],
  imports: [
    NativeScriptModule,
    NativeScriptHttpModule,
    NativeScriptFormsModule,
    NativeScriptRouterModule,
    NativeScriptRouterModule.forRoot(routes),
    LoopBackSDKModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule {}

platformNativeScriptDynamic({
  // startPageActionBarHidden: true  // 最初のページはアクションバーを非表示
}).bootstrapModule(AppModule);
