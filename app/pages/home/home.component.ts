import { Component, ViewChild, AfterViewInit } from "@angular/core";
import { RadSideDrawerComponent, SideDrawerType } from "nativescript-telerik-ui/sidedrawer/angular";
import { RouterExtensions } from "nativescript-angular/router";
import { AppUser, AccessToken } from '../../shared/sdk/models';
import { AppUserApi } from '../../shared/sdk/services';

@Component({
  templateUrl: "./pages/home/home.component.html",
  styleUrls: ["./pages/home/home.component.css"]
})
export class HomeComponent implements AfterViewInit {
  @ViewChild(RadSideDrawerComponent) private drawerComponent: RadSideDrawerComponent;
  private drawer: SideDrawerType;
  private pages: [{name: string}];
  private groups: [{name: string, updatedAt: Date}];
  private user = {};

  constructor(private routerExtensions: RouterExtensions, private appUserApi: AppUserApi) {
    this.pages = [
      {name: "Page1"},
      {name: "Page2"},
      {name: "Page3"},
    ];
    this.groups = [
      {name: "グループ１", updatedAt: new Date()},
      {name: "グループ２", updatedAt: new Date()},
      {name: "グループ３", updatedAt: new Date()},
      {name: "グループ４", updatedAt: new Date()},
      {name: "グループ５", updatedAt: new Date()}
    ];
  }
  ngAfterViewInit() {
    console.log("### After View Init");
    this.drawer = this.drawerComponent.sideDrawer;
    this.appUserApi.login({
      email: "kobune.junichi+100@gmail.com",
      password: "aaaaaaaa",
      uuid: ""
    }).subscribe((result) => {
      console.dump(result);
      this.user = result.user;

    }, (error) => {
      console.dump(error);
    });
  }
  openDrawer() {
    this.drawer.showDrawer();
  }
  goGroup($event) {
    this.routerExtensions.navigate(["/group-feed"], {});
  }
}