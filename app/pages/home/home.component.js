"use strict";
var core_1 = require("@angular/core");
var angular_1 = require("nativescript-telerik-ui/sidedrawer/angular");
var router_1 = require("nativescript-angular/router");
var services_1 = require("../../shared/sdk/services");
var HomeComponent = (function () {
    function HomeComponent(routerExtensions, appUserApi) {
        this.routerExtensions = routerExtensions;
        this.appUserApi = appUserApi;
        this.user = {};
        this.pages = [
            { name: "Page1" },
            { name: "Page2" },
            { name: "Page3" },
        ];
        this.groups = [
            { name: "グループ１", updatedAt: new Date() },
            { name: "グループ２", updatedAt: new Date() },
            { name: "グループ３", updatedAt: new Date() },
            { name: "グループ４", updatedAt: new Date() },
            { name: "グループ５", updatedAt: new Date() }
        ];
    }
    HomeComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        console.log("### After View Init");
        this.drawer = this.drawerComponent.sideDrawer;
        this.appUserApi.login({
            email: "kobune.junichi+100@gmail.com",
            password: "aaaaaaaa",
            uuid: ""
        }).subscribe(function (result) {
            console.dump(result);
            _this.user = result.user;
        }, function (error) {
            console.dump(error);
        });
    };
    HomeComponent.prototype.openDrawer = function () {
        this.drawer.showDrawer();
    };
    HomeComponent.prototype.goGroup = function ($event) {
        this.routerExtensions.navigate(["/group-feed"], {});
    };
    return HomeComponent;
}());
__decorate([
    core_1.ViewChild(angular_1.RadSideDrawerComponent),
    __metadata("design:type", angular_1.RadSideDrawerComponent)
], HomeComponent.prototype, "drawerComponent", void 0);
HomeComponent = __decorate([
    core_1.Component({
        templateUrl: "./pages/home/home.component.html",
        styleUrls: ["./pages/home/home.component.css"]
    }),
    __metadata("design:paramtypes", [router_1.RouterExtensions, services_1.AppUserApi])
], HomeComponent);
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJob21lLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsc0NBQW9FO0FBQ3BFLHNFQUFvRztBQUNwRyxzREFBK0Q7QUFFL0Qsc0RBQXVEO0FBTXZELElBQWEsYUFBYTtJQU94Qix1QkFBb0IsZ0JBQWtDLEVBQVUsVUFBc0I7UUFBbEUscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUFVLGVBQVUsR0FBVixVQUFVLENBQVk7UUFGOUUsU0FBSSxHQUFHLEVBQUUsQ0FBQztRQUdoQixJQUFJLENBQUMsS0FBSyxHQUFHO1lBQ1gsRUFBQyxJQUFJLEVBQUUsT0FBTyxFQUFDO1lBQ2YsRUFBQyxJQUFJLEVBQUUsT0FBTyxFQUFDO1lBQ2YsRUFBQyxJQUFJLEVBQUUsT0FBTyxFQUFDO1NBQ2hCLENBQUM7UUFDRixJQUFJLENBQUMsTUFBTSxHQUFHO1lBQ1osRUFBQyxJQUFJLEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxJQUFJLElBQUksRUFBRSxFQUFDO1lBQ3RDLEVBQUMsSUFBSSxFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsSUFBSSxJQUFJLEVBQUUsRUFBQztZQUN0QyxFQUFDLElBQUksRUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFFLElBQUksSUFBSSxFQUFFLEVBQUM7WUFDdEMsRUFBQyxJQUFJLEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxJQUFJLElBQUksRUFBRSxFQUFDO1lBQ3RDLEVBQUMsSUFBSSxFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsSUFBSSxJQUFJLEVBQUUsRUFBQztTQUN2QyxDQUFDO0lBQ0osQ0FBQztJQUNELHVDQUFlLEdBQWY7UUFBQSxpQkFjQztRQWJDLE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUNuQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDO1FBQzlDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDO1lBQ3BCLEtBQUssRUFBRSw4QkFBOEI7WUFDckMsUUFBUSxFQUFFLFVBQVU7WUFDcEIsSUFBSSxFQUFFLEVBQUU7U0FDVCxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUMsTUFBTTtZQUNsQixPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3JCLEtBQUksQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQztRQUUxQixDQUFDLEVBQUUsVUFBQyxLQUFLO1lBQ1AsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN0QixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDRCxrQ0FBVSxHQUFWO1FBQ0UsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUMzQixDQUFDO0lBQ0QsK0JBQU8sR0FBUCxVQUFRLE1BQU07UUFDWixJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsYUFBYSxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDdEQsQ0FBQztJQUNILG9CQUFDO0FBQUQsQ0FBQyxBQTFDRCxJQTBDQztBQXpDb0M7SUFBbEMsZ0JBQVMsQ0FBQyxnQ0FBc0IsQ0FBQzs4QkFBMEIsZ0NBQXNCO3NEQUFDO0FBRHhFLGFBQWE7SUFKekIsZ0JBQVMsQ0FBQztRQUNULFdBQVcsRUFBRSxrQ0FBa0M7UUFDL0MsU0FBUyxFQUFFLENBQUMsaUNBQWlDLENBQUM7S0FDL0MsQ0FBQztxQ0FRc0MseUJBQWdCLEVBQXNCLHFCQUFVO0dBUDNFLGFBQWEsQ0EwQ3pCO0FBMUNZLHNDQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBWaWV3Q2hpbGQsIEFmdGVyVmlld0luaXQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgUmFkU2lkZURyYXdlckNvbXBvbmVudCwgU2lkZURyYXdlclR5cGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LXRlbGVyaWstdWkvc2lkZWRyYXdlci9hbmd1bGFyXCI7XG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xuaW1wb3J0IHsgQXBwVXNlciwgQWNjZXNzVG9rZW4gfSBmcm9tICcuLi8uLi9zaGFyZWQvc2RrL21vZGVscyc7XG5pbXBvcnQgeyBBcHBVc2VyQXBpIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3Nkay9zZXJ2aWNlcyc7XG5cbkBDb21wb25lbnQoe1xuICB0ZW1wbGF0ZVVybDogXCIuL3BhZ2VzL2hvbWUvaG9tZS5jb21wb25lbnQuaHRtbFwiLFxuICBzdHlsZVVybHM6IFtcIi4vcGFnZXMvaG9tZS9ob21lLmNvbXBvbmVudC5jc3NcIl1cbn0pXG5leHBvcnQgY2xhc3MgSG9tZUNvbXBvbmVudCBpbXBsZW1lbnRzIEFmdGVyVmlld0luaXQge1xuICBAVmlld0NoaWxkKFJhZFNpZGVEcmF3ZXJDb21wb25lbnQpIHByaXZhdGUgZHJhd2VyQ29tcG9uZW50OiBSYWRTaWRlRHJhd2VyQ29tcG9uZW50O1xuICBwcml2YXRlIGRyYXdlcjogU2lkZURyYXdlclR5cGU7XG4gIHByaXZhdGUgcGFnZXM6IFt7bmFtZTogc3RyaW5nfV07XG4gIHByaXZhdGUgZ3JvdXBzOiBbe25hbWU6IHN0cmluZywgdXBkYXRlZEF0OiBEYXRlfV07XG4gIHByaXZhdGUgdXNlciA9IHt9O1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucywgcHJpdmF0ZSBhcHBVc2VyQXBpOiBBcHBVc2VyQXBpKSB7XG4gICAgdGhpcy5wYWdlcyA9IFtcbiAgICAgIHtuYW1lOiBcIlBhZ2UxXCJ9LFxuICAgICAge25hbWU6IFwiUGFnZTJcIn0sXG4gICAgICB7bmFtZTogXCJQYWdlM1wifSxcbiAgICBdO1xuICAgIHRoaXMuZ3JvdXBzID0gW1xuICAgICAge25hbWU6IFwi44Kw44Or44O844OX77yRXCIsIHVwZGF0ZWRBdDogbmV3IERhdGUoKX0sXG4gICAgICB7bmFtZTogXCLjgrDjg6vjg7zjg5fvvJJcIiwgdXBkYXRlZEF0OiBuZXcgRGF0ZSgpfSxcbiAgICAgIHtuYW1lOiBcIuOCsOODq+ODvOODl++8k1wiLCB1cGRhdGVkQXQ6IG5ldyBEYXRlKCl9LFxuICAgICAge25hbWU6IFwi44Kw44Or44O844OX77yUXCIsIHVwZGF0ZWRBdDogbmV3IERhdGUoKX0sXG4gICAgICB7bmFtZTogXCLjgrDjg6vjg7zjg5fvvJVcIiwgdXBkYXRlZEF0OiBuZXcgRGF0ZSgpfVxuICAgIF07XG4gIH1cbiAgbmdBZnRlclZpZXdJbml0KCkge1xuICAgIGNvbnNvbGUubG9nKFwiIyMjIEFmdGVyIFZpZXcgSW5pdFwiKTtcbiAgICB0aGlzLmRyYXdlciA9IHRoaXMuZHJhd2VyQ29tcG9uZW50LnNpZGVEcmF3ZXI7XG4gICAgdGhpcy5hcHBVc2VyQXBpLmxvZ2luKHtcbiAgICAgIGVtYWlsOiBcImtvYnVuZS5qdW5pY2hpKzEwMEBnbWFpbC5jb21cIixcbiAgICAgIHBhc3N3b3JkOiBcImFhYWFhYWFhXCIsXG4gICAgICB1dWlkOiBcIlwiXG4gICAgfSkuc3Vic2NyaWJlKChyZXN1bHQpID0+IHtcbiAgICAgIGNvbnNvbGUuZHVtcChyZXN1bHQpO1xuICAgICAgdGhpcy51c2VyID0gcmVzdWx0LnVzZXI7XG5cbiAgICB9LCAoZXJyb3IpID0+IHtcbiAgICAgIGNvbnNvbGUuZHVtcChlcnJvcik7XG4gICAgfSk7XG4gIH1cbiAgb3BlbkRyYXdlcigpIHtcbiAgICB0aGlzLmRyYXdlci5zaG93RHJhd2VyKCk7XG4gIH1cbiAgZ29Hcm91cCgkZXZlbnQpIHtcbiAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL2dyb3VwLWZlZWRcIl0sIHt9KTtcbiAgfVxufSJdfQ==