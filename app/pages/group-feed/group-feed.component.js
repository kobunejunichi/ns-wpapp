"use strict";
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var color_1 = require("color");
var GroupFeedComponent = (function () {
    function GroupFeedComponent(page) {
        this.page = page;
    }
    GroupFeedComponent.prototype.ngOnInit = function () {
        // 動的にデザインを変更してみる
        this.page.actionBar.backgroundColor = new color_1.Color("#82bcff");
        this.page.actionBar.color = new color_1.Color("#ffffff");
        this.page.css = "\n    Page {\n      background-image: url(\"~/images/parts/icon-share@2x.png\");\n    }\n    ";
    };
    GroupFeedComponent.prototype.ngAfterViewInit = function () {
    };
    return GroupFeedComponent;
}());
GroupFeedComponent = __decorate([
    core_1.Component({
        templateUrl: "./pages/group-feed/group-feed.component.html",
        styleUrls: ["./pages/group-feed/group-feed.component.css"]
    }),
    __metadata("design:paramtypes", [page_1.Page])
], GroupFeedComponent);
exports.GroupFeedComponent = GroupFeedComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3JvdXAtZmVlZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJncm91cC1mZWVkLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsc0NBQTRFO0FBRTVFLGdDQUErQjtBQUMvQiwrQkFBOEI7QUFNOUIsSUFBYSxrQkFBa0I7SUFDN0IsNEJBQW9CLElBQVU7UUFBVixTQUFJLEdBQUosSUFBSSxDQUFNO0lBQzlCLENBQUM7SUFDRCxxQ0FBUSxHQUFSO1FBQ0UsaUJBQWlCO1FBQ2pCLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGVBQWUsR0FBRyxJQUFJLGFBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUMzRCxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEdBQUcsSUFBSSxhQUFLLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsK0ZBSWYsQ0FBQztJQUNKLENBQUM7SUFDRCw0Q0FBZSxHQUFmO0lBQ0EsQ0FBQztJQUNILHlCQUFDO0FBQUQsQ0FBQyxBQWZELElBZUM7QUFmWSxrQkFBa0I7SUFKOUIsZ0JBQVMsQ0FBQztRQUNULFdBQVcsRUFBRSw4Q0FBOEM7UUFDM0QsU0FBUyxFQUFFLENBQUMsNkNBQTZDLENBQUM7S0FDM0QsQ0FBQztxQ0FFMEIsV0FBSTtHQURuQixrQkFBa0IsQ0FlOUI7QUFmWSxnREFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIFZpZXdDaGlsZCwgT25Jbml0LCBBZnRlclZpZXdJbml0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IEFjdGlvbkJhciB9IGZyb20gXCJ1aS9hY3Rpb24tYmFyXCI7XG5pbXBvcnQgeyBQYWdlIH0gZnJvbSBcInVpL3BhZ2VcIjtcbmltcG9ydCB7IENvbG9yIH0gZnJvbSBcImNvbG9yXCI7XG5cbkBDb21wb25lbnQoe1xuICB0ZW1wbGF0ZVVybDogXCIuL3BhZ2VzL2dyb3VwLWZlZWQvZ3JvdXAtZmVlZC5jb21wb25lbnQuaHRtbFwiLFxuICBzdHlsZVVybHM6IFtcIi4vcGFnZXMvZ3JvdXAtZmVlZC9ncm91cC1mZWVkLmNvbXBvbmVudC5jc3NcIl1cbn0pXG5leHBvcnQgY2xhc3MgR3JvdXBGZWVkQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBBZnRlclZpZXdJbml0IHtcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBwYWdlOiBQYWdlKSB7XG4gIH1cbiAgbmdPbkluaXQoKSB7XG4gICAgLy8g5YuV55qE44Gr44OH44K244Kk44Oz44KS5aSJ5pu044GX44Gm44G/44KLXG4gICAgdGhpcy5wYWdlLmFjdGlvbkJhci5iYWNrZ3JvdW5kQ29sb3IgPSBuZXcgQ29sb3IoXCIjODJiY2ZmXCIpO1xuICAgIHRoaXMucGFnZS5hY3Rpb25CYXIuY29sb3IgPSBuZXcgQ29sb3IoXCIjZmZmZmZmXCIpO1xuICAgIHRoaXMucGFnZS5jc3MgPSBgXG4gICAgUGFnZSB7XG4gICAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJ+L2ltYWdlcy9wYXJ0cy9pY29uLXNoYXJlQDJ4LnBuZ1wiKTtcbiAgICB9XG4gICAgYDtcbiAgfVxuICBuZ0FmdGVyVmlld0luaXQoKSB7XG4gIH1cbn0iXX0=