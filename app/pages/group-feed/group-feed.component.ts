import { Component, ViewChild, OnInit, AfterViewInit } from "@angular/core";
import { ActionBar } from "ui/action-bar";
import { Page } from "ui/page";
import { Color } from "color";

@Component({
  templateUrl: "./pages/group-feed/group-feed.component.html",
  styleUrls: ["./pages/group-feed/group-feed.component.css"]
})
export class GroupFeedComponent implements OnInit, AfterViewInit {
  constructor(private page: Page) {
  }
  ngOnInit() {
    // 動的にデザインを変更してみる
    this.page.actionBar.backgroundColor = new Color("#82bcff");
    this.page.actionBar.color = new Color("#ffffff");
    this.page.css = `
    Page {
      background-image: url("~/images/parts/icon-share@2x.png");
    }
    `;
  }
  ngAfterViewInit() {
  }
}